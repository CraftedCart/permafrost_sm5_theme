local function string_starts_with(str, start)
  return str:sub(1, #start) == start
end

local function unload_pf_modules()
  local to_unload = {}

  for k, _ in pairs(package.loaded) do
    if string_starts_with(k, "permafrost") then
      table.insert(to_unload, k)
    end
  end

  for _, v in pairs(to_unload) do
    package.loaded[v] = nil
  end
end

local function load_pf_module(mod_name)
  local path = THEME:GetCurrentThemeDirectory() .. "src/" .. mod_name:gsub("%.", "/") .. ".lua"
  local contents, err = lua.ReadFile(path)
  if err then error("Failed to read file: " .. path) end

  local chunk, err = load(
    function()
      -- wh... I don't know why but this fixes issues with.. files being tens of thousands of lines long apparently wtf?
      local ret = contents
      contents = nil
      return ret
    end,
    mod_name .. " @ " .. path
  )
  if err then error(err) end

  return chunk
end

unload_pf_modules()

if not g_pf_module_loader_installed then
  -- Lua 5.2 renamed `package.loaders` -> `package.searchers`
  if package.searchers ~= nil then
    table.insert(package.searchers, load_pf_module)
    g_pf_module_loader_installed = true -- Global
  else
    table.insert(package.loaders, load_pf_module)
    g_pf_module_loader_installed = true -- Global
  end
end
