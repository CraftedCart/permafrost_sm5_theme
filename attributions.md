Sounds/hitsound/snap.wav: https://freesound.org/people/Snapper4298/sounds/177494/ CC BY 3.0
Sounds/hitsound/snare.wav: https://freesound.org/people/VEXST/sounds/26903/ CC BY 3.0
Sounds/hitsound/soft_pop.wav: https://freesound.org/people/CBJ_Student/sounds/545198/ CC BY 3.0
Sounds/hitsound/pop2.wav: https://freesound.org/people/runirasmussen/sounds/178446/ CC BY 3.0
Sounds/hitsound/clap.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/click.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/drum_hit.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/drum_hit_soft.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/drum_thud.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/jingle.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/pop.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/hitsound/pop_reverb.wav: From opsu! https://github.com/itdelatrisu/opsu

Sounds/combo_break/cartoon_slip.wav: From opsu! https://github.com/itdelatrisu/opsu
Sounds/combo_break/duck_quack.wav: https://freesound.org/people/qubodup/sounds/442820/ CC BY 3.0
Sounds/combo_break/human_quack.wav: https://freesound.org/people/stomachache/sounds/53258/ CC0

https://github.com/telemachus/split BSD-3-Clause
