Permafrost (Etterna / StepMania 5 theme)
========================================

## WIP

This theme is still woefully incomplete - it's usable, though missing *a lot* of stuff.

## About

Permafrost is a clean theme for both vanilla StepMania and Etterna.

## Compatibility

This theme has been known to work with...

- Etterna 0.69.1
- StepMania 5.3 Alpha 4.6.0 (mostly-ish-kinda)

Your mileage may vary with other versions of the game.

## Installation

Download a zip of this repository, and unzip it right to your StepMania/Etterna themes folder. For help on how to find this, see [the StepMania wiki](https://github.com/stepmania/stepmania/wiki/User-Data-Locations).

If you have `git` installed, you may prefer to clone this into the themes folder instead.

## Stuff you should probably know

aka stuff I haven't communicated in the UI well yet

- `Ctrl-R` on the music wheel will trigger a differential reload on Etterna
- To search for songs in a pack, search for `[@Pack Name]` (typing `@` will insert the square brackets for you)
- To get to gameplay settings, press `Backspace` on the song wheel (Double pressing enter doesn't work!!)
- This theme looks a bit broken in gameplay with anything other than 50% notefield scale...
- If anything goes wrong, `F12` will reload the theme
