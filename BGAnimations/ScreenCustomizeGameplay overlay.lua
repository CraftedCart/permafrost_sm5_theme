local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pfcustomizegameplaywidget = require("permafrost.widget.pfcustomizegameplaywidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfsignal = require("permafrost.pfsignal")
local pfinput = require("permafrost.pfinput")
local pfsong = require("permafrost.pfsong")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput
local PfCustomizeGameplayWidget = pfcustomizegameplaywidget.PfCustomizeGameplayWidget

local canvas = PfCanvas()
canvas:add_background()

local customize_gameplay_widget = PfCustomizeGameplayWidget()
customize_gameplay_widget:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, pfconfig.top_bar_height)
    actor.size = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT - pfconfig.top_bar_height)
  end
)
canvas:add_child(customize_gameplay_widget)

local function input_handler(event)
  if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
    if event.GameButton == "Back" then
      pfutil.play_click_sound()

      pfwidget.set_focus(nil)
      pfutil.screen_switch("ScreenSelectMusic")

    -- else
    --   SCREENMAN:SystemMessage(event.GameButton .. " / " .. event.DeviceInput.button)
    end
  end
end

canvas:add_hook(
  "OnCommand",
  function(actor)
    pfinput.add_input_handler(input_handler)
    pfutil.key_hints.add(pflang.get_entry("hint_switch_page"), "[    ]")

    customize_gameplay_widget:focus()

    -- Run next frame - we need to do this for music to play for *some* reason
    canvas.ctx.timer.run_in_seconds(
      0,
      function()
        local song = GAMESTATE:GetCurrentSong()
        SOUND:PlayMusicPart(song:GetPreviewMusicPath(), GAMESTATE:GetCurMusicSeconds(), -1, 0, 0, false, true, false)
      end
    )
  end
)

-- Handle music looping {{{
local was_prev_frame_timing_delayed = true
local function check_song_on_tick(delta_seconds)
  local now_time = GAMESTATE:GetCurMusicSeconds()
  local song = GAMESTATE:GetCurrentSong()
  if not song then return end
  local song_length = song:MusicLengthSeconds()

  -- If timing delayed returns false, we give an extra 1 frame delay for now_time to catch up
  if SOUND:IsTimingDelayed() then
    was_prev_frame_timing_delayed = true
  elseif now_time > song_length then
    if was_prev_frame_timing_delayed then
      was_prev_frame_timing_delayed = false
    else
      SOUND:PlayMusicPart(song:GetPreviewMusicPath(), 0, -1, 0, 0, false, true, false)
    end
  end
end

pftimer.connect_tick(check_song_on_tick)

canvas:add_hook(
  "EndCommand",
  function(actor)
    pftimer.disconnect_tick(check_song_on_tick)
  end
)

-- }}}

return canvas:make_actor()
