local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfeval = require("permafrost.pfeval")
local pfscore = require("permafrost.pfscore")
local pfsong = require("permafrost.pfsong")
local pfwidget = require("permafrost.pfwidget")
local pfoffsetgraphwidget = require("permafrost.widget.pfoffsetgraphwidget")
local pfoffsetdistribwidget = require("permafrost.widget.pfoffsetdistribwidget")
local pfleaderboardwidget = require("permafrost.widget.pfleaderboardwidget")
local pfpiewidget = require("permafrost.widget.pfpiewidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfinput = require("permafrost.pfinput")
local pflang = require("permafrost.pflang")
local pfeasing = require("permafrost.pfeasing")
local pfconfig = require("permafrost.pfconfig")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput
local PfOffsetGraphWidget = pfoffsetgraphwidget.PfOffsetGraphWidget
local PfOffsetDistribWidget = pfoffsetdistribwidget.PfOffsetDistribWidget
local PfPieWidget = pfpiewidget.PfPieWidget
local PfPieSegment = pfpiewidget.PfPieSegment
local PfLeaderboardWidget = pfleaderboardwidget.PfLeaderboardWidget

local MISSING_BACKGROUND_PATH = THEME:GetPathG("", "bg_logo.png")
local CIRCLE256_PATH = THEME:GetPathG("", "circle256.png")
local CIRCLE256_OUTLINE_PATH = THEME:GetPathG("", "circle256_outline.png")
local STAR64_PATH = THEME:GetPathG("", "star64.png")
local X64_PATH = THEME:GetPathG("", "x64.png")
local ANIM_DELAY = 2.5

local song = pfsong.get_current_song()
local score
local stage_stats

local canvas = PfCanvas()
canvas:add_background()

-- Song BG {{{

local song_bg = PfRect()
song_bg:add_hook(
  "InitCommand",
  function(actor)
    local song_bg = song.background_path
    if song_bg == nil then
      song_bg = MISSING_BACKGROUND_PATH
    end

    actor.draw_order = -299
    actor.position = Vec2(0, SCREEN_CENTER_Y)
    actor.align = Vec2(0, 0.5)
    actor.color = color("0.2, 0.2, 0.2, 1")
    actor:load_texture_background(song_bg)
    actor:scale_to_cover(Vec2(0, 0), Vec2(SCREEN_WIDTH, SCREEN_BOTTOM))
  end
)
canvas:add_child(song_bg)

-- }}}

-- Song title/subtitle/artist {{{

local song_title_text = PfText{font = pfconfig.fonts.light_40}
song_title_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, pfconfig.top_bar_height + 64)
    actor.scale = 1.0
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.title_text
    actor.alpha = 0
    actor.text = song.display_main_title
  end
)
canvas:add_child(song_title_text)

local song_subtitle_text = PfText{font = pfconfig.fonts.light_40}
song_subtitle_text:add_hook(
  "OnCommand",
  function(actor)
    actor.position = Vec2(song_title_text.x + song_title_text.width_scaled + 20, 0)
    actor.scale = 0.5
    actor.align = Vec2(0, 1)
    actor.color = pfconfig.colors.title_text
    actor.alpha = 0
    actor.text = song.display_sub_title
  end
)
canvas:add_child(song_subtitle_text)

local song_artist_text = PfText{font = pfconfig.fonts.light_40}
song_artist_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, pfconfig.top_bar_height + 64 + 48)
    actor.scale = 0.7
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.title_text
    actor.alpha = 0
    actor.text = song.display_artist
  end
)
canvas:add_child(song_artist_text)

-- }}}

-- Grade circle {{{

local circle_canvas = PfCanvas()
circle_canvas:add_hook(
  "OnCommand",
  function(actor)
    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
    actor.align = Vec2(0.5, 0.5)
    actor.alpha = 0
    actor.scale = 2

    actor.ctx.anim{
      start_val = actor.alpha,
      end_val = 1,
      prop_table = actor,
      prop_name = "alpha",
      duration = 0.5,
      easing = pfeasing.linear,
    }

    actor.ctx.anim{
      start_val = actor.scale.x,
      end_val = 1,
      prop_table = actor,
      prop_name = "scale",
      duration = 0.5,
      easing = pfeasing.out_expo,
    }
  end
)
canvas:add_child(circle_canvas)

local circle_widget = PfRect()
circle_widget:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, 0)
    actor.align = Vec2(0.5, 0.5)
    actor.color = pfconfig.palette.gray_dark
    actor.alpha = 0.8
    actor:load_texture(CIRCLE256_PATH)
    actor.size = Vec2(256, 256)
  end
)
circle_canvas:add_child(circle_widget)

local circle_outline_widget = PfRect()
circle_outline_widget:add_hook(
  "OnCommand",
  function(actor)
    actor.position = Vec2(0, 0)
    actor.align = Vec2(0.5, 0.5)
    actor.color = score.grade.color
    actor.alpha = 0.8
    actor:load_texture(CIRCLE256_OUTLINE_PATH)
    actor.size = Vec2(256, 256)
  end
)
circle_canvas:add_child(circle_outline_widget)

-- }}}

-- Grade text {{{

local grade_text = PfText{font = pfconfig.fonts.light_90}
grade_text:add_hook(
  "OnCommand",
  function(actor)
    local grade = score.grade

    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y - 24)
    actor.scale = 4.0
    actor.align = Vec2(0.5, 0.5)
    actor.color = grade.color
    actor.alpha = 0
    actor.text = pflang.get_entry(grade.name_key)

    actor.ctx.anim{
      start_val = 0,
      end_val = 1,
      prop_table = actor,
      prop_name = "alpha",
      duration = 0.5,
      easing = pfeasing.linear,
    }

    actor.ctx.anim{
      start_val = 4,
      end_val = 1,
      prop_table = actor,
      prop_name = "scale",
      duration = 0.5,
      easing = pfeasing.out_expo,
    }

    local init_elapsed_seconds = pftimer.elapsed_seconds
    local anim1_started = false
    local anim2_started = false
    local tick_func = function(delta_seconds)
      if pftimer.elapsed_seconds > init_elapsed_seconds + 0.3 and not anim1_started then
        anim1_started = true

        actor.ctx.anim{
          start_val = actor.y,
          end_val = SCREEN_CENTER_Y - 50,
          prop_table = actor,
          prop_name = "y",
          duration = 0.5,
          easing = pfeasing.out_expo,
        }
      end

      if pftimer.elapsed_seconds > init_elapsed_seconds + ANIM_DELAY and not anim2_started then
        anim2_started = true

        actor.ctx.anim{
          start_val = actor.x,
          end_val = 64,
          prop_table = actor,
          prop_name = "x",
          duration = 0.8,
          easing = pfeasing.in_out_expo,
        }

        actor.ctx.anim{
          start_val = actor.h_align,
          end_val = 0,
          prop_table = actor,
          prop_name = "h_align",
          duration = 0.5,
          easing = pfeasing.in_out_expo,
        }

        actor.ctx.anim{
          start_val = actor.y,
          end_val = pfconfig.top_bar_height + 64 + 48 + 48,
          prop_table = actor,
          prop_name = "y",
          duration = 0.5,
          easing = pfeasing.in_out_expo,
        }

        actor.ctx.anim{
          start_val = actor.scale.x,
          end_val = 0.5,
          prop_table = actor,
          prop_name = "scale",
          duration = 0.5,
          easing = pfeasing.in_out_expo,
        }
      end
    end
    pftimer.connect_tick(tick_func)

    actor:add_hook(
      "EndCommand",
      function()
        pftimer.disconnect_tick(tick_func)
      end
    )
  end
)
canvas:add_child(grade_text)

-- }}}

-- Accuracy text {{{

local accuracy_text = PfText{font = pfconfig.fonts.light_40}
accuracy_text:add_hook(
  "OnCommand",
  function(actor)
    local accuracy = score.percentage_score
    local display_accuracy = accuracy / 1.5

    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y + 256)
    actor.scale = 1.0
    actor.align = Vec2(0.5, 1)
    actor.color = pfconfig.colors.title_text
    actor.alpha = 0
    actor.text = "0.00%"

    local init_elapsed_seconds = pftimer.elapsed_seconds
    local anim1_started = false
    local anim2_started = false
    local tick_func = function(delta_seconds)
      if pftimer.elapsed_seconds > init_elapsed_seconds + 0.3 and not anim1_started then
        anim1_started = true

        actor.ctx.anim{
          start_val = actor.alpha,
          end_val = 1,
          prop_table = actor,
          prop_name = "alpha",
          duration = 0.5,
          easing = pfeasing.linear,
        }

        actor.ctx.anim{
          start_val = actor.y,
          end_val = SCREEN_CENTER_Y + 50,
          prop_table = actor,
          prop_name = "y",
          duration = 0.5,
          easing = pfeasing.out_expo,
        }
      end

      if anim1_started then
        display_accuracy = pfmath.lerp(display_accuracy, accuracy, math.min(delta_seconds * 8, 1))
        actor.text = string.format("%.2f%%", display_accuracy * 100)
      end

      if pftimer.elapsed_seconds > init_elapsed_seconds + ANIM_DELAY and not anim2_started then
        anim2_started = true

        actor.ctx.anim{
          start_val = actor.x,
          end_val = 64 + (grade_text.width_scaled / 2) + 20,
          prop_table = actor,
          prop_name = "x",
          duration = 0.9,
          easing = pfeasing.in_out_expo,
        }

        actor.ctx.anim{
          start_val = actor.h_align,
          end_val = 0,
          prop_table = actor,
          prop_name = "h_align",
          duration = 0.6,
          easing = pfeasing.in_out_expo,
        }

        actor.ctx.anim{
          start_val = actor.y,
          end_val = pfconfig.top_bar_height + 64 + 48 + 48 + 24,
          prop_table = actor,
          prop_name = "y",
          duration = 0.6,
          easing = pfeasing.in_out_expo,
        }
      end
    end
    pftimer.connect_tick(tick_func)

    actor:add_hook(
      "EndCommand",
      function()
        pftimer.disconnect_tick(tick_func)
      end
    )
  end
)
canvas:add_child(accuracy_text)

-- }}}

-- Judgment text {{{

local left_sidebar_sections = {}
local PIE_HIDDEN_COLOR = pfconfig.palette.gray
for k, v in ipairs{
  {lang_key = "judge_w1", count_key = "tap_w1_count", color = pfconfig.colors.judge_w1},
  {lang_key = "judge_w2", count_key = "tap_w2_count", color = pfconfig.colors.judge_w2},
  {lang_key = "judge_w3", count_key = "tap_w3_count", color = pfconfig.colors.judge_w3},
  {lang_key = "judge_w4", count_key = "tap_w4_count", color = pfconfig.colors.judge_w4},
  {lang_key = "judge_w5", count_key = "tap_w5_count", color = pfconfig.colors.judge_w5},
  {lang_key = "judge_miss", count_key = "tap_miss_count", color = pfconfig.colors.judge_miss},
} do
  local widgets = {}

  local pie = PfPieWidget()
  pie:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(40, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (k - 1)) + 8)
      actor.alpha = 0
      actor.radius = 12
      actor.radius_inner = 6

      local player_stats = stage_stats.player_stats[PLAYER_1]

      local w1_color = k == 1 and pfconfig.colors.judge_w1 or PIE_HIDDEN_COLOR
      local w2_color = k == 2 and pfconfig.colors.judge_w2 or PIE_HIDDEN_COLOR
      local w3_color = k == 3 and pfconfig.colors.judge_w3 or PIE_HIDDEN_COLOR
      local w4_color = k == 4 and pfconfig.colors.judge_w4 or PIE_HIDDEN_COLOR
      local w5_color = k == 5 and pfconfig.colors.judge_w5 or PIE_HIDDEN_COLOR
      local miss_color = k == 6 and pfconfig.colors.judge_miss or PIE_HIDDEN_COLOR

      actor:add_segment(PfPieSegment(player_stats.tap_w1_count, w1_color))
      actor:add_segment(PfPieSegment(player_stats.tap_w2_count, w2_color))
      actor:add_segment(PfPieSegment(player_stats.tap_w3_count, w3_color))
      actor:add_segment(PfPieSegment(player_stats.tap_w4_count, w4_color))
      actor:add_segment(PfPieSegment(player_stats.tap_w5_count, w5_color))
      actor:add_segment(PfPieSegment(player_stats.tap_miss_count, miss_color))
      actor:remake_pie_mesh()
    end
  )
  canvas:add_child(pie)
  table.insert(widgets, pie)

  local score_text = PfText{font = pfconfig.fonts.light_40}
  score_text:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(64, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (k - 1)))
      actor.scale = 0.7
      actor.align = Vec2(0, 0)
      actor.color = v.color
      actor.alpha = 0
      actor.text = pflang.get_entry(v.lang_key)
    end
  )
  canvas:add_child(score_text)
  table.insert(widgets, score_text)

  local count_text = PfText{font = pfconfig.fonts.light_40}
  count_text:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(64 + 256, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (k - 1)))
      actor.scale = 0.7
      actor.align = Vec2(1, 0)
      actor.color = v.color
      actor.alpha = 0

      local player_stats = stage_stats.player_stats[PLAYER_1]
      actor.text = tostring(player_stats[v.count_key])
    end
  )
  canvas:add_child(count_text)
  table.insert(widgets, count_text)

  table.insert(left_sidebar_sections, widgets)
end

-- }}}

-- Max combo, rating {{{

local combo_icon = PfRect()
combo_icon:add_hook(
  "OnCommand",
  function(actor)
    actor.position = Vec2(40, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (#left_sidebar_sections - 1)) + 10)
    actor.alpha = 0
    actor:load_texture(X64_PATH)
    actor.scale = 0.4
  end
)
canvas:add_child(combo_icon)

local max_combo_text = PfText{font = pfconfig.fonts.light_40}
max_combo_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (#left_sidebar_sections - 1)))
    actor.scale = 0.7
    actor.align = Vec2(0, 0)
    actor.alpha = 0
    actor.text = pflang.get_entry("max_combo")
  end
)
canvas:add_child(max_combo_text)

local max_combo_count_text = PfText{font = pfconfig.fonts.light_40}
max_combo_count_text:add_hook(
  "OnCommand",
  function(actor)
    actor.position = Vec2(64 + 256, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * (#left_sidebar_sections - 1)))
    actor.scale = 0.7
    actor.align = Vec2(1, 0)
    actor.alpha = 0

    local player_stats = stage_stats.player_stats[PLAYER_1]
    actor.text = tostring(player_stats.max_combo)
  end
)
canvas:add_child(max_combo_count_text)
table.insert(left_sidebar_sections, {combo_icon, max_combo_text, max_combo_count_text})

if pfutil.is_etterna then
  local rating_icon = PfRect()
  rating_icon:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(40, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * #left_sidebar_sections) + 10)
      actor.alpha = 0
      actor:load_texture(STAR64_PATH)
      actor.scale = 0.4
    end
  )
  canvas:add_child(rating_icon)

  local rating_text = PfText{font = pfconfig.fonts.light_40}
  rating_text:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(64, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * #left_sidebar_sections))
      actor.scale = 0.7
      actor.align = Vec2(0, 0)
      actor.alpha = 0
      actor.text = pflang.get_entry("rating")
    end
  )
  canvas:add_child(rating_text)

  local rating_score_text = PfText{font = pfconfig.fonts.light_40}
  rating_score_text:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(64 + 256, pfconfig.top_bar_height + 64 + 24 + 24 + 100 + (32 * #left_sidebar_sections))
      actor.scale = 0.7
      actor.align = Vec2(1, 0)
      actor.alpha = 0

      local player_stats = stage_stats.player_stats[PLAYER_1]
      actor.text = string.format("%.2f", player_stats.high_score.etterna_rating_overall)
    end
  )
  canvas:add_child(rating_score_text)
  table.insert(left_sidebar_sections, {rating_icon, rating_text, rating_score_text})
end

-- }}}

-- Offset graphs {{{

local offset_graph_widget
local offset_distrib_widget

if pfutil.is_etterna then
  offset_graph_widget = PfOffsetGraphWidget()
  offset_graph_widget:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(24, SCREEN_HEIGHT - 24)
      actor.align = Vec2(0.5, 1.5) -- I don't understand either
      actor.size = Vec2(SCREEN_WIDTH / 2 - 48 + 12, 256)
    end
  )
  canvas:add_child(offset_graph_widget)

  offset_distrib_widget = PfOffsetDistribWidget()
  offset_distrib_widget:add_hook(
    "OnCommand",
    function(actor)
      actor.position = Vec2(SCREEN_WIDTH / 2 + 12, SCREEN_HEIGHT - 24)
      actor.align = Vec2(0.5, 1) -- I don't understand either
      actor.size = Vec2(SCREEN_WIDTH / 2 - 48 + 12, 256)
    end
  )
  canvas:add_child(offset_distrib_widget)
end

-- }}}

-- Local leaderboard {{{

local local_leaderboard_widget = PfLeaderboardWidget()
local_leaderboard_widget:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 24, pfconfig.top_bar_height + 64)
    actor.align = Vec2(1.5, 0.5) -- /shrug
    actor.width = 600
  end
)
canvas:add_child(local_leaderboard_widget)

-- }}}

-- local tap_score_pie_widget = PfPieWidget()
-- tap_score_pie_widget:add_hook(
--   "OnCommand",
--   function(actor)
--     actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
--     actor.visible = false

--     local player_stats = stage_stats.player_stats[PLAYER_1]

--     actor:add_segment(PfPieSegment(player_stats.tap_w1_count, pfconfig.colors.judge_w1))
--     actor:add_segment(PfPieSegment(player_stats.tap_w2_count, pfconfig.colors.judge_w2))
--     actor:add_segment(PfPieSegment(player_stats.tap_w3_count, pfconfig.colors.judge_w3))
--     actor:add_segment(PfPieSegment(player_stats.tap_w4_count, pfconfig.colors.judge_w4))
--     actor:add_segment(PfPieSegment(player_stats.tap_w5_count, pfconfig.colors.judge_w5))
--     actor:add_segment(PfPieSegment(player_stats.tap_miss_count, pfconfig.colors.judge_miss))
--     actor:remake_pie_mesh()
--   end
-- )
-- canvas:add_child(tap_score_pie_widget)

canvas:add_hook(
  "OnCommand",
  function(actor)
    local init_seconds = pftimer.elapsed_seconds
    local init_frame = pftimer.elapsed_frames
    local music_start_time = GAMESTATE:GetCurMusicSeconds()

    score = pfscore.get_eval_score()
    stage_stats = pfeval.get_stage_stats()

    actor.ctx.timer.run_in_seconds(
      0,
      function()
        -- There's a small stutter but I don't think there's anything we can really do about that >.<
        -- ScreenGameplay's dtor in C++ stops playing the music >.<
        if pftimer.elapsed_frames == init_frame + 1 then
          SOUND:PlayMusicPart(
            GAMESTATE:GetCurrentSong():GetMusicPath(), -- Audio path
            music_start_time, -- Start time
            -1, -- Length
            0, -- Fade in time
            0, -- Fade out time
            false, -- Loop
            true, -- Apply rate
            false -- Align beat
          )
        end
      end
    )

    actor.ctx.timer.run_in_seconds(
      ANIM_DELAY,
      function()
        for _, v in pairs{song_title_text, song_subtitle_rext, song_artist_text} do
          actor.ctx.anim{
            start_val = v.x + 128,
            end_val = v.x,
            prop_table = v,
            prop_name = "x",
            duration = 0.5,
            easing = pfeasing.out_expo,
          }

          actor.ctx.anim{
            start_val = 0,
            end_val = 1,
            prop_table = v,
            prop_name = "alpha",
            duration = 0.5,
            easing = pfeasing.linear,
          }
        end

        actor.ctx.anim{
          start_val = circle_canvas.alpha,
          end_val = 0,
          prop_table = circle_canvas,
          prop_name = "alpha",
          duration = 0.5,
          easing = pfeasing.linear,
        }

        actor.ctx.anim{
          start_val = circle_canvas.scale.x,
          end_val = 0.5,
          prop_table = circle_canvas,
          prop_name = "scale",
          duration = 0.5,
          easing = pfeasing.in_expo,
        }

        if offset_graph_widget ~= nil then
          offset_graph_widget:show()
        end

        if offset_distrib_widget ~= nil then
          offset_distrib_widget:show()
        end

        local_leaderboard_widget:show()
      end
    )

    for k, v in ipairs(left_sidebar_sections) do
      actor.ctx.timer.run_in_seconds(
        ANIM_DELAY + (k * 0.2),
        function()
          for _, widget in pairs(v) do
            actor.ctx.anim{
              start_val = widget.x + 128,
              end_val = widget.x,
              prop_table = widget,
              prop_name = "x",
              duration = 0.5,
              easing = pfeasing.out_expo,
            }

            actor.ctx.anim{
              start_val = widget.alpha,
              end_val = 1,
              prop_table = widget,
              prop_name = "alpha",
              duration = 0.5,
              easing = pfeasing.linear,
            }
          end
        end
      )
    end

    actor.ctx.timer.run_in_seconds(
      ANIM_DELAY + 0.7,
      function()
        actor.ctx.anim{
          start_val = circle_canvas.alpha,
          end_val = 0,
          prop_table = circle_canvas,
          prop_name = "alpha",
          duration = 0.5,
          easing = pfeasing.linear,
        }

        -- tap_score_pie_widget.visible = true

        -- actor.ctx.anim{
        --   start_val = 0,
        --   end_val = 1,
        --   prop_table = tap_score_pie_widget,
        --   prop_name = "percent_circle",
        --   duration = 0.5,
        --   easing = pfeasing.in_out_expo,
        -- }
      end
    )
  end
)

return canvas:make_actor()
