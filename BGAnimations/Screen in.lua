local pfinput = require("permafrost.pfinput")
local pfutil = require("permafrost.pfutil")
local pfconfig = require("permafrost.pfconfig")

-- Reload theme key
local function input_handler(event)
  if event.type == "InputEventType_FirstPress" and event.DeviceInput.button == "DeviceButton_F12" then
    if pfinput.is_shift_down() then
      pfutil.screen_switch("ScreenPfDebug")
    else
      pfutil.reload_theme()
    end
  else
    pfinput.handle_event(event)
  end
end

return Def.Quad {
  InitCommand = function(self)
    self:xy(SCREEN_CENTER_X, SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH, SCREEN_HEIGHT)
  end,

  OnCommand = function(self)
    pfinput.clear_input_state()

    local top_screen = SCREENMAN:GetTopScreen()
    if top_screen then
      SCREENMAN:GetTopScreen():AddInputCallback(input_handler)
    end

    self:diffuse(pfconfig.colors.screen_background)

      -- :linear(0.1)
      :diffusealpha(0)
  end,
}
