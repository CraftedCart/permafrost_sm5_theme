local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfsignal = require("permafrost.pfsignal")
local pfinput = require("permafrost.pfinput")
local pfsong = require("permafrost.pfsong")
local pfscore = require("permafrost.pfscore")
local pfconfig = require("permafrost.pfconfig")
local pfstate = require("permafrost.pfstate")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput

local MISSING_BACKGROUND_PATH = THEME:GetPathG("", "bg_logo.png")

local SEARCH_BAR_HEIGHT = 48

local song_changed_signal = PfSignal()
local step_changed_signal = PfSignal()

local canvas = PfCanvas()
canvas:add_background()

local SORT_METHODS = {
  difficulty = pfsong.sort_methods.difficulty,
  msd = pfsong.sort_methods.difficulty,
  meter = pfsong.sort_methods.difficulty,
  title = pfsong.sort_methods.display_main_title,
  maintitle = pfsong.sort_methods.display_main_title,
  alphabetically = pfsong.sort_methods.display_main_title,
  subtitle = pfsong.sort_methods.display_sub_title,
  group = pfsong.sort_methods.group,
  pack = pfsong.sort_methods.group,
  bpm = pfsong.sort_methods.max_bpm,
  maxbpm = pfsong.sort_methods.max_bpm,
  minbpm = pfsong.sort_methods.min_bpm,
  length = pfsong.sort_methods.gameplay_length,
  duration = pfsong.sort_methods.gameplay_length,
}

local FILTER_PARAMS = {
  difficulty = pfsong.filter_parameters.difficulty,
  msd = pfsong.filter_parameters.difficulty,
  meter = pfsong.filter_parameters.difficulty,
  bpm = pfsong.filter_parameters.max_bpm,
  maxbpm = pfsong.filter_parameters.max_bpm,
  minbpm = pfsong.filter_parameters.min_bpm,
  length = pfsong.filter_parameters.gameplay_length,
  duration = pfsong.filter_parameters.gameplay_length,
}

-- TODO: Check why >= and <= don't seem to work?
local FILTER_OPS = {
  {sign = "=", func = function(a, b) return a == b end},
  {sign = ">=", func = function(a, b) return a >= b end},
  {sign = "<=", func = function(a, b) return a <= b end},
  {sign = ">", func = function(a, b) return a > b end},
  {sign = "<", func = function(a, b) return a < b end},
}

local function string_contains(str, part)
  return string.find(str, part, nil, true)
end

local SEARCH_SIGILS = { "@", ">", "<", "=" }

local function filter_songs(query)
  local all = pfsong.get_all_steps()
  local filtered = {}

  local sort_stack = {}

  local lower_query = query:lower()

  local query_fields = {}
  for part in lower_query:gmatch("%b[]") do
    if part:sub(2, 2) == "@" then
      -- @ matches groups
      for match_part in part:sub(3, -2):gmatch("%S+") do
        table.insert(query_fields, {
          op = string_contains,
          a = function(step) return step.song.group_name:lower() end,
          b = function() return match_part end
        })
      end

    elseif part:sub(2, 2) == ">" then
      -- > means sort ascending
      for match_part in part:sub(3, -2):gmatch("%S+") do
        -- Search for a matching sort method
        for k, v in pairs(SORT_METHODS) do
          if pfutil.string_starts_with(k, match_part) then
            table.insert(sort_stack, v.cmp)
          end
        end
      end

    elseif part:sub(2, 2) == "<" then
      -- > means sort descending
      for match_part in part:sub(3, -2):gmatch("%S+") do
        -- Search for a matching sort method
        for k, v in pairs(SORT_METHODS) do
          if pfutil.string_starts_with(k, match_part) then
            table.insert(sort_stack, pfutil.invert_cmp_sort(v.cmp))
            break
          end
        end
      end

    elseif part:sub(2, 2) == "=" then
      -- = means filter
      for match_part in part:sub(3, -2):gmatch("%S+") do
        -- Search for a matching filter op
        for _, v in pairs(FILTER_OPS) do
          local split = pfutil.string_split(match_part, v.sign)
          if #split == 2 then
            -- Search for a matching filter parameter
            for filter_k, filter_v in pairs(FILTER_PARAMS) do
              if pfutil.string_starts_with(filter_k, split[1]) then
                local b_num = tonumber(split[2])

                if b_num ~= nil then
                  table.insert(query_fields, {
                    op = v.func,
                    a = function(step) return filter_v(step) end,
                    b = function() return b_num end
                  })
                end
                break
              end
            end

            break
          end
        end
      end
    end
  end
  -- Remove the special marker things
  lower_query = lower_query:gsub("%b[]", " ")

  local query_parts = {}
  for part in lower_query:gmatch("%S+") do
    table.insert(query_parts, part)
  end

  for _, v in ipairs(all) do
    local query_matched = true

    for _, part in pairs(query_parts) do
      -- Check non-query-fields matches
      if not (v.song.display_main_title:lower():find(part, nil, true) or
        v.song.main_title:lower():find(part, nil, true) or
        v.song.display_sub_title:lower():find(part, nil, true) or
        v.song.display_artist:lower():find(part, nil, true))
      then
        query_matched = false
        break
      end
    end

    -- Check query field matches
    for _, qv in pairs(query_fields) do
      if not qv.op(qv.a(v), qv.b()) then
        query_matched = false
        break
      end
    end

    if query_matched then
      table.insert(filtered, v)
    end
  end

  -- Always sort by group then alpha then difficulty in the end
  table.insert(sort_stack, pfsong.sort_methods.group.cmp)
  table.insert(sort_stack, pfsong.sort_methods.display_main_title.cmp)
  table.insert(sort_stack, pfsong.sort_methods.difficulty.cmp)

  table.sort(filtered, pfutil.combine_cmp_sorts(sort_stack))

  return filtered
end

-- local player = GAMESTATE:GetEnabledPlayers()[1]
pfsong.conditional_index_songs() -- Songs should already be indexed, but they may not be (eg: if we reload the screen)
local steps = filter_songs(pfstate.music_select_search_query)
local selected_step_index = 1

local function get_current_step()
  return steps[selected_step_index]
end

-- Song background {{{

local song_bg_lower = PfRect()
song_bg_lower:add_hook(
  "InitCommand",
  function(actor)
    actor.draw_order = -299
    actor.position = Vec2(0, SCREEN_CENTER_Y)
    actor.align = Vec2(0, 0.5)
    actor.color = color("0.2, 0.2, 0.2, 1")
    actor:load_texture_background(MISSING_BACKGROUND_PATH)
    actor:scale_to_cover(Vec2(0, 0), Vec2(SCREEN_WIDTH, SCREEN_BOTTOM))
  end
)
canvas:add_child(song_bg_lower)

local song_bg_upper = PfRect()
song_bg_upper:add_hook(
  "InitCommand",
  function(actor)
    actor.draw_order = -298
    actor.position = Vec2(0, SCREEN_CENTER_Y)
    actor.align = Vec2(0, 0.5)
    actor.color = color("0.2, 0.2, 0.2, 1")
    -- Scale will be set whenever a new background texture is set here
  end
)
canvas:add_child(song_bg_upper)

-- }}}

-- Song title/subtitle/artist {{{

local song_title_text = PfText{font = pfconfig.fonts.light_40}
song_title_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, pfconfig.top_bar_height + 64)
    actor.scale = 1.0
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.title_text
  end
)
canvas:add_child(song_title_text)

local song_subtitle_text = PfText{font = pfconfig.fonts.light_40}
song_subtitle_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, 0)
    actor.scale = 0.5
    actor.align = Vec2(0, 1)
    actor.color = pfconfig.colors.title_text
  end
)
canvas:add_child(song_subtitle_text)

local song_artist_text = PfText{font = pfconfig.fonts.light_40}
song_artist_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(64, pfconfig.top_bar_height + 64 + 48)
    actor.scale = 0.7
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.title_text
  end
)
canvas:add_child(song_artist_text)

song_changed_signal:connect(
  function()
    local step = get_current_step()
    -- local steps = GAMESTATE:GetCurrentSteps(player)
    song_title_text.text = step.song.display_main_title
    song_subtitle_text.text = step.song.display_sub_title
    song_subtitle_text.x = song_title_text.x + song_title_text.width_scaled + 20
    song_subtitle_text.y = song_title_text.y + song_title_text.height_scaled
    song_artist_text.text = step.song.display_artist
    -- steps:GetMSD(1, 1)
  end
)
-- song_name_text:add_hook("CurrentRateChangedMessageCommand", function(actor) actor:queue_command("PF_Set") end)
-- song_name_text:add_hook("CurrentStepsP1ChangedMessageCommand", function(actor) actor:queue_command("PF_Set") end)

-- }}}

-- Song wheel {{{

-- Music wheel scroll bar
local song_wheel_scroll_bar

-- Music wheel
local update_entries_signal = PfSignal()
for i = 1, pfconfig.select_music_entry_count do
  local song_entry_canvas
  local song_entry_banner_box
  local song_entry_dim_right_rect
  local song_entry_box
  local song_title_text
  local song_subtitle_text
  local song_grade_text
  local song_percentage_text
  local step_difficulty_bar_bg
  local step_difficulty_bar_fg

  local prev_banner_path = nil
  local load_banner_delayed_action

  -- Update an entry on the list on init/when entries get reused
  local function update_entry()
    local step = steps[i]

    if i < 1 or i > #steps then
      song_entry_canvas.actor:visible(false)
      return
    else
      song_entry_canvas.actor:visible(true)
    end

    local banner_path = steps[i].song.banner_path or MISSING_BACKGROUND_PATH
    if prev_banner_path ~= banner_path then
      prev_banner_path = banner_path

      if load_banner_delayed_action then
        pftimer.cancel_delayed_action(load_banner_delayed_action)
        song_entry_banner_box.ctx.anim.stop_animating_property(song_entry_banner_box, "alpha")
      end
      song_entry_banner_box.alpha = 0
      -- Load the banner after a short delay such that we don't lag the game too hard when scrolling rapidly
      load_banner_delayed_action = canvas.ctx.timer.run_in_seconds(
        0.1,
        function()
          song_entry_banner_box:load_texture_banner(banner_path)
          song_entry_banner_box:scale_to_cover(
            Vec2(0, 0),
            Vec2(pfconfig.select_music_entry_width, pfconfig.select_music_entry_height - 8)
          )

          local h = song_entry_banner_box.height * song_entry_banner_box.scale_y
          local desired_height_percent = 1 - ((pfconfig.select_music_entry_height - 8) / h)
          song_entry_banner_box.actor:croptop(desired_height_percent / 2)
          song_entry_banner_box.actor:cropbottom(desired_height_percent / 2)

          song_entry_banner_box.y = (-h / 2) + ((pfconfig.select_music_entry_height - 8) / 2) + 4

          song_entry_banner_box.ctx.anim{
            start_val = 0,
            end_val = 1,
            prop_table = song_entry_banner_box,
            prop_name = "alpha",
            duration = 0.15,
            easing = pfeasing.linear,
          }
        end
      )
    end

    local grade = nil
    local percentage_score = nil
    local score = pfscore.get_scores_for_step(step, PLAYER_1)
      :iter()
      :filter(function(val)
          return val.music_rate == class.NULL or -- Vanilla StepMania
            pfmath.nearly_equal(val.music_rate, 1) -- Etterna
      end)
      :sort(function(a, b)
          -- Sort descending, largest score first
          return a.percentage_score > b.percentage_score
      end)
      :next()
    if score then
      grade = score.grade
      percentage_score = score.percentage_score
    end

    song_title_text.position = Vec2(24, 26)
    song_title_text.scale = 0.5
    song_title_text.align = Vec2(0, 1)
    song_title_text.color = pfconfig.colors.title_text
    song_title_text.alpha = 1
    song_title_text.text = step.song and step.song.display_main_title or " - "

    song_subtitle_text.position = Vec2(24, 34)
    song_subtitle_text.scale = 0.4
    song_subtitle_text.align = Vec2(0, 0)
    song_subtitle_text.color = pfconfig.colors.title_text
    song_subtitle_text.alpha = 1
    song_subtitle_text.text = step.song and step.song.display_sub_title or " - "

    song_grade_text.position = Vec2(pfconfig.select_music_entry_width - 24, 26)
    song_grade_text.scale = 0.5
    song_grade_text.align = Vec2(1, 1)
    song_grade_text.alpha = 1
    song_grade_text.visible = grade ~= nil

    song_percentage_text.position = Vec2(pfconfig.select_music_entry_width - 24, 34)
    song_percentage_text.scale = 0.4
    song_percentage_text.align = Vec2(1, 0)
    song_percentage_text.alpha = 1
    song_percentage_text.visible = percentage_score ~= nil

    song_entry_dim_right_rect.visible = grade ~= nil
    if grade then
      song_grade_text.color = grade.color
      song_percentage_text.color = grade.color
      song_entry_dim_right_rect.color = grade.color
      song_grade_text.text = grade.name
      song_percentage_text.text = string.format("%04.2f%%", percentage_score * 100)
    end

    step_difficulty_bar_fg.scale_x = math.min(step.difficulty / pfconfig.select_music_bar_max_difficulty, 1)
  end

  update_entries_signal:connect(update_entry)

  local function entry_on_tick(delta_seconds)
    song_entry_box.actor:diffuseleftedge(i == selected_step_index and
      Alpha(pfconfig.colors.accent, 0.7) or
      Alpha(pfconfig.palette.black, 0.7))

    local target_y = SCREEN_CENTER_Y +
      (i * pfconfig.select_music_entry_height) +
      (pfconfig.select_music_entry_height / 2) -
      (selected_step_index * pfconfig.select_music_entry_height) -
      pfconfig.select_music_entry_height

    song_entry_canvas.y = pfmath.lerp(song_entry_canvas.y, target_y, delta_seconds * 8)

    if song_entry_canvas.y < -pfconfig.select_music_entry_off_screen_border and
    target_y < -pfconfig.select_music_entry_off_screen_border then
      local how_far_behind = math.ceil(
        math.abs(song_entry_canvas.y + pfconfig.select_music_entry_off_screen_border) /
          (pfconfig.select_music_entry_height * pfconfig.select_music_entry_count)
      )

      i = i + pfconfig.select_music_entry_count * how_far_behind
      song_entry_canvas.y = song_entry_canvas.y +
        (pfconfig.select_music_entry_count * pfconfig.select_music_entry_height * how_far_behind)

      update_entry()
    elseif song_entry_canvas.y > SCREEN_HEIGHT + pfconfig.select_music_entry_off_screen_border and
    target_y > SCREEN_HEIGHT + pfconfig.select_music_entry_off_screen_border then
      local how_far_behind = math.ceil(
        math.abs(song_entry_canvas.y - pfconfig.select_music_entry_off_screen_border - SCREEN_HEIGHT) /
          (pfconfig.select_music_entry_height * pfconfig.select_music_entry_count)
      )

      i = i - pfconfig.select_music_entry_count * how_far_behind
      song_entry_canvas.y = song_entry_canvas.y -
        (pfconfig.select_music_entry_count * pfconfig.select_music_entry_height * how_far_behind)

      update_entry()
    end
  end

  pftimer.connect_tick(entry_on_tick)
  canvas:add_hook(
    "EndCommand",
    function()
      pftimer.disconnect_tick(entry_on_tick)
      pftimer.cancel_delayed_action(load_banner_delayed_action)
    end
  )

  song_entry_canvas = PfCanvas()
  song_entry_canvas:add_hook(
    "InitCommand",
    function(actor)
      actor.x = SCREEN_WIDTH - pfconfig.select_music_entry_width
      actor.y = SCREEN_CENTER_Y + (i * pfconfig.select_music_entry_height) + (pfconfig.select_music_entry_height / 2) + SCREEN_CENTER_Y
      actor.alpha = 0

      actor.ctx.anim{
        start_val = actor.x + 24,
        end_val = actor.x,
        prop_table = actor,
        prop_name = "x",
        duration = 1.5,
        easing = pfeasing.out_expo,
      }

      actor.ctx.anim{
        start_val = 0,
        end_val = 1,
        prop_table = actor,
        prop_name = "alpha",
        duration = 1.5,
        easing = pfeasing.out_expo,
      }

      update_entry()
    end
  )
  canvas:add_child(song_entry_canvas)

  song_entry_banner_box = PfRect()
  song_entry_banner_box:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(0, 4)
      actor.align = Vec2(0, 0)
      actor.scale = Vec2(pfconfig.select_music_entry_width, pfconfig.select_music_entry_height - 8)
      actor.color = pfconfig.palette.white
    end
  )
  song_entry_canvas:add_child(song_entry_banner_box)

  -- Tinted with the grade color
  song_entry_dim_right_rect = PfRect()
  song_entry_dim_right_rect:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(pfconfig.select_music_entry_width, 4)
      actor.align = Vec2(1, 0)
      actor.size = Vec2(96, pfconfig.select_music_entry_height - 8)
      actor.fade_left = 1
    end
  )
  song_entry_canvas:add_child(song_entry_dim_right_rect)

  song_entry_box = PfRect()
  song_entry_box:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(0, 4)
      actor.align = Vec2(0, 0)
      actor.scale = Vec2(pfconfig.select_music_entry_width, pfconfig.select_music_entry_height - 8)
      actor.color = pfconfig.palette.black
      actor.alpha = 0.7
    end
  )
  song_entry_canvas:add_child(song_entry_box)

  song_title_text = PfText{}
  song_entry_canvas:add_child(song_title_text)

  song_subtitle_text = PfText{}
  song_entry_canvas:add_child(song_subtitle_text)

  song_grade_text = PfText{}
  song_entry_canvas:add_child(song_grade_text)

  song_percentage_text = PfText{}
  song_entry_canvas:add_child(song_percentage_text)

  step_difficulty_bar_bg = PfRect()
  step_difficulty_bar_bg:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(24, pfconfig.select_music_entry_height - 12)
      actor.align = Vec2(0, 0)
      actor.size = Vec2(pfconfig.select_music_entry_width - 48, 4)
      actor.color = pfconfig.palette.black
      actor.alpha = 0.5
    end
  )
  song_entry_canvas:add_child(step_difficulty_bar_bg)

  step_difficulty_bar_fg = PfRect()
  step_difficulty_bar_fg:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(24, pfconfig.select_music_entry_height - 12)
      actor.align = Vec2(0, 0)
      actor.size = Vec2(pfconfig.select_music_entry_width - 48, 4)
      actor.color = pfconfig.palette.white
    end
  )
  song_entry_canvas:add_child(step_difficulty_bar_fg)
end

-- Music wheel scroll bar
song_wheel_scroll_bar = PfRect()

local SCROLL_BAR_OFFSET = pfconfig.top_bar_height + SEARCH_BAR_HEIGHT
local function tick_scroll_bar(delta_seconds)
  local target_percent = ((selected_step_index - 1) / (#steps - 1))
  if pfmath.is_nan(target_percent) then
    -- This'll happen if there are no steps, such as if a search yields no results
    target_percent = 0
  end

  song_wheel_scroll_bar.y = pfmath.lerp(
    song_wheel_scroll_bar.y,
    ((SCREEN_HEIGHT - SCROLL_BAR_OFFSET) * target_percent) + SCROLL_BAR_OFFSET,
    delta_seconds * 8
  )

  song_wheel_scroll_bar.v_align = pfmath.lerp(
    song_wheel_scroll_bar.v_align,
    target_percent,
    delta_seconds * 8
  )
end

song_wheel_scroll_bar:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 4, 0)
    actor.align = Vec2(0, 0)
    actor.scale = Vec2(4, 40)
    actor.color = pfconfig.palette.white

    pftimer.connect_tick(tick_scroll_bar)
  end
)
song_wheel_scroll_bar:add_hook(
  "EndCommand",
  function(actor)
    pftimer.disconnect_tick(tick_scroll_bar)
  end
)

canvas:add_child(song_wheel_scroll_bar)

-- }}}

-- Song switching {{{

local last_song_wheel_movement = 0
local background_dirty = false
local prev_step = nil
local function switch_song(new_index)
  assert(new_index ~= nil, "new_index was nil!")

  selected_step_index = new_index
  local step = steps[selected_step_index]

  if prev_step == step then return end
  if new_index > #steps then return end

  last_song_wheel_movement = pftimer.elapsed_seconds
  background_dirty = true

  if not prev_step or prev_step.song ~= step.song then
    GAMESTATE:SetCurrentSong(step.song.sm_song)
    song_changed_signal:emit()

    SOUND:StopMusic()
    -- This will loop from check_song_on_tick, from the beginning instead of from the sample start
    SOUND:PlayMusicPart(step.song.preview_music_path, step.song.preview_start, -1, 0, 0, false, true, false)
  end

  if not prev_step or prev_step ~= step then
    step_changed_signal:emit()
  end

  prev_step = step
end

local was_prev_frame_timing_delayed = true
local function check_song_on_tick(delta_seconds)
  local now_time = GAMESTATE:GetCurMusicSeconds()
  local song = GAMESTATE:GetCurrentSong()
  if not song then return end
  local song_length = song:MusicLengthSeconds()

  -- If timing delayed returns false, we give an extra 1 frame delay for now_time to catch up
  if SOUND:IsTimingDelayed() then
    was_prev_frame_timing_delayed = true
  elseif now_time > song_length then
    if was_prev_frame_timing_delayed then
      was_prev_frame_timing_delayed = false
    else
      SOUND:PlayMusicPart(song:GetPreviewMusicPath(), 0, -1, 0, 0, false, true, false)
    end
  end
end

pftimer.connect_tick(check_song_on_tick)

canvas:add_hook(
  "EndCommand",
  function(actor)
    pftimer.disconnect_tick(check_song_on_tick)
  end
)

-- }}}

-- Song progress bar {{{

local song_progress_bar_bg = PfRect()
song_progress_bar_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(24, SCREEN_HEIGHT - 24)
    actor.align = Vec2(0, 0.5)
    actor.scale = Vec2(SCREEN_WIDTH - pfconfig.select_music_entry_width - 24 - 24, 4)
    actor.color = pfconfig.palette.white
    actor.alpha = 0.25
  end
)
canvas:add_child(song_progress_bar_bg)

local song_progress_bar_knob = PfRect()
song_progress_bar_knob:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(24, SCREEN_HEIGHT - 24)
    actor.align = Vec2(0, 0.5)
    actor.scale = Vec2(24, 4)
    actor.color = pfconfig.palette.white
  end
)
canvas:add_child(song_progress_bar_knob)

local song_length_text = PfText{font = pfconfig.fonts.regular_40}
song_length_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - pfconfig.select_music_entry_width - 24, SCREEN_HEIGHT - 24 - 24)
    actor.align = Vec2(1, 0.5)
    actor.scale = 0.6
    actor.color = pfconfig.palette.white
  end
)
canvas:add_child(song_length_text)

local function update_song_progress_on_tick(delta_seconds)
  local now_time = GAMESTATE:GetCurMusicSeconds()
  local song = GAMESTATE:GetCurrentSong()
  if not song then return end
  local song_length = song:MusicLengthSeconds()
  local gameplay_length = song:GetLastSecond()
  local bar_width = SCREEN_WIDTH - pfconfig.select_music_entry_width - 24 - 24

  song_progress_bar_knob.x = pfmath.lerp(
    song_progress_bar_knob.x,
    bar_width * pfmath.clamp(now_time / song_length, 0, 1) + 24,
    delta_seconds * 8
  )

  song_progress_bar_knob.h_align = pfmath.lerp(
    song_progress_bar_knob.h_align,
    pfmath.clamp(now_time / song_length, 0, 1),
    delta_seconds * 8
  )

  -- song_length_text.text = SecondsToMSS(song_length)
  song_length_text.text = SecondsToMSS(gameplay_length)
end

pftimer.connect_tick(update_song_progress_on_tick)
canvas:add_hook(
  "EndCommand",
  function()
    pftimer.disconnect_tick(update_song_progress_on_tick)
  end
)

-- }}}

-- Difficulty text {{{

local song_difficulty_text = PfText{font = pfconfig.fonts.regular_40}
song_difficulty_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(24, SCREEN_HEIGHT - 24 - 24)
    actor.align = Vec2(0, 0.5)
    actor.scale = 0.6
    actor.color = pfconfig.palette.white
  end
)
canvas:add_child(song_difficulty_text)

step_changed_signal:connect(function(self)
  local step = get_current_step()
  song_difficulty_text.text = pflang.format_entry{"difficulty_num", difficulty = step.difficulty}
end)

-- }}}

-- Search {{{

local function get_index_for_song(song, songs_table)
  songs_table = songs_table or steps

  for k, v in pairs(songs_table) do
    if v == song then
      return k
    end
  end

  return nil
end

local function get_index_for_sm_song(sm_song, steps_table)
  steps_table = steps_table or steps

  for k, v in pairs(steps_table) do
    if v.song.sm_song == sm_song then
      return k
    end
  end

  return nil
end

local function get_index_for_sm_step(sm_step, steps_table)
  steps_table = steps_table or steps

  for k, v in pairs(steps_table) do
    if v.sm_step == sm_step then
      return k
    end
  end

  return nil
end

local search_bar = PfTextInput()
search_bar:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - pfconfig.select_music_entry_width, pfconfig.top_bar_height)
    actor.size = Vec2(pfconfig.select_music_entry_width, SEARCH_BAR_HEIGHT)
    actor.placeholder_text = pflang.get_entry("search_with_keybind")

    actor:set_text_no_notify(pfstate.music_select_search_query)
  end
)

local function on_search_changed(query)
  local new_songs = filter_songs(query)
  local new_song_index = get_index_for_song(steps[selected_step_index], new_songs) or 1
  steps = new_songs
  switch_song(new_song_index)

  -- Save the new query
  pfstate.music_select_search_query = query

  update_entries_signal:emit()
end

local function search_update_attributes(query, actor)
  local start_index = 1
  while true do
    local match_start, match_end = query:find("%b[]", start_index)
    if not match_start then break end

    if pfutil.table_contains(SEARCH_SIGILS, search_bar.text:sub(match_start + 1, match_start + 1)) then
      search_bar.text_widget.actor:AddAttribute(
        match_start - 1,
        {
          Length = match_end - match_start + 1,
          Diffuse = pfconfig.palette.pink_light,
        }
      )
    else
      search_bar.text_widget.actor:AddAttribute(
        match_start - 1,
        {
          Length = match_end - match_start + 1,
          Diffuse = pfconfig.palette.gray_light2,
        }
      )
    end

    start_index = match_end + 1
  end
end

local function on_search_char_inserted(c)
  if c == "[" then -- Insert ] when you type [
    search_bar.text = search_bar.text:sub(0, search_bar.insert_point) ..
      "]" ..
      search_bar.text:sub(search_bar.insert_point + 1)

  elseif c == "]" then -- Skip over ] if that's the next character
    if search_bar.text:sub(search_bar.insert_point + 1, search_bar.insert_point + 1) == "]" then
      search_bar.text = search_bar.text:sub(0, search_bar.insert_point - 1)  ..
        search_bar.text:sub(search_bar.insert_point + 1)
    end

  else -- Insert [SIGIL] when typing @, if we're not inside a [...] block
    for _, sigil in pairs(SEARCH_SIGILS) do
      if c == sigil then
        local is_cursor_in_special = false
        local start_index = 1
        while true do
          local match_start, match_end = search_bar.text:find("%b[]", start_index)
          if not match_start then break end

          if match_start < search_bar.insert_point and match_end > search_bar.insert_point then
            is_cursor_in_special = true
          end

          start_index = match_end + 1
        end

        if not is_cursor_in_special then
          search_bar.text = search_bar.text:sub(0, search_bar.insert_point - 1) ..
            "[" .. sigil .. "]" ..
            search_bar.text:sub(search_bar.insert_point + 1)
          search_bar.insert_point = search_bar.insert_point + 1
        end
      end
    end
  end
end

local function on_search_char_deleted(c)
  if c == "[" then
    -- Delete ] if it is the next character
    if search_bar.text:sub(search_bar.insert_point + 1, search_bar.insert_point + 1) == "]" then
      search_bar.text = search_bar.text:sub(0, search_bar.insert_point)  ..
        search_bar.text:sub(search_bar.insert_point + 2)
    end

  elseif c == "]" then
      -- Delete [ if it is the prev character
      if search_bar.text:sub(search_bar.insert_point, search_bar.insert_point) == "[" then
        search_bar.text = search_bar.text:sub(0, search_bar.insert_point - 1)  ..
          search_bar.text:sub(search_bar.insert_point + 1)
        search_bar.insert_point = search_bar.insert_point - 1
      end
  end
end

local function on_search_return_or_escape()
  search_bar.focused = false
  return true
end

search_bar.text_changed_signal:connect(on_search_changed)
search_bar.char_inserted_signal:connect(on_search_char_inserted)
search_bar.char_deleted_signal:connect(on_search_char_deleted)
search_bar.update_attributes_signal:connect(search_update_attributes)
search_bar.on_return_signal:connect(on_search_return_or_escape)
search_bar.on_escape_signal:connect(on_search_return_or_escape)
canvas:add_child(search_bar)

-- }}}

-- Input handling {{{

local function add_step_index(cur_step_index, value)
  if #steps == 0 then return nil end

  local idx = cur_step_index + value
  while idx > #steps do
    idx = idx - #steps
  end
  while idx < 1 do
    idx = idx + #steps
  end

  return idx
end

local function get_next_song_step_index(cur_step_index)
  if #steps == 0 then return nil end

  local cur_step = steps[cur_step_index]
  local cur_song = prev_step.song

  local idx = cur_step_index
  while true do
    idx = idx + 1
    if idx > #steps then
      idx = 1
    end

    local step = steps[idx]
    local song = step.song
    if step == cur_step then return idx end
    if song ~= cur_song then return idx end
  end
end

local function get_prev_song_step_index(cur_step_index)
  if #steps == 0 then return nil end

  local cur_step = steps[cur_step_index]
  local cur_song = prev_step.song

  local idx = cur_step_index
  while true do
    idx = idx - 1
    if idx < 1 then
      idx = #steps
    end

    local step = steps[idx]
    local song = step.song
    if step == cur_step then return idx end
    if song ~= cur_song then
      -- Return the first step in the list, not the last
      local test_idx = idx - 1
      while true do
        if test_idx < 1 then return idx end
        if steps[test_idx].song ~= song then return idx end

        idx = idx - 1
        test_idx = test_idx - 1
      end
    end
  end
end

local function input_handler(event)
  if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
    if pfinput.is_ctrl_down() and event.DeviceInput.button == "DeviceButton_f" then
      search_bar.focused = true

    elseif pfinput.is_ctrl_down() and event.DeviceInput.button == "DeviceButton_r" and pfutil.is_etterna then
      -- Reload songs
      SCREENMAN:SystemMessage(pflang.get_entry("reloading_songs"))
      -- Wait a frame here so the "Reloading songs..." text shows up
      canvas.ctx.timer.run_in_seconds(
        0,
        function()
          SONGMAN:DifferentialReload()
          pfsong.index_songs()
          pfutil.screen_switch("ScreenSelectMusic") -- TODO: Not do a whole screen switch but reload in-place maybe?
          SCREENMAN:SystemMessage(pflang.get_entry("finished_reloading_songs"))
        end
        )

    elseif event.GameButton == "Start" then
      pfutil.play_click_sound()

      local step = steps[selected_step_index]
      GAMESTATE:SetCurrentSteps(1, step.sm_step)

      -- Etterna removed this method
      if not pfutil.is_etterna then
        GAMESTATE:SetCurrentPlayMode("PlayMode_Regular")
      end

      local can, reason = GAMESTATE:CanSafelyEnterGameplay()
      if can then
        pfutil.screen_switch("ScreenGameplay")
      else
        SCREENMAN:SystemMessage("Cannot safely enter gameplay: " .. reason)
      end

    elseif event.DeviceInput.button == "DeviceButton_backspace" then
      pfutil.play_click_sound()
      pfutil.screen_switch("ScreenCustomizeGameplay")

    elseif event.GameButton == "MenuRight" then
      pfutil.play_click_sound()
      local idx = get_next_song_step_index(selected_step_index)
      if idx then switch_song(idx) end

    elseif event.GameButton == "MenuDown" or event.DeviceInput.button == "DeviceButton_mousewheel down" then
      pfutil.play_click_sound()
      local idx = add_step_index(selected_step_index, 1)
      if idx then switch_song(idx) end

    elseif event.GameButton == "MenuLeft" then
      pfutil.play_click_sound()
      local idx = get_prev_song_step_index(selected_step_index)
      if idx then switch_song(idx) end

    elseif event.GameButton == "MenuUp" or event.DeviceInput.button == "DeviceButton_mousewheel up" then
      pfutil.play_click_sound()
      local idx = add_step_index(selected_step_index, -1)
      if idx then switch_song(idx) end

    elseif event.DeviceInput.button == "DeviceButton_pgdn" then
      pfutil.play_click_sound()
      local idx = add_step_index(selected_step_index, 18)
      if idx then switch_song(idx) end

    elseif event.DeviceInput.button == "DeviceButton_pgup" then
      pfutil.play_click_sound()
      local idx = add_step_index(selected_step_index, -18)
      if idx then switch_song(idx) end

    elseif event.DeviceInput.button == "DeviceButton_home" then
      if #steps > 0 then
        switch_song(1)
      end
      pfutil.play_click_sound()

    elseif event.DeviceInput.button == "DeviceButton_end" then
      if #steps > 0 then
        switch_song(#steps)
      end
      pfutil.play_click_sound()

    elseif event.GameButton == "Back" and event.type == "InputEventType_FirstPress" then
      pfutil.screen_switch("ScreenTitleMenu")

    -- else
    --   SCREENMAN:SystemMessage(event.GameButton .. " / " .. event.DeviceInput.button)
    end
  end
end

canvas:add_hook(
  "OnCommand",
  function(actor)
    pfinput.add_input_handler(input_handler)

    -- TODO: Localize
    pfutil.key_hints.add(pflang.get_entry("hint_select_music"), "Left    Right")
    pfutil.key_hints.add(pflang.get_entry("hint_select_difficulty"), "Up    Down")
    pfutil.key_hints.add(pflang.get_entry("hint_customize_gameplay"), "Backspace")
  end
)

-- }}}

local function check_rclick_scroll()
  if pfinput.is_key_down("DeviceButton_right mouse button") then
    local idx = pfmath.round(
      pfmath.clamp(
        (pfutil.get_mouse_position().y - SCROLL_BAR_OFFSET) / (SCREEN_HEIGHT - SCROLL_BAR_OFFSET),
        0,
        1
      ) * (#steps - 1)
    ) + 1
    switch_song(idx)
  end
end

local prev_song = nil
canvas:add_hook(
  "OnCommand",
  function(actor)
    -- Run next frame - we need to do this for music to play for *some* reason
    actor.ctx.timer.run_in_seconds(
      0,
      function()
        local p1_step = GAMESTATE:GetCurrentSteps(1)
        if p1_step then
          local idx = get_index_for_sm_step(p1_step)
          if idx ~= nil then
            switch_song(idx)
          else
            switch_song(1)
          end
        else
          local song = GAMESTATE:GetCurrentSong()
          if song then
            local idx = get_index_for_sm_song(song)
            if idx ~= nil then
              switch_song(idx)
            else
              switch_song(1)
            end
          else
            switch_song(1)
          end
        end
      end
    )

    canvas:set_update_function(
      function(self, delta_seconds)
        if background_dirty and pftimer.elapsed_seconds > last_song_wheel_movement + 0.5 then
          if not steps[selected_step_index] then return end

          local prev_background_path
          if not prev_song then
            prev_background_path = MISSING_BACKGROUND_PATH
          else
            prev_background_path = prev_song.background_path or MISSING_BACKGROUND_PATH
          end

          local new_background_path
          if steps[selected_step_index] then
            new_background_path = steps[selected_step_index].song.background_path or MISSING_BACKGROUND_PATH
          else
            new_background_path = MISSING_BACKGROUND_PATH
          end

          song_bg_lower:load_texture_background(prev_background_path)
          song_bg_lower:scale_to_cover(Vec2(0, 0), Vec2(SCREEN_WIDTH, SCREEN_BOTTOM))

          song_bg_upper:load_texture_background(new_background_path)
          song_bg_upper:scale_to_cover(Vec2(0, 0), Vec2(SCREEN_WIDTH, SCREEN_BOTTOM))
          song_bg_upper.alpha = 0
          song_bg_upper:tween_linear(0.5)
          song_bg_upper.alpha = 1

          background_dirty = false
          prev_song = steps[selected_step_index].song
        end

        check_rclick_scroll()
      end
    )
  end
)

return canvas:make_actor()
