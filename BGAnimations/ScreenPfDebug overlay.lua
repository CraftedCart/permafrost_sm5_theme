local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfsignal = require("permafrost.pfsignal")
local pfinput = require("permafrost.pfinput")
local pfsong = require("permafrost.pfsong")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfVBox = pfwidget.PfVBox
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput
local PfButton = pfwidget.PfButton

local function object_to_string(obj)
  if type(obj) == "table" then
    return pfutil.table_to_string(obj)
  else
    return tostring(obj)
  end
end

local canvas = PfCanvas()
canvas:add_background()

local vbox = PfVBox()
vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, pfconfig.top_bar_height)
    actor.size = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT - pfconfig.top_bar_height)
    actor.spacing = Vec2(20, 4)
  end
)
canvas:add_child(vbox)

-- Add "exit to screen" buttons
for k, screen_name in ipairs{
  "ScreenSelectMusic",
  "ScreenEvaluation",
} do
  local screen_button = PfButton()
  screen_button:add_hook(
    "InitCommand",
    function(actor)
      actor.height = 32
      actor.text = "Exit to " .. screen_name

      if k == 1 then actor.focused = true end
    end
  )
  screen_button.clicked_signal:connect(function()
    pfutil.screen_switch(screen_name)
    pfwidget.set_focus(nil)
  end)
  vbox:add_child(screen_button)
end

local screen_name_text_input = PfTextInput()
screen_name_text_input:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 48
    actor.placeholder_text = "Jump to screen name"
  end
)
screen_name_text_input.on_return_signal:connect(function()
  if THEME:HasMetric(screen_name_text_input.text, "Class") or SCREENMAN:ScreenClassExists(screen_name_text_input.text) then
    pfutil.screen_switch(screen_name_text_input.text)
  else
    SCREENMAN:SystemMessage("Screen does not exist")
  end
end)
vbox:add_child(screen_name_text_input)

local eval_text_input = PfTextInput()
eval_text_input:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 48
    actor.placeholder_text = "Eval Lua"
  end
)
eval_text_input.on_return_signal:connect(function()
  local code = eval_text_input.text

  local func, err = load("return (" .. code .. ")", "Lua console (statement)", "t")
  if func then
    local _, ret = pcall(func)
    SCREENMAN:SystemMessage(object_to_string(ret))
  else
    -- If it fails, try see if it's an statement
    local stmt_func, stmt_err = load(code, "Lua console (statement)", "t")
    if stmt_func then
      local _, ret = pcall(stmt_func)
      SCREENMAN:SystemMessage(object_to_string(ret))
    else
      SCREENMAN:SystemMessage(stmt_err .. "\n" .. err)
    end
  end
end)
vbox:add_child(eval_text_input)

return canvas:make_actor()
