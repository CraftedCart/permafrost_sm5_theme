local pfconfig = require("permafrost.pfconfig")
local pfutil = require("permafrost.pfutil")

return Def.Quad {
  InitCommand = function(self)
    self:xy(SCREEN_CENTER_X, SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH, SCREEN_HEIGHT)
  end,
  OnCommand = function(self)
    self:diffuse(pfconfig.colors.screen_background)
      :diffusealpha(0)

      -- :linear(0.1)
      -- :diffusealpha(1)

    self:sleep(0.1)
  end,
  OffCommand = function(self)
    pfutil.pre_screen_switched_signal:emit()
  end,
}
