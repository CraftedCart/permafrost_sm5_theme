local pfutil = require("permafrost.pfutil")

-- TODO: Disable transitions for this screen
return Def.ActorFrame {
  OnCommand = function(self)
    pfutil.screen_switch("ScreenSelectMusic")
  end,
}
