local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfinput = require("permafrost.pfinput")
local pfnotefieldconfig = require("permafrost.pfnotefieldconfig")
local screen_gameplay = require("permafrost.screen.screen_gameplay")

local PfNoteFieldConfig = pfnotefieldconfig.PfNoteFieldConfig

-- We need to do this immediately, not in an InitCommand
local nf_config = PfNoteFieldConfig()
nf_config:load_from_player(1)
nf_config:apply_to_player(1)

local ui = screen_gameplay.make(nf_config)
local canvas = ui.root_widget
canvas:screen_fade_in_out()

-- Canvas hooks {{{

canvas:add_hook(
  "JudgmentMessageCommand",
  function(actor, args)

    if args.TapNoteOffset then -- If this is a tap...
      ui.tap_judgment_signal:emit{
        tap_note_offset = args.TapNoteOffset,
        tap_note_score = args.TapNoteScore,
      }
    else -- It's a hold
      ui.hold_judgment_signal:emit{
        hold_note_offset = args.HoldNoteOffset,
        hold_note_score = args.HoldNoteScore,
      }
    end
  end
)

canvas:add_hook(
  "ComboChangedMessageCommand",
  function(actor, args)
    local stats = args.PlayerStageStats

    if args.OldCombo > 30 and stats:GetCurrentCombo() == 0 then
      ui.combo_break_sounds[ui.current_combo_break_sound_idx]:play()
      ui.current_combo_break_sound_idx = ui.current_combo_break_sound_idx + 1
      if ui.current_combo_break_sound_idx > ui.max_combo_break_sounds then
        ui.current_combo_break_sound_idx = 1
      end
    end

    ui.combo_changed_signal:emit{
      combo = stats:GetCurrentCombo(),
      old_combo = args.OldCombo,
      w1_full_combo = stats:FullComboOfScore("TapNoteScore_W1"),
      w2_full_combo = stats:FullComboOfScore("TapNoteScore_W2"),
      w3_full_combo = stats:FullComboOfScore("TapNoteScore_W3"),
    }
  end
)

canvas:add_hook(
  "LifeChangedMessageCommand",
  function(actor, args)
    ui.life_changed_signal:emit{
      life = args.LifeMeter:GetLife(),
    }
  end
)

local function handle_input(event)
  if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
    if pfinput.is_ctrl_down() and event.DeviceInput.button == "DeviceButton_r" then
      -- Restart
      pfutil.screen_switch_immediate("ScreenGameplay")
    end
  end
end

canvas:add_hook(
  "OnCommand",
  function(actor, args)
    pfinput.add_input_handler(handle_input)
  end
)

-- }}}

return canvas:make_actor()
