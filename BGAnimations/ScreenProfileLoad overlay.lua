local pfwidget = require("permafrost.pfwidget")
local pfconfig = require("permafrost.pfconfig")

local PfCanvas = pfwidget.PfCanvas

local canvas = PfCanvas()
canvas:add_background()

-- Loading profiles text
canvas:add_child(
  pfconfig.fonts.light_40 .. {
    InitCommand = function(self)
      self:xy(SCREEN_CENTER_X, SCREEN_CENTER_Y)
        :zoom(1)
        :valign(0.5)
        :halign(0.5)
        :diffuse(pfconfig.colors.title_text)

      local text = THEME:GetString("ScreenProfileLoad", "LoadingProfiles")
      self:settext(text)
    end,
  }
  )

canvas:add_child(Def.Actor {
    BeginCommand = function(self)
      -- ...why is this even a thing in the default theme
      -- if SCREENMAN:GetTopScreen():HaveProfileToLoad() then
      --   self:sleep(1)
      -- end

      self:queuecommand("Load")
    end,

    LoadCommand = function()
      SCREENMAN:GetTopScreen():Continue()
    end,
  }
  )

return canvas:make_actor()
