local pfwidget = require("permafrost.pfwidget")
local pfanim = require("permafrost.pfanim")
local pfkeyhintwidget = require("permafrost.widget.pfkeyhintwidget")
local pffilepicker = require("permafrost.widget.pffilepicker")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfinput = require("permafrost.pfinput")
local pfutil = require("permafrost.pfutil")
local pfmath = require("permafrost.pfmath")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfuserconfig = require("permafrost.pfuserconfig")
local pfplatform = require("permafrost.pfplatform")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfAnchored = pfwidget.PfAnchored
local PfOverlay = pfwidget.PfOverlay
local PfVBox = pfwidget.PfVBox
local PfHBox = pfwidget.PfHBox
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput
local PfButton = pfwidget.PfButton
local EFocusDirection = pfwidget.EFocusDirection
local ESizeMode = pfwidget.ESizeMode
local PfFilePicker = pffilepicker.PfFilePicker
local PfKeyHintWidget = pfkeyhintwidget.PfKeyHintWidget

local canvas = PfCanvas()

-- Login panel {{{

local profile_all_vbox = PfVBox()

local profile_panel_anchored = PfAnchored()
profile_panel_anchored:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 512, pfconfig.top_bar_height - 64)
    actor.width = 512
    actor.alpha = 0

    pftimer.update_signal:connect(
      function(delta_seconds)
        actor.height = profile_all_vbox.height
      end
    )
  end
)

local profile_panel_bg = PfRect()
profile_panel_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.anchors.max = Vec2(1, 1)
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.panel_background
  end
)
profile_panel_anchored:add_child(profile_panel_bg)

local profile_panel_bg_shadow = PfRect()
profile_panel_bg_shadow:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.anchors.min = Vec2(0, 1)
    actor.slot.anchors.max = Vec2(1, 1)
    actor.slot.size.y = 8
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.shadow
    actor.fade_bottom = 1
  end
)
profile_panel_anchored:add_child(profile_panel_bg_shadow)

local profile_panel_overlay = PfOverlay()
profile_panel_overlay:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, pfconfig.top_bar_height - 64)
    actor.min_width = 512
    -- actor.alpha = 0
  end
)
profile_panel_anchored:add_child(profile_panel_overlay)

profile_all_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(20, 0)
  end
)
profile_panel_overlay:add_child(profile_all_vbox)

local login_vbox = PfVBox()
login_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 0)
  end
)
profile_all_vbox:add_child(login_vbox)

local login_title_text = PfText{font = pfconfig.fonts.light_40}
login_title_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.width_mode = ESizeMode.KEEP
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.margins.vert = 20
    actor.text = pflang.format_entry{"login_to_online_platform", platform_name = pfconfig.online_platform.name}
  end
)
login_vbox:add_child(login_title_text)

local login_ask_method_vbox = PfVBox()
login_ask_method_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 8)
  end
)
login_vbox:add_child(login_ask_method_vbox)

local login_username_password_button = PfButton()
login_username_password_button:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 32
    actor.text = pflang.get_entry("login_with_username_password")
  end
)
login_ask_method_vbox:add_child(login_username_password_button)

local login_import_til_death_button = PfButton()
login_import_til_death_button:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 32
    actor.text = pflang.get_entry("login_import_from_til_death")
  end
)
login_ask_method_vbox:add_child(login_import_til_death_button)

local login_waiting_vbox = PfVBox()
login_waiting_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 0)
  end
)
profile_all_vbox:add_child(login_waiting_vbox)

local login_waiting_text = PfText{font = pfconfig.fonts.light_40}
login_waiting_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.width_mode = ESizeMode.KEEP
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.margins.vert = 20
    actor.text = pflang.get_entry("connecting")
    actor.visible = false
  end
)
login_waiting_vbox:add_child(login_waiting_text)

local login_username_password_vbox = PfVBox()
login_username_password_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 8)
    actor.visible = false
  end
)
login_vbox:add_child(login_username_password_vbox)

local login_username_text_input = PfTextInput()
login_username_text_input:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 48
    actor.placeholder_text = pflang.get_entry("username")
  end
)
login_username_password_vbox:add_child(login_username_text_input)

local login_password_text_input = PfTextInput()
login_password_text_input:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 48
    actor.placeholder_text = pflang.get_entry("password")
    actor.is_password = true
  end
)
login_username_password_vbox:add_child(login_password_text_input)

local login_username_password_confirm_button = PfButton()
login_username_password_confirm_button:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 32
    actor.text = pflang.get_entry("log_in")
  end
)
login_username_password_vbox:add_child(login_username_password_confirm_button)

local login_username_password_back_button = PfButton()
login_username_password_back_button:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 32
    actor.text = pflang.get_entry("back")
  end
)
login_username_password_vbox:add_child(login_username_password_back_button)

local profile_vbox = PfVBox()
profile_vbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 8)
    actor.visible = false
  end
)
profile_all_vbox:add_child(profile_vbox)

local profile_username_text = PfText{font = pfconfig.fonts.light_40}
profile_username_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.width_mode = ESizeMode.KEEP
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.margins.vert = 20
    actor.text = ""
  end
)
profile_vbox:add_child(profile_username_text)

local profile_rank_text = PfText{font = pfconfig.fonts.light_40}
profile_rank_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.width_mode = ESizeMode.KEEP
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = ""
  end
)
profile_vbox:add_child(profile_rank_text)

local skillset_titles_hbox = PfHBox()
skillset_titles_hbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(0, 0)
    actor.margins.top = 20
    actor.margins.bottom = 16
  end
)
profile_vbox:add_child(skillset_titles_hbox)

local profile_skillset_title_name_text = PfText{font = pfconfig.fonts.regular_40}
profile_skillset_title_name_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = pflang.get_entry("skillset")
  end
)
skillset_titles_hbox:add_child(profile_skillset_title_name_text)

local profile_skillset_rating_name_text = PfText{font = pfconfig.fonts.regular_40}
profile_skillset_rating_name_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = pflang.get_entry("rating")
  end
)
skillset_titles_hbox:add_child(profile_skillset_rating_name_text)

local profile_skillset_title_rank_text = PfText{font = pfconfig.fonts.regular_40}
profile_skillset_title_rank_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = pflang.get_entry("rank")
  end
)
skillset_titles_hbox:add_child(profile_skillset_title_rank_text)

local skillsets_hbox = PfHBox()
skillsets_hbox:add_hook(
  "InitCommand",
  function(actor)
    actor.spacing = Vec2(80, 0)
    actor.margins.bottom = 20
  end
)
profile_vbox:add_child(skillsets_hbox)

local profile_skillset_names_text = PfText{font = pfconfig.fonts.light_40}
profile_skillset_names_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = ""
  end
)
skillsets_hbox:add_child(profile_skillset_names_text)

local profile_skillset_ratings_text = PfText{font = pfconfig.fonts.light_40}
profile_skillset_ratings_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = ""
  end
)
skillsets_hbox:add_child(profile_skillset_ratings_text)

local profile_skillset_ranks_text = PfText{font = pfconfig.fonts.light_40}
profile_skillset_ranks_text:add_hook(
  "InitCommand",
  function(actor)
    actor.slot.height_mode = ESizeMode.KEEP
    actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.45
    actor.text = ""
  end
)
skillsets_hbox:add_child(profile_skillset_ranks_text)

local logout_button = PfButton()
logout_button:add_hook(
  "InitCommand",
  function(actor)
    actor.height = 32
    actor.text = pflang.get_entry("log_out")
  end
)
profile_vbox:add_child(logout_button)

canvas:add_child(profile_panel_anchored)

local profile_panel_visible = false

local function show_profile_panel()
  profile_panel_visible = true

  canvas.ctx.anim{
    start_val = profile_panel_anchored.y,
    end_val = pfconfig.top_bar_height,
    prop_table = profile_panel_anchored,
    prop_name = "y",
    duration = 0.2,
    easing = pfeasing.out_expo,
  }

  canvas.ctx.anim{
    start_val = profile_panel_anchored.alpha,
    end_val = 1,
    prop_table = profile_panel_anchored,
    prop_name = "alpha",
    duration = 0.2,
    easing = pfeasing.out_expo,
  }

  profile_panel_overlay:focus()
end

local function hide_profile_panel()
  profile_panel_visible = false
  pfwidget.set_focus(nil)

  canvas.ctx.anim{
    start_val = profile_panel_anchored.y,
    end_val = pfconfig.top_bar_height - 64,
    prop_table = profile_panel_anchored,
    prop_name = "y",
    duration = 0.2,
    easing = pfeasing.out_expo,
  }

  canvas.ctx.anim{
    start_val = profile_panel_anchored.alpha,
    end_val = 0,
    prop_table = profile_panel_anchored,
    prop_name = "alpha",
    duration = 0.2,
    easing = pfeasing.out_expo,
  }
end

local function toggle_profile_panel()
  if profile_panel_visible then
    hide_profile_panel()
  else
    show_profile_panel()
  end
end

local function login_ask_username_and_password()
  login_ask_method_vbox.visible = false
  login_username_password_vbox.visible = true
  login_username_text_input:focus()
end
login_username_password_button.clicked_signal:connect(login_ask_username_and_password)

local function login_back_username_and_password()
  login_username_password_vbox.visible = false
  login_ask_method_vbox.visible = true
  login_ask_method_vbox:focus()
end
login_username_password_back_button.clicked_signal:connect(login_back_username_and_password)

local function login_with_username_and_password()
  if
    #login_username_text_input.text == 0 or
    #login_password_text_input.text == 0
  then
    return true
  end

  DLMAN:Login(login_username_text_input.text, login_password_text_input.text)
  login_password_text_input.text = ""
  login_waiting_text.visible = true
  login_vbox.visible = false

  return true
end
login_username_password_confirm_button.clicked_signal:connect(login_with_username_and_password)
login_username_text_input.on_return_signal:connect(login_with_username_and_password)
login_password_text_input.on_return_signal:connect(login_with_username_and_password)

local function import_login_from_til_death()
  local chunk = loadfile("Save/LocalProfiles/00000000/Til Death_settings/playerConfig.lua")
  if chunk then
    local etterna_player = chunk()
    DLMAN:LoginWithToken(etterna_player.UserName, etterna_player.PasswordToken)
    login_waiting_text.visible = true
    login_vbox.visible = false
  else
    SCREENMAN:SystemMessage("Could not load Save/LocalProfiles/00000000/Til Death_settings/playerConfig.lua")
  end
end
login_import_til_death_button.clicked_signal:connect(import_login_from_til_death)

local function logout()
  DLMAN:Logout()
  pfuserconfig.private_config.etterna.token = nil
  pfuserconfig.save_to_file()

  login_vbox.visible = true
  profile_vbox.visible = false
  if profile_panel_visible then
    login_vbox:focus()
  end
end
logout_button.clicked_signal:connect(logout)

pfinput.add_overlay_input_handler(
  function(event)
    if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
      if pfutil.is_etterna then
        if pfinput.is_ctrl_down() and pfinput.is_alt_down() and event.DeviceInput.button == "DeviceButton_p" then
          toggle_profile_panel()
          return true
        elseif profile_panel_visible and event.DeviceInput.button == "DeviceButton_escape" then
          hide_profile_panel()
          return true
        end
      end
    end

    return false
  end
)

-- }}}

-- General focusing stuff {{{

pfinput.add_overlay_input_handler(
  function(event)
    local focused = pfwidget.get_focus()
    if focused then
      local handled = focused:handle_input_event(event)
      if handled then return true end
    end

    if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
      if event.DeviceInput.button == "DeviceButton_tab" then
        local dir
        if pfinput.is_shift_down() then
          dir = EFocusDirection.PREV
        else
          dir = EFocusDirection.NEXT
        end

        return pfwidget.focus_next(dir)

      elseif event.DeviceInput.button == "DeviceButton_up" then
        return pfwidget.focus_next(EFocusDirection.UP)
      elseif event.DeviceInput.button == "DeviceButton_down" then
        return pfwidget.focus_next(EFocusDirection.DOWN)
      elseif event.DeviceInput.button == "DeviceButton_left" then
        return pfwidget.focus_next(EFocusDirection.LEFT)
      elseif event.DeviceInput.button == "DeviceButton_right" then
        return pfwidget.focus_next(EFocusDirection.RIGHT)
      end
    end

    return false
  end
)

-- }}}

-- Top bar {{{

local top_bar_canvas = PfCanvas()

local top_bar_bg = PfRect()
top_bar_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, 0)
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.top_bar_background
    actor.size = Vec2(SCREEN_WIDTH, pfconfig.top_bar_height)
  end
)
top_bar_canvas:add_child(top_bar_bg)

local top_bar_bg_shadow = PfRect()
top_bar_bg_shadow:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, pfconfig.top_bar_height)
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.shadow
    actor.fade_bottom = 1
    actor.size = Vec2(SCREEN_WIDTH, 8)
  end
)
top_bar_canvas:add_child(top_bar_bg_shadow)

-- Online profile {{{

local user_text = PfText{font = pfconfig.fonts.light_40}
user_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 640, 12)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.text = ""
  end
)
top_bar_canvas:add_child(user_text)

local profile_keybind_text = PfText{font = pfconfig.fonts.regular_40}
profile_keybind_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(20, 16)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 0.5")
    actor.scale = 0.4
    actor.text = pfutil.is_etterna and pflang.get_entry("profile_keybind") or ""
  end
)
top_bar_canvas:add_child(profile_keybind_text)

local online_skill_text
local online_rank_text

online_skill_text = PfText{font = pfconfig.fonts.regular_40}
online_skill_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(20, 6)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.4
    actor.text = ""
  end
)
top_bar_canvas:add_child(online_skill_text)

online_rank_text = PfText{font = pfconfig.fonts.regular_40}
online_rank_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(20, 24)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.4
    actor.text = ""
  end
)
top_bar_canvas:add_child(online_rank_text)

-- }}}

local key_hints_hbox = PfHBox()
key_hints_hbox:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, 0)
    actor.width = SCREEN_WIDTH - 640
    actor.spacing = Vec2(0, 0)
  end
)
top_bar_canvas:add_child(key_hints_hbox)

local key_hint_widgets = {}
local next_key_hint_index = 1 -- Index of the next key hint to use when one is added
for i = 1, 8 do
  local key_hint = PfKeyHintWidget()
  key_hint:add_hook(
    "InitCommand",
    function(actor)
      actor.slot.height_mode = ESizeMode.KEEP
      actor.slot.distribute_mode = pfwidget.EDistributeMode.FIT
    end
    )
  key_hints_hbox:add_child(key_hint)
  table.insert(key_hint_widgets, key_hint)
end

local function clear_key_hints()
  for _, v in pairs(key_hint_widgets) do
    -- Stop any fade-in animations
    pfanim.stop_animating_table(v)

    -- Fade out
    pfanim.animate{
      start_val = v.alpha,
      end_val = 0,
      prop_table = v,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }
  end
  next_key_hint_index = 1
end

local function add_key_hint(action_text, key_text)
  if next_key_hint_index > #key_hint_widgets then
    error("Too many key hints!")
  end

  local key_hint = key_hint_widgets[next_key_hint_index]
  key_hint:set_hint(action_text, key_text)

  pftimer.run_in_seconds(0.05 * (next_key_hint_index - 1), function()
    pfanim.animate{
      start_val = 0,
      end_val = 1,
      prop_table = key_hint,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }
  end)

  next_key_hint_index = next_key_hint_index + 1
end

pfutil.pre_screen_switched_signal:connect(function()
  clear_key_hints()
end)

pfutil.key_hints.on_added_signal:connect(function(args)
  add_key_hint(args.action_text, args.key_text)
end)

-- Clock {{{

local time_text = PfText{font = pfconfig.fonts.light_40}
time_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 12, 12)
    actor.align = Vec2(1, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.text = ""
  end
)
top_bar_canvas:add_child(time_text)

-- }}}

top_bar_canvas:add_hook(
  "InitCommand",
  function(actor)
    actor:set_update_function(
      function(self, delta_seconds)
        if pfutil.is_etterna then
          if DLMAN:IsLoggedIn() then
            user_text.text = DLMAN:GetUsername()
            online_skill_text.text = pflang.format_entry{
              "online_and_local_rating",
              online_rating = DLMAN:GetSkillsetRating("Overall"),
              local_rating = PROFILEMAN:GetProfile(1):GetPlayerRating(),
            }

            local global_rank_text = pflang.format_entry{"global_rank", rank = DLMAN:GetSkillsetRank("Overall")}
            online_rank_text.text = global_rank_text

            profile_username_text.text = pfconfig.online_platform.name .. ": " .. DLMAN:GetUsername()
            profile_rank_text.text = global_rank_text

            local skillset_name_texts = {}
            local skillset_rating_texts = {}
            local skillset_rank_texts = {}
            for _, v in ipairs(pfconfig.skill_sets) do
              table.insert(skillset_name_texts, pflang.get_entry(v.lang_key))
              table.insert(skillset_rating_texts, string.format("%05.2f", DLMAN:GetSkillsetRating(v.id)))
              table.insert(skillset_rank_texts, "#" .. DLMAN:GetSkillsetRank(v.id))
            end

            profile_skillset_names_text.text = table.concat(skillset_name_texts, "\n")
            profile_skillset_ratings_text.text = table.concat(skillset_rating_texts, "\n")
            profile_skillset_ranks_text.text = table.concat(skillset_rank_texts, "\n")

          else
            -- Not logged in
            user_text.text = pflang.format_entry{
              "login_to_online_platform",
              platform_name = pfconfig.online_platform.name
            }
            online_skill_text.text = ""
            online_rank_text.text = ""
          end

          profile_keybind_text.x = user_text.x + user_text.width_scaled + 4
          online_skill_text.x = profile_keybind_text.x + profile_keybind_text.width_scaled + 20
          online_rank_text.x = profile_keybind_text.x + profile_keybind_text.width_scaled + 20
        else
          user_text.text = PROFILEMAN:GetProfile(1):GetDisplayName()
        end

        time_text.text = pfplatform.get_current_time_formatted()
      end
    )
  end
)

canvas:add_child(top_bar_canvas)

-- }}}

-- Reloading songs {{{

local song_reload_canvas = PfCanvas()

local SONG_RELOAD_BOX_HEIGHT = 112
local song_reload_bg = PfRect()
song_reload_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, SCREEN_CENTER_Y)
    actor.align = Vec2(0, 0.5)
    actor.color = pfconfig.colors.top_bar_background -- TODO: Not reuse colors
    actor.size = Vec2(SCREEN_WIDTH, SONG_RELOAD_BOX_HEIGHT)
  end
)
song_reload_canvas:add_child(song_reload_bg)

local song_reload_bg_shadow_top = PfRect()
song_reload_bg_shadow_top:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2))
    actor.align = Vec2(0, 1)
    actor.color = pfconfig.colors.shadow
    actor.fade_top = 1
    actor.size = Vec2(SCREEN_WIDTH, 8)
  end
)
song_reload_canvas:add_child(song_reload_bg_shadow_top)

local song_reload_bg_shadow_bottom = PfRect()
song_reload_bg_shadow_bottom:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, SCREEN_CENTER_Y + (SONG_RELOAD_BOX_HEIGHT / 2))
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.shadow
    actor.fade_bottom = 1
    actor.size = Vec2(SCREEN_WIDTH, 8)
  end
)
song_reload_canvas:add_child(song_reload_bg_shadow_bottom)

local song_reload_title_bg = PfRect()
song_reload_title_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2))
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.palette.purple -- TODO: Not use preset colors
    actor.size = Vec2(SCREEN_WIDTH, 64)
  end
)
song_reload_canvas:add_child(song_reload_title_bg)

local song_reload_title_shadow_bottom = PfRect()
song_reload_title_shadow_bottom:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2) + 64)
    actor.align = Vec2(0, 0)
    actor.color = pfconfig.colors.shadow
    actor.fade_bottom = 1
    actor.size = Vec2(SCREEN_WIDTH, 8)
  end
)
song_reload_canvas:add_child(song_reload_title_shadow_bottom)

local song_reload_text = PfText{font = pfconfig.fonts.light_40}
song_reload_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2) + 16)
    actor.align = Vec2(0.5, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 1
    actor.text = pflang.get_entry("reloading_songs")
  end
)
song_reload_canvas:add_child(song_reload_text)

local song_reload_name_text = PfText{font = pfconfig.fonts.light_40}
song_reload_name_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(24, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2) + 80)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.text = "Pack name: Song name"
  end
)
song_reload_canvas:add_child(song_reload_name_text)

local song_reload_count_text = PfText{font = pfconfig.fonts.light_40}
song_reload_count_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_WIDTH - 24, SCREEN_CENTER_Y - (SONG_RELOAD_BOX_HEIGHT / 2) + 80)
    actor.align = Vec2(1, 0)
    actor.color = color("1, 1, 1, 1")
    actor.scale = 0.6
    actor.text = "0"
  end
)
song_reload_canvas:add_child(song_reload_count_text)

-- The number of songs reloaded thus far...
local song_reload_count = 0

song_reload_canvas:add_hook(
  "InitCommand",
  function(actor)
    actor.visible = false
  end
  )

song_reload_canvas:add_hook(
  "DFRStartedMessageCommand",
  function(actor)
    actor.visible = true
    song_reload_count = 0
  end
  )

song_reload_canvas:add_hook(
  "DFRUpdateMessageCommand",
  function(actor, args)
    song_reload_count = song_reload_count + 1

    -- Flippin Etterna leaving text handling to the C++ core and just exposing your loading message here in args.txt
    -- Anyways, args.txt is in the format Loading:\nPackName\nSongName
    -- So we split that on newlines to figure out our pack/song names

    local lines = {}
    for line in args.txt:gmatch("([^\n]*)\n?") do
      table.insert(lines, line)
    end

    local pack_name = lines[2]
    local song_name = lines[3]

    song_reload_name_text.text = pack_name .. ": " .. song_name
    song_reload_count_text.text = song_reload_count
  end
  )

song_reload_canvas:add_hook(
  "DFRFinishedMessageCommand",
  function(actor)
    actor.visible = false
  end
  )

canvas:add_child(song_reload_canvas)

-- }}}

-- System message {{{

local message_bg = PfRect()
message_bg:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(0, 0)
    actor.align = Vec2(0, 0)
    actor.color = color("0, 0, 0, 0")
    actor.size = Vec2(SCREEN_WIDTH, 30)
  end
)
canvas:add_child(message_bg)

local message_text = PfText{font = pfconfig.fonts.regular_40}
message_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(20, 10)
    actor.align = Vec2(0, 0)
    actor.color = color("1, 1, 1, 0")
    actor.scale = 0.4
    actor.actor:maxwidth(SCREEN_WIDTH - 20)
  end
)
canvas:add_child(message_text)

local delayed_hide_handle = nil
local bg_anim_handle = nil
local text_anim_handle = nil

local function hide_message()
  delayed_hide_handle = nil

  bg_anim_handle = canvas.ctx.anim{
    start_val = message_bg.alpha,
    end_val = 0,
    prop_table = message_bg,
    prop_name = "alpha",
    duration = 0.15,
    easing = pfeasing.linear,
  }

  text_anim_handle = canvas.ctx.anim{
    start_val = message_text.alpha,
    end_val = 0,
    prop_table = message_text,
    prop_name = "alpha",
    duration = 0.15,
    easing = pfeasing.linear,
  }
end

canvas:add_hook(
  "SystemMessageMessageCommand",
  function(actor, params)
    message_bg.alpha = 0.85
    message_text.alpha = 1
    message_text.text = params.Message

    if delayed_hide_handle then pftimer.cancel_delayed_action(delayed_hide_handle) end
    canvas.ctx.anim.stop_animating_table(bg_anim_handle)
    canvas.ctx.anim.stop_animating_table(text_anim_handle)

    delayed_hide_handle = canvas.ctx.timer.run_in_seconds(3, hide_message)
  end
)

canvas:add_hook(
  "HideSystemMessageMessageCommand",
  function(actor)
    message_bg.alpha = 0
    message_text.alpha = 0

    if delayed_hide_handle then pftimer.cancel_delayed_action(delayed_hide_handle) end
    canvas.ctx.anim.stop_animating_table(bg_anim_handle)
    canvas.ctx.anim.stop_animating_table(text_anim_handle)
  end
)

-- File picker
-- local file_picker = PfFilePicker()
-- canvas:add_child(file_picker)

---------------- Canvas init ----------------
canvas:add_hook(
  "InitCommand",
  function(actor)
    -- Load user config
    pfuserconfig.load_from_file()

    -- Init profiles
    if GAMESTATE:HaveProfileToLoad() then
      GAMESTATE:LoadProfiles(true)
    end

    -- Log in to Etterna online
    if pfutil.is_etterna then
      if
        (not DLMAN:IsLoggedIn()) and
        pfuserconfig.private_config.etterna.user_name and
        pfuserconfig.private_config.etterna.token
      then
        DLMAN:LoginWithToken(pfuserconfig.private_config.etterna.user_name, pfuserconfig.private_config.etterna.token)
      else
        login_vbox.visible = false
        profile_vbox.visible = true
      end
    end

    -- Animate in the top bar
    canvas.ctx.anim{
      start_val = -pfconfig.top_bar_height - 8,
      end_val = 0,
      prop_table = top_bar_canvas,
      prop_name = "y",
      duration = 0.15,
      easing = pfeasing.out_expo,
    }

    -- Handle ticking
    local update_func
    if pfutil.is_etterna then
      update_func = function(self, delta_seconds)
        -- Etterna seems to update at 60 Hz
        pftimer.update(1 / 60)
      end
    else
      update_func = function(self, delta_seconds)
        pftimer.update(delta_seconds)
      end
    end

    canvas:set_update_function(update_func)
  end
)

canvas:add_hook(
  "ScreenChangedMessageCommand",
  function(actor)
    pfutil.screen_switched_signal:emit()
  end
)

-- OnlineUpdateMessageCommand = function(self)
--   BroadcastIfActive("UpdateRanking")
-- end,

canvas:add_hook(
  "LoginMessageCommand",
  function(self)
    login_waiting_text.visible = false
    login_vbox.visible = true
    -- Workaround for Etterna issue (LoginMessageCommand is broadcast for a *failed* login...)
    if not DLMAN:IsLoggedIn() then
      SCREENMAN:SystemMessage(pflang.get_entry("login_failed"))
      return
    end

    -- Save successful login details
    pfuserconfig.private_config.etterna.user_name = DLMAN:GetUsername()
    pfuserconfig.private_config.etterna.token = DLMAN:GetToken()
    pfuserconfig.save_to_file()

    login_ask_method_vbox.visible = true
    login_username_password_vbox.visible = false

    login_vbox.visible = false
    profile_vbox.visible = true
    if profile_panel_visible then
      profile_vbox:focus()
    end
  end
)

-- This seems broken in Etterna... (In that a bad login will broadcast LoginMessageCommand)
canvas:add_hook(
  "LoginFailedMessageCommand",
  function(self)
    login_waiting_text.visible = false
    login_vbox.visible = true
    SCREENMAN:SystemMessage(pflang.get_entry("login_failed"))
  end
)

-- canvas:add_hook(
--   "LogOutMessageCommand",
--   function(self)
--   end
-- )

return canvas:make_actor()
