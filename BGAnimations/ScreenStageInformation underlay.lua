-- Y'know I didn't think I'd have to make this screen a thing, but something with Etterna's restart keybind screws with
-- my theme - stuff doesn't get unregistered from tickers in EndCommand and that leaves tickers calling into code that
-- references stale actors.
--
-- Seriously, Etterna keeps finding ways to fuck with me, from this issue, to the bad naming of things like `doot` and
-- `loot` and `notShit` when I try browsing through the source code, and more.
--
-- Somehow the very prescence of this mostly-empty screen solves that... well as long as you're not a fucking lunatic
-- who likes to spam the restart button... seriously what's up with this game.
--
-- Now, the way Etterna handles the restart keybind is that it switches to this screen, `ScreenStageInformation`, before
-- it switches back to `ScreenGameplay`.

local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")

local PfCanvas = pfwidget.PfCanvas

local canvas = PfCanvas()
canvas:add_hook(
  "OnCommand",
  function(actor)
    pfutil.screen_switch_immediate("ScreenGameplay")
  end
  )

canvas:add_background()

return canvas:make_actor()
