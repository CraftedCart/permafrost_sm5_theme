local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pfsong = require("permafrost.pfsong")
local pflang = require("permafrost.pflang")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfText = pfwidget.PfText
local PfProgressBar = pfwidget.PfProgressBar

local canvas = PfCanvas()
canvas:add_background()

local indexing_text = PfText{}
indexing_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
    actor.align = Vec2(0.5, 0.5)
    actor.text = pflang.get_entry("indexing_songs")
  end
)
canvas:add_child(indexing_text)

local indexing_progress_text = PfText{}
indexing_progress_text:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y + 48)
    actor.align = Vec2(0.5, 1)
    actor.scale = 0.5
  end
)
canvas:add_child(indexing_progress_text)

local indexing_progress_bar = PfProgressBar()
indexing_progress_bar:add_hook(
  "InitCommand",
  function(actor)
    actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y + 64)
    actor.align = Vec2(1, 1)
    actor.size = Vec2(512, 4)
  end
)
canvas:add_child(indexing_progress_bar)

canvas:add_hook(
  "InitCommand",
  function(actor)
    local indexing_coro = coroutine.create(function()
      -- pfsong.conditional_index_songs(function(done, total)
      pfsong.index_songs(function(done, total)
        indexing_progress_text.text = done .. " / " .. total
        indexing_progress_bar.progress = done / total
      end)
    end)

    actor.actor:SetUpdateFunction(function()
      if not coroutine.resume(indexing_coro) then
        -- We're done!
        pfutil.screen_switch("ScreenSelectMusic") -- TODO: Should this maybe be ScreenSelectPlayMode?
        actor.actor:SetUpdateFunction(nil)
      end
    end)
  end
)

return canvas:make_actor()
