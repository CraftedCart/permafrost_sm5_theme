local pfwidget = require("permafrost.pfwidget")

local PfCanvas = pfwidget.PfCanvas

local canvas = PfCanvas()
return canvas:make_actor()
