local pfinput = require("permafrost.pfinput")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")

local PfCanvas = pfwidget.PfCanvas

local canvas = PfCanvas()
canvas:add_background()

-- Stepmania/Etterna title
canvas:add_child(
  pfconfig.fonts.thin_90 .. {
    InitCommand = function(self)
      self:xy(SCREEN_CENTER_X, 64 + 24)
        :zoom(1)
        :valign(0)
        :halign(0.5)
        :diffuse(pfconfig.colors.title_text)
        :diffusealpha(0)

        :decelerate(0.5)
        :addy(-24)
        :diffusealpha(1)

      self:settext(pfconfig.game_name)
    end,
  }
  )

-- Permafrost title
canvas:add_child(
  pfconfig.fonts.light_40 .. {
    InitCommand = function(self)
      self:xy(SCREEN_CENTER_X, 64 + 90 + 24)
        :zoom(0.7)
        :valign(0)
        :halign(0.5)
        :diffuse(pfconfig.colors.title_text)
        :diffusealpha(0)

        :sleep(0.1)
        :decelerate(0.5)
        :addy(-24)
        :diffusealpha(1)

      self:settext(pfconfig.theme_name)
    end,
  }
  )

-- Game version (in the bottom right)
canvas:add_child(
  pfconfig.fonts.regular_40 .. {
    InitCommand = function(self)
      self:xy(SCREEN_WIDTH - 24, SCREEN_HEIGHT - 24)
        :zoom(0.5)
        :valign(1)
        :halign(1)
        :diffuse(pfconfig.colors.title_text)
        :diffusealpha(0)

        :decelerate(0.5)
        :diffusealpha(0.2)

      self:settext(pfconfig.game_name .. " " .. pfconfig.game_version)
    end,
  }
  )

-- Menu options
local scroller_choices = pfconfig.title_menu_options
local scroller_option_offset = 60
local scroller_x = SCREEN_CENTER_X - 512
local scroller_y = SCREEN_HEIGHT - (#scroller_choices * scroller_option_offset) - 48

local selected_index = 1
local scroller_stripes = {}

local run_selected_action

for k, v in ipairs(scroller_choices) do
  -- Stripe
  local stripe =
    Def.Quad {
      InitCommand = function(self)
        table.insert(scroller_stripes, self)
        self:xy(0, scroller_y + ((k - 1) * scroller_option_offset) - 10)
          :halign(0)
          :valign(0)
          :zoomto(SCREEN_WIDTH, scroller_option_offset - 10)
          :diffuse(selected_index == k and pfconfig.colors.accent or pfconfig.colors.accent_dark)
          :diffusealpha(0)

          :sleep((k + 0.2) * 0.05)
          :decelerate(0.5)
          :diffusealpha(1)
      end,
      PF_MouseLeftClickMessageCommand = function(self)
        if pfutil.is_mouse_over(self) then
          local prev_index = selected_index
          selected_index = k

          scroller_stripes[prev_index]:diffuse(pfconfig.colors.accent_dark)
          scroller_stripes[selected_index]:diffuse(pfconfig.colors.accent)

          run_selected_action()
        end
      end,
    }

  canvas:add_child(stripe)

  -- Top shadow
  canvas:add_child(
    Def.Quad {
      InitCommand = function(self)
        self:xy(0, scroller_y + ((k - 1) * scroller_option_offset) - 10 - 8)
          :halign(0)
          :valign(0)
          :zoomto(SCREEN_WIDTH, 8)
          :diffuse(pfconfig.colors.shadow)
          :diffusealpha(0)
          :fadetop(1)

          :sleep((k + 0.2) * 0.05)
          :decelerate(0.5)
          :diffuse(pfconfig.colors.shadow)
      end,
    }
    )

  -- Bottom shadow
  canvas:add_child(
    Def.Quad {
      InitCommand = function(self)
        self:xy(0, scroller_y + ((k - 1) * scroller_option_offset) - 10 + scroller_option_offset - 10)
          :halign(0)
          :valign(0)
          :zoomto(SCREEN_WIDTH, 8)
          :diffuse(pfconfig.colors.shadow)
          :diffusealpha(0)
          :fadebottom(1)

          :sleep((k + 0.2) * 0.05)
          :decelerate(0.5)
          :diffuse(pfconfig.colors.shadow)
      end,
    }
    )

  -- Option text
  canvas:add_child(
    pfconfig.fonts.light_40 .. {
      InitCommand = function(self)
        self:xy(scroller_x, scroller_y + ((k - 1) * scroller_option_offset))
          :zoom(1)
          :valign(0)
          :halign(0)
          :diffuse(pfconfig.colors.title_text)
          :diffusealpha(0)

          :sleep((k + 0.2) * 0.05)
          :decelerate(0.5)
          :addx(-24)
          :diffusealpha(1)

        local text = pflang.get_entry(v.id)
        self:settext(text)
      end,
    }
    )
end

-- Left accent 2nd stripe
local accent_stripe_2
canvas:add_child(
  Def.Quad {
    InitCommand = function(self)
      accent_stripe_2 = self

      self:xy(-24, 0)
        :halign(0)
        :valign(0)
        :zoomto(80 + 24, SCREEN_HEIGHT)
        :diffuse(pfconfig.colors.accent_dark)
        :diffusealpha(0)

        :decelerate(0.5)
        :addx(24, 0)
        :diffusealpha(1)
    end,
  }
  )

-- Left accent 2nd shadow
local accent_shadow_2
canvas:add_child(
  Def.Quad {
    InitCommand = function(self)
      accent_shadow_2 = self

      self:xy(104 - 24, 0)
        :halign(0)
        :valign(0)
        :zoomto(8, SCREEN_HEIGHT)
        :diffuse(pfconfig.colors.shadow)
        :diffusealpha(0)
        :faderight(1)

        :decelerate(0.5)
        :addx(24)
        :diffuse(pfconfig.colors.shadow)
    end,
  }
  )

-- Left accent 1st stripe
local accent_stripe_1
canvas:add_child(
  Def.Quad {
    InitCommand = function(self)
      accent_stripe_1 = self

      self:xy(-24, 0)
        :halign(0)
        :valign(0)
        :zoomto(80, SCREEN_HEIGHT)
        :diffuse(pfconfig.colors.accent)
        :diffusealpha(0)

        :sleep(0.1)
        :decelerate(0.5)
        :addx(24)
        :diffusealpha(1)
    end,
  }
  )

-- Left accent 1st shadow
local accent_shadow_1
canvas:add_child(
  Def.Quad {
    InitCommand = function(self)
      accent_shadow_1 = self

      self:xy(80 - 24, 0)
        :halign(0)
        :valign(0)
        :zoomto(8, SCREEN_HEIGHT)
        :diffuse(pfconfig.colors.shadow)
        :diffusealpha(0)
        :faderight(1)

        :sleep(0.1)
        :decelerate(0.5)
        :addx(24)
        :diffuse(pfconfig.colors.shadow)
    end,
  }
  )

run_selected_action = function()
  pfutil.play_click_sound()

  accent_stripe_1:finishtweening()
  accent_shadow_1:finishtweening()
  accent_stripe_2:finishtweening()
  accent_shadow_2:finishtweening()

  accent_stripe_1:accelerate(pfconfig.screen_transition_seconds)
    :zoomto(SCREEN_WIDTH / 4, SCREEN_HEIGHT)
  accent_shadow_1:accelerate(pfconfig.screen_transition_seconds)
    :x(SCREEN_WIDTH / 4)
  accent_stripe_2:accelerate(pfconfig.screen_transition_seconds)
    :zoomto(SCREEN_WIDTH / 4 + 22, SCREEN_HEIGHT)
  accent_shadow_2:accelerate(pfconfig.screen_transition_seconds)
    :x(SCREEN_WIDTH / 4 + 22)

  scroller_choices[selected_index].action()
end

-- Input handling
local function input_handler(event)
  if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
    if event.GameButton == "MenuDown" or event.GameButton == "MenuRight" then
      pfutil.play_click_sound()

      local prev_index = selected_index
      selected_index = selected_index + 1
      if selected_index > #scroller_choices then
        selected_index = 1
      end

      scroller_stripes[prev_index]:diffuse(pfconfig.colors.accent_dark)
      scroller_stripes[selected_index]:diffuse(pfconfig.colors.accent)

    elseif event.GameButton == "MenuUp" or event.GameButton == "MenuLeft" then
      pfutil.play_click_sound()

      local prev_index = selected_index
      selected_index = selected_index - 1
      if selected_index < 1 then
        selected_index = #scroller_choices
      end

      scroller_stripes[prev_index]:diffuse(pfconfig.colors.accent_dark)
      scroller_stripes[selected_index]:diffuse(pfconfig.colors.accent)

    elseif event.GameButton == "Start" then
      run_selected_action()
    end
  end


  if event.type ~= "InputEventType_Release" then
    if event.DeviceInput.button == "DeviceButton_left mouse button" then
      MESSAGEMAN:Broadcast("PF_MouseLeftClick")
    end
  end
end

canvas:add_hook(
  "OnCommand",
  function(actor)
    pfinput.push_input_layer()
    pfinput.add_input_handler(input_handler)
  end
  )

canvas:add_hook(
  "OffCommand",
  function(actor)
    pfinput.pop_input_layer()
  end
  )

return canvas:make_actor()
