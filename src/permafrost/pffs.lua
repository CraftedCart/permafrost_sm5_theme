--- Filesystem stuff
-- @module permafrost.pffs

local pfiter = require("permafrost.pfiter")
local class = require("permafrost.class")

local Iterator = pfiter.Iterator

local pffs = {}

pffs.FileEntry = class.strict {
  path = class.NULL,
  basename = class.property{
    get = function(self) return Basename(self.path) end,
    set = function(self, new) error("Cannot set FileEntry basename") end,
  },

  is_dir = class.property {
    get = function(self) return ActorUtil.GetFileType(self.path) == "FileType_Directory" end,
    set = function(self, new) error("Cannot set FileEntry is_dir") end,
  },

  is_sound = class.property {
    get = function(self) return ActorUtil.GetFileType(self.path) == "FileType_Sound" end,
    set = function(self, new) error("Cannot set FileEntry is_sound") end,
  },

  __init = function(self, path)
    self.path = path
  end,

  list_dir = function(self)
    local dir_listing = FILEMAN:GetDirListing(self.path, false, true)
    local out = pffs.DirListing()

    for k, v in ipairs(dir_listing) do
      out.list[k] = pffs.FileEntry(v)
    end

    return out
  end,

  __mt = {
    __tostring = function(self)
      return self.path
    end,

    __eq = function(self, other)
      return self.path == other.path
    end,
  },
}

pffs.DirListing = class.strict {
  list = class.NULL,

  __init = function(self)
    self.list = {}
  end,

  iter = function(self)
    return pffs.DirListingIter(self)
  end,

  -- __mt = {
    -- __index = function(self, k)
      -- return self.list[k]
    -- end,
  -- },
}

pffs.DirListingIter = class.strict(Iterator) {
  dir_list = class.NULL,
  idx = 1,

  __init = function(self, dir_list)
    Iterator.__init(self)

    self.dir_list = dir_list
  end,

  internal_next = function(self)
    local val = self.dir_list.list[self.idx]
    self.idx = self.idx + 1

    return val
  end,
}

return pffs
