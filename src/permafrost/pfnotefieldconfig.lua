local class = require("permafrost.class")
local pfutil = require("permafrost.pfutil")
local pfuserconfig = require("permafrost.pfuserconfig")
local pftimer = require("permafrost.pftimer")

local pfnotefieldconfig = {}

pfnotefieldconfig.EScrollMode = {
  CMod = {},
  MMod = {},
  XMod = {},
  AMod = {}, -- StepMania 5.3+ only
}

local EScrollMode = pfnotefieldconfig.EScrollMode

pfnotefieldconfig.PfNoteFieldConfig = class.strict {
  scroll_mode = class.NULL,
  scroll_speed = class.NULL,

  scale = class.NULL, -- The mini mod

  note_skin = class.NULL,
  hit_sound_file = class.NULL, -- Nullable
  hit_sound_volume = class.NULL,
  combo_break_sound_file = class.NULL, -- Nullable
  combo_break_sound_volume = class.NULL,

  _music_rate = class.NULL,
  music_rate = class.property {
    get = function(self) return self._music_rate end,
    set = function(self, new)
      self._music_rate = new
      GAMESTATE:GetSongOptionsObject("ModsLevel_Preferred"):MusicRate(new)
    end,
  },

  -- An Etterna-only thing
  _rate_affects_pitch = class.NULL,
  rate_affects_pitch = class.property {
    get = function(self) return self._rate_affects_pitch end,
    set = function(self, new)
      self._rate_affects_pitch = new
      PREFSMAN:SetPreference("EnablePitchRates", new)

      -- We gotta update the music rate for this to take effect
      -- Eugh this looks like a kludge and it is
      -- Dang it Etterna >.<
      local old_rate = self.music_rate
      GAMESTATE:GetSongOptionsObject("ModsLevel_Preferred"):MusicRate(1)
      pftimer.run_in_seconds(
        0,
        function()
          GAMESTATE:GetSongOptionsObject("ModsLevel_Preferred"):MusicRate(old_rate)
        end
      )
    end,
  },

  background_dim = class.NULL,

  __init = function(self)
    -- Defaults
    self.scroll_mode = EScrollMode.CMod
    self.scroll_speed = 600
    self.scale = 1

    self.note_skin = "default"
    self.hit_sound_volume = 1

    self._music_rate = 1
    self._rate_affects_pitch = true

    self.background_dim = 0.3
  end,

  load_from_player = function(self, player_num)
    local player = player_num == 2 and PLAYER_2 or PLAYER_1
    local sm_player_options = GAMESTATE:GetPlayerState(player):GetPlayerOptions("ModsLevel_Preferred")
    local sm_song_options = GAMESTATE:GetSongOptionsObject("ModsLevel_Preferred")

    local speed = sm_player_options:CMod()
    if speed then
      self.scroll_mode = EScrollMode.CMod
      self.scroll_speed = speed
    else
      speed = sm_player_options:MMod()
      if speed then
        self.scroll_mode = EScrollMode.MMod
        self.scroll_speed = speed
      else
        speed = sm_player_options:XMod()
        if speed then
          self.scroll_mode = EScrollMode.XMod
          self.scroll_speed = speed
        else
          error("Couldn't find the scroll mode")
        end
      end
    end

    -- Mini = 0 is 100% scale
    -- Mini = 1 is 50% scale
    -- Mini = 2 is 0% scale
    -- self.scale = 1 - (sm_player_options:Mini() / 2)
    self.scale = pfuserconfig.public_config.notefield.scale

    self.note_skin = sm_player_options:NoteSkin()
    self.hit_sound_file = pfuserconfig.public_config.notefield.hit_sound.file or class.NULL
    self.hit_sound_volume = pfuserconfig.public_config.notefield.hit_sound.volume

    self.combo_break_sound_file = pfuserconfig.public_config.notefield.combo_break_sound.file or class.NULL
    self.combo_break_sound_volume = pfuserconfig.public_config.notefield.combo_break_sound.volume

    self.music_rate = sm_song_options:MusicRate()
    if pfutil.is_etterna then
      self.rate_affects_pitch = PREFSMAN:GetPreference("EnablePitchRates")
    end

    -- ETTERNA WHYYYYYY
    if pfutil.is_etterna then
      self.background_dim = (1 - PREFSMAN:GetPreference("BGBrightness")) ^ 0.5
    else
      self.background_dim = 1 - PREFSMAN:GetPreference("BGBrightness")
    end
  end,

  apply_to_player = function(self, player_num)
    local player = player_num == 2 and PLAYER_2 or PLAYER_1
    local sm_player_options = GAMESTATE:GetPlayerState(player):GetPlayerOptions("ModsLevel_Preferred")
    local sm_song_options = GAMESTATE:GetSongOptionsObject("ModsLevel_Preferred")

    if self.scroll_mode == EScrollMode.CMod then
      sm_player_options:CMod(self.scroll_speed)
    elseif self.scroll_mode == EScrollMode.MMod then
      sm_player_options:MMod(self.scroll_speed)
    elseif self.scroll_mode == EScrollMode.XMod then
      sm_player_options:XMod(self.scroll_speed)
    else
      error("Couldn't apply the scroll mode")
    end

    -- Mini = 0 is 100% scale
    -- Mini = 1 is 50% scale
    -- Mini = 2 is 0% scale
    sm_player_options:Mini(2 - (self.scale * 2))
    pfuserconfig.public_config.notefield.scale = self.scale

    sm_player_options:NoteSkin(self.note_skin)

    if self.hit_sound_file == class.NULL then
      pfuserconfig.public_config.notefield.hit_sound.file = nil
    else
      pfuserconfig.public_config.notefield.hit_sound.file = self.hit_sound_file
    end
    pfuserconfig.public_config.notefield.hit_sound.volume = self.hit_sound_volume

    if self.combo_break_sound_file == class.NULL then
      pfuserconfig.public_config.notefield.combo_break_sound.file = nil
    else
      pfuserconfig.public_config.notefield.combo_break_sound.file = self.combo_break_sound_file
    end
    pfuserconfig.public_config.notefield.combo_break_sound.volume = self.combo_break_sound_volume

    sm_song_options:MusicRate(self.music_rate)
    if pfutil.is_etterna then
      PREFSMAN:SetPreference("EnablePitchRates", self.rate_affects_pitch)
    end

    -- ETTERNA WHYYYYYY
    -- In fact, as far as I can tel, Etterna does more than just squaring
    -- And I can't even tell where or whyy
    if pfutil.is_etterna then
      PREFSMAN:SetPreference("BGBrightness", 1 - (self.background_dim ^ 2))
    else
      PREFSMAN:SetPreference("BGBrightness", 1 - self.background_dim)
    end

    PREFSMAN:SavePreferences()
    pfuserconfig.save_to_file()
  end,
}

return pfnotefieldconfig
