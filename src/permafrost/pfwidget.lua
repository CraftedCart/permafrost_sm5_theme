local class = require("permafrost.class")
local pfmath = require("permafrost.pfmath")
local pfsignal = require("permafrost.pfsignal")
local pftimer = require("permafrost.pftimer")
local pfscreenctx = require("permafrost.pfscreenctx")
local pfeasing = require("permafrost.pfeasing")
local pfinput = require("permafrost.pfinput")
local pflang = require("permafrost.pflang")
local pfconfig = require("permafrost.pfconfig")
local pfplatform = require("permafrost.pfplatform")
local pfutil = require("permafrost.pfutil")

local PfSignal = pfsignal.PfSignal
local Vec2 = pfmath.Vec2
local Vec3 = pfmath.Vec3

local pfwidget = {}

local TRIANGLE_PATH = THEME:GetPathG("", "triangle.png")
local CHECK_PATH = THEME:GetPathG("", "check.png")

pfwidget.weak = {
  focused = nil
}
setmetatable(
  pfwidget.weak,
  {
    __mode = "v"
  }
)

pfwidget.focus_changed_signal = PfSignal()

function pfwidget.get_focus()
  return pfwidget.weak.focused
end

function pfwidget.set_focus(widget)
  local prev_focus = pfwidget.get_focus()

  if prev_focus ~= widget then
    pfwidget.weak.focused = widget
    pfwidget.focus_changed_signal:emit(prev_focus, widget)
  end
end

function pfwidget.focus_next(dir)
  local focused = pfwidget.get_focus()
  if focused then
    return focused.parent:focus_next(focused, dir)
  end

  return false
end

pfwidget.Margins = class {
  horiz = class.property {
    get = function(self)
      assert(self.left == self.right)
      return self.left
    end,
    set = function(self, new)
      self.left = new
      self.right = new
    end,
  },

  vert = class.property {
    get = function(self)
      assert(self.top == self.bottom)
      return self.top
    end,
    set = function(self, new)
      self.top = new
      self.bottom = new
    end,
  },

  __init = function(self, args)
    self.left = args.left or args.horiz or 0
    self.top = args.top or args.vert or 0
    self.right = args.right or args.horiz or 0
    self.bottom = args.bottom or args.vert or 0
  end,
}

pfwidget.Anchors = class {
  __init = function(self, args)
    -- The top-left anchor point
    self.min = args.min or Vec2(0, 0)

    -- THe bottom-right anchor point
    self.max = args.max or Vec2(0, 0)
  end,
}

pfwidget.EFocusDirection = {
  NEXT = {},
  PREV = {},

  UP = {},
  DOWN = {},
  LEFT = {},
  RIGHT = {},
}

pfwidget.ESizeMode = {
  -- Containers should not try to resize widgets
  KEEP = {},

  -- Containers will resize widgets to fit a space
  FILL = {},
}

pfwidget.EDistributeMode = {
  FIT = {},
  EVEN = {},
}

pfwidget.PfTexture = class.strict {
  -- The underlying StepMania RageTexture
  sm_texture = class.NULL,

  __init = function(self, sm_texture)
    self.sm_texture = sm_texture
  end,
}

pfwidget.PfActor = class.strict {
  -- The underlying StepMania actor
  actor = class.NULL,

  parent = class.NULL,
  hooks = class.NULL,

  -- For stuff that should have an associated lifetime
  _ctx = class.NULL,
  ctx = class.property {
    get = function(self)
      if self._ctx == class.NULL then
        -- Try to get the context from the parent
        if self.parent == class.NULL then
          -- No parent, make a context here
          self._ctx = pfscreenctx.new(self)
        else
          self._ctx = self.parent.ctx -- Cache the parent ctx for quicker lookup
        end
      end

      return self._ctx
    end,
    set = function(self, new) self._ctx = new end,
  },

  -- Emitted on EndCommand
  end_signal = class.NULL,

  -- Layout stuff
  slot = class.NULL,
  margins = class.NULL,

  -- Focus stuff
  is_focusable = class.NULL,
  focused = class.property {
    get = function(self) return pfwidget.get_focus() == self end,
    set = function(self, new)
      if new then
        pfwidget.set_focus(self)
      else
        pfwidget.set_focus(nil)
      end
    end,
  },

  focus = function(self)
    if self.is_focusable then
      pfwidget.set_focus(self)
      return true
    else
      return false
    end
  end,

  focus_next = function(self, from, direction)
    self.focused = false

    if self.parent ~= class.NULL then
      return self.parent:focus_next(from, direction)
    end
  end,

  focus_in_signal = class.NULL,
  focus_out_signal = class.NULL,

  -- Overridable
  handle_focus_in = function(self) end,
  handle_focus_out = function(self) end,

  -- Input handling (when focused)
  handle_input_event = function(self, event)
    return false
  end,

  _handle_focus_changed = class.NULL,

  -- Init
  __init = function(self)
    self.hooks = {}
    self.margins = pfwidget.Margins{}
    self.is_focusable = false

    self.focus_in_signal = PfSignal()
    self.focus_out_signal = PfSignal()

    self.end_signal = PfSignal()

    -- Gotta store it in the object as we're connecting the signal weakly
    self._handle_focus_changed = function(prev_focus, new_focus)
      if new_focus == self then
        self:handle_focus_in()
        self.focus_in_signal:emit()
      elseif prev_focus == self then
        self:handle_focus_out()
        self.focus_out_signal:emit()
      end
    end

    self:add_hook(
      "EndCommand",
      function(actor)
        actor.end_signal:emit()
      end
      )

    pfwidget.focus_changed_signal:connect_weak(self._handle_focus_changed)
  end,

  position = class.property {
    get = function(self)
      return Vec3(
        self.actor:GetX(),
        self.actor:GetY(),
        self.actor:GetZ()
      )
    end,

    set = function(self, new)
      self.actor:x(new.x or 0)
      self.actor:y(new.y or 0)
      self.actor:z(new.z or 0)
    end,
  },

  x = class.property {
    get = function(self) return self.actor:GetX() end,
    set = function(self, new) self.actor:x(new) end,
  },

  y = class.property {
    get = function(self) return self.actor:GetY() end,
    set = function(self, new) self.actor:y(new) end,
  },

  z = class.property {
    get = function(self) return self.actor:GetZ() end,
    set = function(self, new) self.actor:z(new) end,
  },

  rotation = class.property {
    get = function(self)
      return Vec3(
        self.actor:GetRotationX(),
        self.actor:GetRotationY(),
        self.actor:GetRotationZ()
      )
    end,

    set = function(self, new)
      self.actor:rotationx(new.x or 0)
      self.actor:rotationy(new.y or 0)
      self.actor:rotationz(new.z or 0)
    end,
  },

  rotation_x = class.property {
    get = function(self) return self.actor:GetRotationX() end,
    set = function(self, new) self.actor:rotationx(new) end,
  },

  rotation_y = class.property {
    get = function(self) return self.actor:GetRotationY() end,
    set = function(self, new) self.actor:rotationy(new) end,
  },

  rotation_z = class.property {
    get = function(self) return self.actor:GetRotationZ() end,
    set = function(self, new) self.actor:rotationz(new) end,
  },

  scale = class.property {
    get = function(self)
      return Vec3(
        self.actor:GetZoomX(),
        self.actor:GetZoomY(),
        self.actor:GetZoomZ()
      )
    end,

    set = function(self, new)
      if type(new) == "table" then
        self.scale_x = new.x or 1
        self.scale_y = new.y or 1
        self.scale_z = new.z or 1
      elseif type(new) == "number" then
        self.scale_x = new
        self.scale_y = new
        self.scale_z = new
      else
        error("Invalid type passed to PfActor.scale.set")
      end
    end,
  },

  scale_x = class.property {
    get = function(self) return self.actor:GetZoomX() end,
    set = function(self, new) self.actor:zoomx(new) end,
  },

  scale_y = class.property {
    get = function(self) return self.actor:GetZoomY() end,
    set = function(self, new) self.actor:zoomy(new) end,
  },

  scale_z = class.property {
    get = function(self) return self.actor:GetZoomZ() end,
    set = function(self, new) self.actor:zoomz(new) end,
  },

  size = class.property {
    get = function(self)
      return Vec2(
        self.width,
        self.height
      )
    end,

    set = function(self, new)
      if type(new) == "table" then
        self.width = new.x or 1
        self.height = new.y or 1
      elseif type(new) == "number" then
        self.width = new
        self.height = new
      else
        error("Invalid type passed to PfActor.size.set")
      end
    end,
  },

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new) self.actor:SetWidth(new) end,
  },

  width_scaled = class.property {
    get = function(self) return self.actor:GetWidth() * self.scale_x end,
    set = function(self, new)
      self.width = new / self.scale_x
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new) self.actor:SetHeight(new) end,
  },

  height_scaled = class.property {
    get = function(self) return self.actor:GetHeight() * self.scale_x end,
    set = function(self, new)
      self.height = new / self.scale_x
    end,
  },

  color = class.property {
    get = function(self) return self.actor:GetDiffuse() end,
    set = function(self, new) self.actor:diffuse(new) end,
  },

  color_bottom = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge/corner colors") end,
    set = function(self, new) self.actor:diffusebottomedge(new) end,
  },

  color_left = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge/corner colors") end,
    set = function(self, new) self.actor:diffuseleftedge(new) end,
  },

  color_right = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge/corner colors") end,
    set = function(self, new) self.actor:diffuserightedge(new) end,
  },

  color_top = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge/corner colors") end,
    set = function(self, new) self.actor:diffusetopedge(new) end,
  },

  alpha = class.property {
    get = function(self) return self.actor:GetDiffuseAlpha() end,
    set = function(self, new) self.actor:diffusealpha(new) end,
  },

  fade_bottom = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge fade percents") end,
    set = function(self, new) self.actor:fadebottom(new) end,
  },

  fade_left = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge fade percents") end,
    set = function(self, new) self.actor:fadeleft(new) end,
  },

  fade_right = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge fade percents") end,
    set = function(self, new) self.actor:faderight(new) end,
  },

  fade_top = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge fade percents") end,
    set = function(self, new) self.actor:fadetop(new) end,
  },

  crop_bottom = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge crop percents") end,
    set = function(self, new) self.actor:cropbottom(new) end,
  },

  crop_left = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge crop percents") end,
    set = function(self, new) self.actor:cropleft(new) end,
  },

  crop_right = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge crop percents") end,
    set = function(self, new) self.actor:cropright(new) end,
  },

  crop_top = class.property {
    get = function(self) error("StepMania does not seem to have a function to get actor edge crop percents") end,
    set = function(self, new) self.actor:croptop(new) end,
  },

  align = class.property {
    get = function(self)
      return Vec2(
        self.actor:GetHAlign(),
        self.actor:GetVAlign()
      )
    end,

    set = function(self, new)
      self.actor:halign(new.x or 0)
      self.actor:valign(new.y or 0)
    end,
  },

  h_align = class.property {
    get = function(self) return self.actor:GetHAlign() end,
    set = function(self, new) self.actor:halign(new) end,
  },

  v_align = class.property {
    get = function(self) return self.actor:GetVAlign() end,
    set = function(self, new) self.actor:valign(new) end,
  },

  draw_order = class.property {
    get = function(self) error("StepMania does not seem to have a function to get an actor's draw order") end,
    set = function(self, new)
      self.actor:draworder(new)

      if self.parent ~= class.NULL and self.parent.actor ~= class.NULL then
        self.parent.actor:SortByDrawOrder()
      end
    end,
  },

  -- This widget's visibility
  visible = class.property {
    get = function(self) return self.actor:GetVisible() end,
    set = function(self, new) self.actor:visible(new) end,
  },

  texture = class.property {
    get = function(self) return pfwidget.PfTexture(self.actor:GetTexture()) end,
    set = function(self, new)
      if new.__instance_of ~= nil and new:__instance_of(pfwidget.PfTexture) then
        self.actor:SetTexture(new.sm_texture)
      else
        self.actor:SetTexture(new)
      end
    end,
  },

  texture_state = class.property {
    get = function(self) error("StepMania does not seem to have a function to get an actor's state, unless it is a Sprite") end,
    set = function(self, new) self.actor:setstate(new) end,
  },

  texture_state_num = class.property {
    get = function(self) return self.actor:GetNumStates() end,
    set = function(self, new) error("texture_state_num is read-only") end,
  },

  -- Check if this and all parent widgets are visible
  is_visible = function(self)
    local test = self

    while test ~= class.NULL do
      if not test.visible then return false end
      test = test.parent
    end

    return true
  end,

  add_x = function(self, val)
    self.actor:addx(val)
  end,

  add_y = function(self, val)
    self.actor:addy(val)
  end,

  add_z = function(self, val)
    self.actor:addz(val)
  end,

  queue_command = function(self, command_name)
    self.actor:queuecommand(command_name)
  end,

  tween_sleep = function(self, seconds)
    self.actor:sleep(seconds)
  end,

  tween_linear = function(self, seconds)
    self.actor:linear(seconds)
  end,

  tween_accelerate = function(self, seconds)
    self.actor:accelerate(seconds)
  end,

  tween_decelerate = function(self, seconds)
    self.actor:decelerate(seconds)
  end,

  tween_spring = function(self, seconds)
    self.actor:spring(seconds)
  end,

  tween_pause = function(self)
    self.actor:pause()
  end,

  -- Tween defined in _fallback theme
  tween_smooth = function(self, seconds)
    self.actor:smooth(seconds)
  end,

  -- Tween defined in _fallback theme
  tween_bounce_begin = function(self, seconds)
    self.actor:bouncebegin(seconds)
  end,

  -- Tween defined in _fallback theme
  tween_bounce_end = function(self, seconds)
    self.actor:bounceend(seconds)
  end,

  -- Tween defined in _fallback theme
  tween_drop = function(self, seconds)
    self.actor:drop(seconds)
  end,

  hurry_tweening = function(self, factor)
    self.actor:hurrytweening(factor)
  end,

  stop_tweening = function(self)
    self.actor:stoptweening()
  end,

  finish_tweening = function(self)
    self.actor:finishtweening()
  end,

  load_texture = function(self, path)
    self.actor:Load(path)
  end,

  load_texture_background = function(self, path)
    self.actor:LoadBackground(path)
  end,

  load_texture_banner = function(self, path)
    self.actor:LoadBanner(path)
  end,

  scale_to_cover = function(self, top_left, bottom_right)
    self.actor:scaletocover(
      top_left.x,
      top_left.y,
      bottom_right.x,
      bottom_right.y
    )
  end,

  scale_to_fit = function(self, top_left, bottom_right)
    self.actor:scaletofit(
      top_left.x,
      top_left.y,
      bottom_right.x,
      bottom_right.y
    )
  end,

  crop_to = function(self, size)
    self.actor:CropTo(size.x, size.y)
  end,

  add_hook = function(self, hook_name, hook_func)
    local hook = self.hooks[hook_name]
    if not hook then
      hook = {}
      self.hooks[hook_name] = hook
    end

    table.insert(hook, hook_func)
  end,

  make_actor = function(self)
    return self:_get_actor_type()(self:_get_actor_init_table())
  end,

  _get_actor_type = function(self)
    return Def.Actor
  end,

  _get_actor_init_table = function(self)
    local t = {}
    if not self.hooks.InitCommand then
      self.hooks.InitCommand = {}
    end

    for k, v in pairs(self.hooks) do
      if k == "InitCommand" then
        t[k] = function(actor_self, ...)
          self.actor = actor_self
          actor_self.pf_widget = self

          for _, hook_v in ipairs(v) do
            hook_v(self, ...)
          end
        end
      else
        t[k] = function(actor_self, ...)
          for _, hook_v in ipairs(v) do
            hook_v(self, ...)
          end
        end
      end
    end

    return t
  end,
}

pfwidget.PfCanvas = class.strict(pfwidget.PfActor) {
  _children_to_add = class.NULL,
  pf_children = class.NULL,

  __init = function(self)
    pfwidget.PfActor.__init(self)

    self.is_focusable = true

    self._children_to_add = {}
    self.pf_children = {}
  end,

  focus = function(self)
    for _, v in pairs(self.pf_children) do
      if v:is_visible() and v:focus() then
        return true
      end
    end

    return false
  end,

  focus_next = function(self, from, direction)
    local from_idx = pfutil.table_index_of(self.pf_children, from)
    assert(from_idx)

    if
      direction == pfwidget.EFocusDirection.NEXT or
      direction == pfwidget.EFocusDirection.DOWN or
      direction == pfwidget.EFocusDirection.RIGHT
    then
      local next_idx = from_idx
      while true do
        next_idx = next_idx + 1
        if next_idx > #self.pf_children then
          next_idx = 1
        end
        if next_idx == from_idx then return true end

        local next_widget = self.pf_children[next_idx]
        if next_widget:is_visible() and next_widget.is_focusable and next_widget:focus() then
          return true
        end
      end

    else
      local next_idx = from_idx
      while true do
        next_idx = next_idx - 1
        if next_idx < 1 then
          next_idx = #self.pf_children
        end
        if next_idx == from_idx then return true end

        local next_widget = self.pf_children[next_idx]
        if next_widget:is_visible() and next_widget.is_focusable and next_widget:focus() then
          return true
        end
      end

    end

    return false
  end,

  add_child = function(self, child)
    table.insert(self._children_to_add, child)

    if child.__instance_of ~= nil and child:__instance_of(pfwidget.PfActor) then
      child.parent = self -- Setting the parent on vanilla StepMania actors murders performance >.<
      table.insert(self.pf_children, child)
    end
  end,

  add_background = function(self)
    local bg = pfwidget.PfRect()
    bg:add_hook(
      "InitCommand",
      function(actor)
        actor.draw_order = -300
        actor.position = Vec2(-1000, -1000)
        actor.scale = Vec2(SCREEN_WIDTH + 2000, SCREEN_HEIGHT + 2000)
        actor.color = pfconfig.colors.screen_background
        actor.align = Vec2(0, 0)
      end
    )
    self:add_child(bg:make_actor())

    self:screen_zoom_in_out()
  end,

  screen_zoom_in_out = function(self)
    local waiter = pfwidget.PfActor()
    self:add_child(waiter)

    local cover = pfwidget.PfRect()
    cover:add_hook(
      "InitCommand",
      function(actor)
        actor.draw_order = 300
        actor.position = Vec2(-1000, -1000)
        actor.scale = Vec2(SCREEN_WIDTH + 2000, SCREEN_HEIGHT + 2000)
        actor.color = pfconfig.colors.screen_background
        actor.align = Vec2(0, 0)
      end
    )
    self:add_child(cover:make_actor())

    self:add_hook(
      "OnCommand",
      function(actor)
        waiter.actor:sleep(pfconfig.screen_transition_seconds)

        actor.position = Vec2(-SCREEN_WIDTH * 0.25 * 0.5, -SCREEN_HEIGHT * 0.25 * 0.5)
        actor.scale = 1.25
        cover.alpha = 1
        actor.ctx.anim{
          start_val = actor.x,
          end_val = 0,
          prop_table = actor,
          prop_name = "x",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.out_expo,
        }
        actor.ctx.anim{
          start_val = actor.y,
          end_val = 0,
          prop_table = actor,
          prop_name = "y",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.out_expo,
        }
        actor.ctx.anim{
          start_val = actor.scale.x,
          end_val = 1,
          prop_table = actor,
          prop_name = "scale",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.out_expo,
        }
        actor.ctx.anim{
          start_val = cover.alpha,
          end_val = 0,
          prop_table = cover,
          prop_name = "alpha",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.linear,
        }
      end
    )

    self:add_hook(
      "OffCommand",
      function(actor)
        waiter.actor:sleep(pfconfig.screen_transition_seconds)

        actor.position = Vec2(0, 0)
        actor.scale = 1
        cover.alpha = 0
        actor.ctx.anim{
          start_val = actor.x,
          end_val = SCREEN_WIDTH * 0.25 * 0.5,
          prop_table = actor,
          prop_name = "x",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.in_expo,
        }
        actor.ctx.anim{
          start_val = actor.y,
          end_val = SCREEN_HEIGHT * 0.25 * 0.5,
          prop_table = actor,
          prop_name = "y",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.in_expo,
        }
        actor.ctx.anim{
          start_val = actor.scale.x,
          end_val = 0.75,
          prop_table = actor,
          prop_name = "scale",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.in_expo,
        }
        actor.ctx.anim{
          start_val = cover.alpha,
          end_val = 1,
          prop_table = cover,
          prop_name = "alpha",
          duration = pfconfig.screen_transition_seconds,
          easing = pfeasing.linear,
        }
      end
    )
  end,

  screen_fade_in_out = function(self)
    local cover = pfwidget.PfRect()
    cover:add_hook(
      "InitCommand",
      function(actor)
        actor.draw_order = 300
        actor.position = Vec2(0, 0)
        actor.scale = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT)
        actor.align = Vec2(0, 0)
      end
    )
    self:add_child(cover:make_actor())

    self:add_hook(
      "OnCommand",
      function(actor)
        cover.actor:diffuse(pfconfig.colors.screen_background)
          :linear(0.1)
          :diffusealpha(0)
      end
    )

    self:add_hook(
      "OffCommand",
      function(actor)
        cover.actor:diffuse(pfconfig.colors.screen_background)
          :linear(0.1)
          :diffusealpha(1)
      end
    )
  end,

  -- whaa how is this a function in StepMania but adding a child from an ActorDef isn't?????
  -- Also, how come LoadActor lets you pass params and returns an actor, but this... nope
  add_child_from_path = function(self, path)
    return self.actor:AddChildFromPath(path)
  end,

  set_update_function = function(self, func)
    self.actor:SetUpdateFunction(func)
  end,

  _get_actor_init_table = function(self)
    local t = pfwidget.PfActor._get_actor_init_table(self)

    for _, v in ipairs(self._children_to_add) do
      if v.__instance_of and v:__instance_of(pfwidget.PfActor) then
        table.insert(t, v:make_actor())
      else
        table.insert(t, v)
      end
    end

    return t
  end,

  _get_actor_type = function(self)
    return Def.ActorFrame
  end,
}

pfwidget.PfAnchoredSlot = class.strict {
  offset = class.NULL,
  size = class.NULL,
  anchors = class.NULL,
  width_mode = class.NULL,
  height_mode = class.NULL,

  __init = function(self)
    self.offset = Vec2()
    self.size = Vec2()
    self.anchors = pfwidget.Anchors{}
    self.width_mode = pfwidget.ESizeMode.FILL
    self.height_mode = pfwidget.ESizeMode.FILL
  end,
}

pfwidget.PfAnchored = class.strict(pfwidget.PfCanvas) {
  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    local update_func = function(delta_seconds) self:_layout_widgets() end
    pftimer.update_signal:connect(update_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
      end
    )
  end,

  _layout_widgets = function(self)
    for _, v in pairs(self.pf_children) do
      v.position = (self.size * v.slot.anchors.min) + v.slot.offset

      local desired_size = (self.size * v.slot.anchors.max) + v.slot.size - v.position

      if v.slot.width_mode == pfwidget.ESizeMode.FILL then
        v.width = desired_size.x
      end

      if v.slot.height_mode == pfwidget.ESizeMode.FILL then
        v.height = desired_size.y
      end
    end
  end,

  add_child = function(self, child)
    pfwidget.PfCanvas.add_child(self, child)

    if child.__instance_of and child:__instance_of(pfwidget.PfActor) then
      child.slot = pfwidget.PfAnchoredSlot()
    end
  end,
}

pfwidget.PfOverlay = class.strict(pfwidget.PfCanvas) {
  min_width = 0,
  min_height = 0,

  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    pftimer.update_signal:connect(function(delta_seconds) self:_layout_widgets() end)
  end,

  _layout_widgets = function(self)
    local desired_size = Vec2(self.min_width, self.min_height)

    for _, v in pairs(self.pf_children) do
      if v.visible then
        if v.size.x > desired_size.x then
          desired_size.x = v.size.x
        end
        if v.size.y > desired_size.y then
          desired_size.y = v.size.y
        end
      end
    end

    for _, v in pairs(self.pf_children) do
      v.position = Vec2(0, 0)
      v.size = desired_size
    end

    self.size = desired_size
  end,
}

pfwidget.PfVBoxSlot = class.strict {
  width_mode = pfwidget.ESizeMode.FILL,
}

pfwidget.PfVBox = class.strict(pfwidget.PfCanvas) {
  spacing = class.NULL,

  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    self.spacing = Vec2(20, 8)

    local update_func = function(delta_seconds) self:_layout_widgets() end
    pftimer.update_signal:connect(update_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
      end
    )
  end,

  _layout_widgets = function(self)
    local next_y = 0
    for _, v in ipairs(self.pf_children) do
      if v.visible then
        next_y = next_y + v.margins.top
        v.y = next_y
        next_y = next_y + v.height_scaled + v.margins.bottom + self.spacing.y

        v.x = self.spacing.x + v.margins.left

        if v.slot.width_mode == pfwidget.ESizeMode.FILL then
          v.width = self.width - (self.spacing.x * 2) - v.margins.right
        end
      end
    end

    self.height = next_y
  end,

  add_child = function(self, child)
    pfwidget.PfCanvas.add_child(self, child)

    if child.__instance_of and child:__instance_of(pfwidget.PfActor) then
      child.slot = pfwidget.PfVBoxSlot()
    end
  end,
}

pfwidget.PfHBoxSlot = class.strict {
  height_mode = pfwidget.ESizeMode.FILL,
  distribute_mode = pfwidget.EDistributeMode.FIT,
}

pfwidget.PfHBox = class.strict(pfwidget.PfCanvas) {
  spacing = class.NULL,

  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    self.spacing = Vec2(20, 8)

    local update_func = function(delta_seconds) self:_layout_widgets() end
    pftimer.update_signal:connect(update_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
      end
    )
  end,

  _layout_widgets = function(self)
    local next_x = 0
    local max_height = 0
    for _, v in ipairs(self.pf_children) do
      if v.visible then
        if v.slot.distribute_mode == pfwidget.EDistributeMode.EVEN then
          v.x = next_x
          next_x = next_x + (self.width / #self.pf_children)
        else
          next_x = next_x + v.margins.left
          v.x = next_x
          next_x = next_x + v.width_scaled + v.margins.right + self.spacing.x
        end

        v.y = self.spacing.y + v.margins.top

        if v.slot.height_mode == pfwidget.ESizeMode.FILL then
          v.height = self.height - (self.spacing.y * 2) - v.margins.bottom
        end

        max_height = math.max(max_height, v.height_scaled + self.spacing.y + v.margins.top)
      end
    end

    self.width = next_x
    self.height = max_height
  end,

  add_child = function(self, child)
    pfwidget.PfCanvas.add_child(self, child)

    if child.__instance_of and child:__instance_of(pfwidget.PfActor) then
      child.slot = pfwidget.PfHBoxSlot()
    end
  end,
}

pfwidget.PfRect = class.strict(pfwidget.PfActor) {
  _get_actor_type = function(self)
    return Def.Quad
  end,
}

pfwidget.PfMesh = class.strict(pfwidget.PfActor) {
  vertices = class.property {
    get = function(self) error("Cannot get PfMesh vertices") end,
    set = function(self, new) self.actor:SetVertices(new) end,
  },

  _get_actor_type = function(self)
    return Def.ActorMultiVertex
  end,
}

pfwidget.PfRenderTarget = class.strict(pfwidget.PfCanvas) {
  texture = class.property {
    get = function(self) return pfwidget.PfTexture(self.actor:GetTexture()) end,
    set = function(self, new) error("Cannot set render target texture") end,
  },

  use_alpha_buffer = class.property {
    get = function(self) error("Cannot check whether render target has alpha buffer enabled") end,
    set = function(self, new) self.actor:EnableAlphaBuffer(new) end,
  },

  use_depth_buffer = class.property {
    get = function(self) error("Cannot check whether render target has depth buffer enabled") end,
    set = function(self, new) self.actor:EnableDepthBuffer(new) end,
  },

  use_float = class.property {
    get = function(self) error("Cannot check whether render target uses floats") end,
    set = function(self, new) self.actor:EnableFloat(new) end,
  },

  __init = function(self)
    pfwidget.PfCanvas.__init(self)
  end,

  -- Really StepMania... you can only create an ActorFrameTexture once >.<
  capture = function(self)
    self.actor:Create()
  end,

  _get_actor_type = function(self)
    return Def.ActorFrameTexture
  end,
}

pfwidget.PfText = class.strict(pfwidget.PfActor) {
  font = class.NULL,

  text = class.property {
    get = function(self) return self.actor:GetText() end,
    set = function(self, new) self.actor:settext(new) end,
  },

  __init = function(self, args)
    pfwidget.PfActor.__init(self)

    self.font = args.font or pfconfig.fonts.regular_40
  end,

  set_formatted_text = function(self, format, ...)
    self.actor:settextf(format, ...)
  end,

  make_actor = function(self)
    return self.font .. self:_get_actor_init_table()
  end,

  _get_actor_init_table = function(self)
    local t = pfwidget.PfActor._get_actor_init_table(self)
    t.Font = self.font

    return t
  end,

  -- Not really used but I'll keep it here for completeness
  _get_actor_type = function(self)
    return Def.BitmapText
  end,
}

pfwidget.PfButton = class.strict(pfwidget.PfCanvas) {
  bg_widget = class.NULL,
  left_shadow_widget = class.NULL,
  right_shadow_widget = class.NULL,
  accent_widget = class.NULL,
  text_widget = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.bg_widget.width = new
      self.right_shadow_widget.x = new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self:_set_height(new)
    end,
  },

  _set_height = function(self, new)
    self.actor:SetHeight(new)
    self.bg_widget.height = new
    self.left_shadow_widget.height = new
    self.right_shadow_widget.height = new
    self.accent_widget.height = new
    self.text_widget.y = new / 2
  end,

  focus = function(self)
    self.focused = true
    return true
  end,

  text = class.property {
    get = function(self) return self.text_widget.text end,
    set = function(self, new) self.text_widget.text = new end,
  },

  text_scale = class.property {
    get = function(self) return self.text_widget.scale end,
    set = function(self, new) self.text_widget.scale = new end,
  },

  clicked_signal = class.NULL,

  __init = function(self, args)
    pfwidget.PfCanvas.__init(self)

    self.is_focusable = true

    self.clicked_signal = PfSignal()

    if not args then args = {} end
    local font = args.font or pfconfig.fonts.light_40

    self.bg_widget = pfwidget.PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1, 1)
        actor.color = pfconfig.colors.button_background
      end
    )
    self:add_child(self.bg_widget)

    self.left_shadow_widget = pfwidget.PfRect()
    self.left_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(1, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_left = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.left_shadow_widget)

    self.right_shadow_widget = pfwidget.PfRect()
    self.right_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_right = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.right_shadow_widget)

    self.accent_widget = pfwidget.PfRect()
    self.accent_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(8, 1)
        actor.color = pfconfig.colors.button_accent
      end
    )
    self:add_child(self.accent_widget)

    self.text_widget = pfwidget.PfText{font = font}
    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.button_text
      end
    )
    self:add_child(self.text_widget)

    local update_func = function(delta_seconds)
      if self.focused --[[ or pfutil.is_mouse_over(self.bg_widget.actor) ]] then
        if
          pfinput.is_key_down("DeviceButton_enter") or
          pfinput.is_key_down("DeviceButton_KP enter") or
          pfinput.is_key_down("DeviceButton_space")
        then
          self.accent_widget.color = pfconfig.colors.button_accent_press
          self.left_shadow_widget.color = pfconfig.colors.button_accent_press
          self.right_shadow_widget.color = pfconfig.colors.button_accent_press
        else
          self.accent_widget.color = pfconfig.colors.button_accent_hover
          self.left_shadow_widget.color = pfconfig.colors.button_accent_hover
          self.right_shadow_widget.color = pfconfig.colors.button_accent_hover
        end
      else
        self.accent_widget.color = pfconfig.colors.button_accent
        self.left_shadow_widget.color = pfconfig.colors.shadow
        self.right_shadow_widget.color = pfconfig.colors.shadow
      end
    end

    pftimer.update_signal:connect(update_func)
    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
      end
    )
  end,

  handle_input_event = function(self, event)
    if event.type == "InputEventType_FirstPress" then
      if
        event.DeviceInput.button == "DeviceButton_enter" or
        event.DeviceInput.button == "DeviceButton_KP enter" or
        event.DeviceInput.button == "DeviceButton_space"
      then
        self.clicked_signal:emit()
        return true
      end
    end

    return false
  end,
}

pfwidget.PfOptionGroup = class.strict {
  buttons = class.NULL,

  selected = class.NULL,
  selection_changed_signal = class.NULL,

  __init = function(self)
    self.buttons = {}
    self.selection_changed_signal = PfSignal()
  end,

  add_button = function(self, button, userdata)
    table.insert(
      self.buttons,
      {
        button = button,
        userdata = userdata,
      }
    )
    button.option_group = self
  end,

  select = function(self, userdata)
    for _, v in pairs(self.buttons) do
      if v.userdata == userdata then
        if v ~= self.selected then
          v.button:select()
          local old = self.selected
          self.selected = v
          self.selection_changed_signal:emit(v.userdata, old.userdata)
        end

        return
      end
    end
  end,

  select_button = function(self, button)
    for _, v in pairs(self.buttons) do
      if v.button == button then
        if v ~= self.selected then
          v.button:select()
          local old = self.selected
          self.selected = v
          self.selection_changed_signal:emit(v.userdata, old.userdata)
        end

        return
      end
    end
  end,
}

pfwidget.PfOptionButton = class.strict(pfwidget.PfButton) {
  active_indicator_widget = class.NULL,

  option_group = class.NULL,

  _set_height = function(self, new)
    pfwidget.PfButton._set_height(self, new)

    self.active_indicator_widget.y = new / 2
  end,

  __init = function(self)
    pfwidget.PfButton.__init(self)

    self.active_indicator_widget = pfwidget.PfRect()
    self.active_indicator_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
        actor.rotation_z = -90
      end
    )
    self:add_child(self.active_indicator_widget)

    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.x = actor.x + 32
      end
    )

    local select_func = function() self:clicked_select() end
    self.clicked_signal:connect(select_func)
  end,

  clicked_select = function(self)
    if self.option_group ~= class.NULL then
      self.option_group:select_button(self)
    end
  end,

  select = function(self)
    self.active_indicator_widget.alpha = 1

    if self.option_group ~= class.NULL then
      for _, v in pairs(self.option_group.buttons) do
        if v.button ~= self then
          v.button:deselect()
        end
      end
    end
  end,

  deselect = function(self)
    self.active_indicator_widget.alpha = 0.1
  end,
}

pfwidget.PfToggleButton = class.strict(pfwidget.PfButton) {
  checked_widget = class.NULL,

  _value = false,
  value = class.property {
    get = function(self) return self._value end,
    set = function(self, new)
      local old = self._value

      if new ~= old then
        self._value = new
        self.value_changed_signal:emit(new)

        self.checked_widget.alpha = new and 1 or 0.1
      end
    end,
  },

  value_changed_signal = class.NULL,

  _set_height = function(self, new)
    pfwidget.PfButton._set_height(self, new)

    self.checked_widget.y = new / 2
  end,

  __init = function(self)
    pfwidget.PfButton.__init(self)

    self.value_changed_signal = PfSignal()

    self.checked_widget = pfwidget.PfRect()
    self.checked_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0, 0.5)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(CHECK_PATH)
        actor.size = Vec2(20, 20)
      end
    )
    self:add_child(self.checked_widget)

    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.x = actor.x + 32
      end
    )

    local select_func = function() self.value = not self.value end
    self.clicked_signal:connect(select_func)
  end,
}

-- {seconds to hold the key down for, increment}
local spinbox_increments = {
  {3, 30},
  {2, 20},
  {1, 10},
  {0.75, 5},
  {0.5, 2},
  {0, 1},
}

pfwidget.PfSpinBox = class.strict(pfwidget.PfCanvas) {
  bg_widget = class.NULL,
  left_shadow_widget = class.NULL,
  right_shadow_widget = class.NULL,
  text_widget = class.NULL,
  left_arrow_widget = class.NULL,
  right_arrow_widget = class.NULL,

  left_pressed_time = class.NULL,
  right_pressed_time = class.NULL,
  last_update_time = 0,

  increment_size = 1,
  _text_format = "%.0f",
  text_format = class.property {
    get = function(self) return self._text_format end,
    set = function(self, new)
      self._text_format = new
      self.text = string.format(new, self.value * self.display_multiplier)
    end,
  },

  _display_multiplier = 1,
  display_multiplier = class.property {
    get = function(self) return self._display_multiplier end,
    set = function(self, new)
      self._display_multiplier = new
      self.text = string.format(new, self.value * new)
    end,
  },

  _min_value = class.NULL,
  min_value = class.property {
    get = function(self) return self._min_value end,
    set = function(self, new)
      self._min_value = new
      if self.value < new then
        self.value = new
      end
    end,
  },

  _max_value = class.NULL,
  max_value = class.property {
    get = function(self) return self._max_value end,
    set = function(self, new)
      self._max_value = new
      if self.value > new then
        self.value = new
      end
    end,
  },

  _value = 0,
  value = class.property {
    get = function(self) return self._value end,
    set = function(self, new)
      local old = self._value

      if self.max_value ~= class.NULL and new > self.max_value then
        new = self.max_value
      elseif self.min_value ~= class.NULL and new < self.min_value then
        new = self.min_value
      end

      self._value = new
      self.text = string.format(self.text_format, new * self.display_multiplier)
      self.text_widget.x = self.width / 2

      if old ~= new then
        self.value_changed_signal:emit(new)
      end
    end,
  },

  value_changed_signal = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.bg_widget.width = new
      self.right_shadow_widget.x = new
      self.right_arrow_widget.x = new - 24
      self.text_widget.x = self.width / 2
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self:_set_height(new)
    end,
  },

  _set_height = function(self, new)
    self.actor:SetHeight(new)
    self.bg_widget.height = new
    self.left_shadow_widget.height = new
    self.right_shadow_widget.height = new
    self.left_arrow_widget.y = new / 2
    self.right_arrow_widget.y = new / 2
    self.text_widget.y = new / 2
  end,

  focus = function(self)
    self.focused = true
    return true
  end,

  text = class.property {
    get = function(self) return self.text_widget.text end,
    set = function(self, new) self.text_widget.text = new end,
  },

  text_scale = class.property {
    get = function(self) return self.text_widget.scale end,
    set = function(self, new) self.text_widget.scale = new end,
  },

  __init = function(self, args)
    pfwidget.PfCanvas.__init(self)

    self.is_focusable = true

    self.value_changed_signal = PfSignal()

    if not args then args = {} end
    local font = args.font or pfconfig.fonts.light_40

    self.bg_widget = pfwidget.PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1, 1)
        actor.color = pfconfig.colors.button_background
      end
    )
    self:add_child(self.bg_widget)

    self.left_shadow_widget = pfwidget.PfRect()
    self.left_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(1, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_left = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.left_shadow_widget)

    self.right_shadow_widget = pfwidget.PfRect()
    self.right_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_right = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.right_shadow_widget)

    self.left_arrow_widget = pfwidget.PfRect()
    self.left_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
        actor.rotation_z = 90
      end
    )
    self:add_child(self.left_arrow_widget)

    self.right_arrow_widget = pfwidget.PfRect()
    self.right_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
        actor.rotation_z = -90
      end
    )
    self:add_child(self.right_arrow_widget)

    self.text_widget = pfwidget.PfText{font = font}
    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.button_text
        actor.text = self.value
      end
    )
    self:add_child(self.text_widget)

    local update_func = function(delta_seconds)
      if self.focused --[[ or pfutil.is_mouse_over(self.bg_widget.actor) ]] then
        local left_down = pfinput.is_key_down("DeviceButton_left")
        local right_down = pfinput.is_key_down("DeviceButton_right")

        if left_down or right_down then
          self.left_shadow_widget.color = pfconfig.colors.button_accent_press
          self.right_shadow_widget.color = pfconfig.colors.button_accent_press
        else
          self.left_shadow_widget.color = pfconfig.colors.button_accent_hover
          self.right_shadow_widget.color = pfconfig.colors.button_accent_hover
        end

        self.left_arrow_widget.alpha = left_down and 1 or 0.25
        self.right_arrow_widget.alpha = right_down and 1 or 0.25
      else
        self.left_shadow_widget.color = pfconfig.colors.shadow
        self.right_shadow_widget.color = pfconfig.colors.shadow
      end

      if self.last_update_time + 0.05 <= pftimer.elapsed_seconds then
        self.last_update_time = pftimer.elapsed_seconds

        if self.right_pressed_time ~= class.NULL then
          local delta = (pftimer.elapsed_seconds - 0.4) - self.right_pressed_time
          if delta > 0 then
            for _, v in ipairs(spinbox_increments) do
              if delta > v[1] then
                self.value = self.value + (v[2] * self.increment_size)
                break
              end
            end
          end

        elseif self.left_pressed_time ~= class.NULL then
          local delta = (pftimer.elapsed_seconds - 0.4) - self.left_pressed_time
          if delta > 0 then
            for _, v in ipairs(spinbox_increments) do
              if delta > v[1] then
                self.value = self.value - (v[2] * self.increment_size)
                break
              end
            end
          end
        end
      end
    end

    pftimer.update_signal:connect(update_func)
    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
      end
    )
  end,

  handle_input_event = function(self, event)
    if event.type == "InputEventType_FirstPress" then
      if event.DeviceInput.button == "DeviceButton_left" then
        self.left_pressed_time = pftimer.elapsed_seconds
        self.last_update_time = pftimer.elapsed_seconds
        self.value = self.value - self.increment_size
        return true

      elseif event.DeviceInput.button == "DeviceButton_right" then
        self.right_pressed_time = pftimer.elapsed_seconds
        self.last_update_time = pftimer.elapsed_seconds
        self.value = self.value + self.increment_size
        return true
      end
    end

    if event.type == "InputEventType_Repeat" then
      return event.DeviceInput.button == "DeviceButton_left" or
        event.DeviceInput.button == "DeviceButton_right"
    end

    if event.type == "InputEventType_Release" then
      if event.DeviceInput.button == "DeviceButton_left" then
        self.left_pressed_time = class.NULL
        return true
      elseif event.DeviceInput.button == "DeviceButton_right" then
        self.right_pressed_time = class.NULL
        return true
      end
    end

    return false
  end,
}

pfwidget.PfComboBox = class.strict(pfwidget.PfCanvas) {
  bg_widget = class.NULL,
  left_shadow_widget = class.NULL,
  right_shadow_widget = class.NULL,
  text_widget = class.NULL,
  all_values_canvas = class.NULL,
  all_values_bg = class.NULL,
  all_values_text_widget = class.NULL,
  left_arrow_widget = class.NULL,
  right_arrow_widget = class.NULL,

  left_pressed_time = class.NULL,
  right_pressed_time = class.NULL,
  last_update_time = 0,

  _selected_index = 1,
  selected_index = class.property {
    get = function(self) return self._selected_index end,
    set = function(self, new)
      local old = self._selected_index
      local num_values = #self._values

      if new > num_values then
        new = 1
      elseif new < 1 then
        new = num_values
      end

      self._selected_index = new

      self:update_text()
      self:update_all_values_widget()

      if old ~= new then
        self.value_changed_signal:emit(self._values[new], new)
      end
    end,
  },

  selected_value = class.property {
    get = function(self) return self._values[self._selected_index] end,
    set = function(self, new)
      for k, v in pairs(self.values) do
        if v.userdata == new then
          self.selected_index = k
          return
        end
      end
    end,
  },

  value_changed_signal = class.NULL,

  _values = class.NULL, -- A table of tables, where the inner tables must contain "name" and "userdata" as properties
  values = class.property {
    get = function(self) return self._values end,
    set = function(self, new)
      self._values = new

      local all = {}
      for _, v in pairs(new) do
        table.insert(all, v.name)
      end
      self.all_values_text_widget.text = table.concat(all, "\n")

      self.all_values_bg.height = #new *
        pfconfig.font_info[pfconfig.fonts.regular_40].line_spacing *
        self.all_values_text_widget.scale_y +
        16

      self:update_text()
    end,
  },

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.bg_widget.width = new
      self.right_shadow_widget.x = new
      self.right_arrow_widget.x = new - 24
      self.text_widget.x = self.width / 2
      self.all_values_bg.width = new
      self.all_values_text_widget.x = self.width / 2
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self:_set_height(new)
    end,
  },

  _set_height = function(self, new)
    self.actor:SetHeight(new)
    self.bg_widget.height = new
    self.left_shadow_widget.height = new
    self.right_shadow_widget.height = new
    self.left_arrow_widget.y = new / 2
    self.right_arrow_widget.y = new / 2
    self.text_widget.y = new / 2
  end,

  focus = function(self)
    self.focused = true
    return true
  end,

  text = class.property {
    get = function(self) return self.text_widget.text end,
    set = function(self, new) self.text_widget.text = new end,
  },

  text_scale = class.property {
    get = function(self) return self.text_widget.scale end,
    set = function(self, new) self.text_widget.scale = new end,
  },

  __init = function(self, args)
    pfwidget.PfCanvas.__init(self)

    self.is_focusable = true

    self.value_changed_signal = PfSignal()

    if not args then args = {} end
    local font = args.font or pfconfig.fonts.light_40

    self.bg_widget = pfwidget.PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1, 1)
        actor.color = pfconfig.colors.button_background
      end
    )
    self:add_child(self.bg_widget)

    self.left_shadow_widget = pfwidget.PfRect()
    self.left_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(1, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_left = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.left_shadow_widget)

    self.right_shadow_widget = pfwidget.PfRect()
    self.right_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_right = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.right_shadow_widget)

    self.text_widget = pfwidget.PfText{font = font}
    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.button_text
      end
    )
    self:add_child(self.text_widget)

    self.all_values_canvas = pfwidget.PfCanvas()
    self.all_values_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.alpha = 0
      end
    )
    self:add_child(self.all_values_canvas)

    self.all_values_bg = pfwidget.PfRect()
    self.all_values_bg:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -16)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.panel_background
      end
    )
    self.all_values_canvas:add_child(self.all_values_bg)

    self.all_values_text_widget = pfwidget.PfText{font = font}
    self.all_values_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.scale = 0.5
        actor.color = pfconfig.colors.button_text
      end
    )
    self.all_values_canvas:add_child(self.all_values_text_widget)

    self.left_arrow_widget = pfwidget.PfRect()
    self.left_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
        actor.rotation_z = 90
      end
    )
    self:add_child(self.left_arrow_widget)

    self.right_arrow_widget = pfwidget.PfRect()
    self.right_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        actor.alpha = 0.25
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
        actor.rotation_z = -90
      end
    )
    self:add_child(self.right_arrow_widget)

    local update_func = function(delta_seconds)
      if self.focused --[[ or pfutil.is_mouse_over(self.bg_widget.actor) ]] then
        local left_down = pfinput.is_key_down("DeviceButton_left")
        local right_down = pfinput.is_key_down("DeviceButton_right")

        if left_down or right_down then
          self.left_shadow_widget.color = pfconfig.colors.button_accent_press
          self.right_shadow_widget.color = pfconfig.colors.button_accent_press
        else
          self.left_shadow_widget.color = pfconfig.colors.button_accent_hover
          self.right_shadow_widget.color = pfconfig.colors.button_accent_hover
        end

        self.left_arrow_widget.alpha = left_down and 1 or 0.25
        self.right_arrow_widget.alpha = right_down and 1 or 0.25
      else
        self.left_shadow_widget.color = pfconfig.colors.shadow
        self.right_shadow_widget.color = pfconfig.colors.shadow
      end

      if self.last_update_time + 0.05 <= pftimer.elapsed_seconds then
        self.last_update_time = pftimer.elapsed_seconds

        if self.right_pressed_time ~= class.NULL then
          local delta = (pftimer.elapsed_seconds - 0.4) - self.right_pressed_time
          if delta > 0 then
            self.selected_index = self.selected_index + 1
          end

        elseif self.left_pressed_time ~= class.NULL then
          local delta = (pftimer.elapsed_seconds - 0.4) - self.left_pressed_time
          if delta > 0 then
            self.selected_index = self.selected_index - 1
          end
        end
      end
    end

    -- We need the overlay to draw on top of stuff
    self:add_hook(
      "InitCommand",
      function(actor)
        -- actor.draw_order = 100
      end
    )

    pftimer.update_signal:connect(update_func)
    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.update_signal:disconnect(update_func)
        self.ctx.anim.stop_animating_table(self.all_values_canvas)
      end
    )
  end,

  update_all_values_widget_immediate = function(self)
    local line_spacing = pfconfig.font_info[pfconfig.fonts.regular_40].line_spacing * self.all_values_text_widget.scale_y
    local leftover_space = self.height - line_spacing
    self.all_values_canvas.y = line_spacing * -1 * (self.selected_index - 1) + (line_spacing / 4) + (leftover_space / 2)
  end,

  update_all_values_widget = function(self)
    local line_spacing = pfconfig.font_info[pfconfig.fonts.regular_40].line_spacing * self.all_values_text_widget.scale_y
    local leftover_space = self.height - line_spacing
    local target_y = line_spacing * -1 * (self.selected_index - 1) + (line_spacing / 4) + (leftover_space / 2)

    self.ctx.anim{
      start_val = self.all_values_canvas.y,
      end_val = target_y,
      prop_table = self.all_values_canvas,
      prop_name = "y",
      duration = 0.2,
      easing = pfeasing.out_expo,
    }
  end,

  handle_focus_in = function(self)
    pfwidget.PfCanvas.handle_focus_in(self)

    self.draw_order = 100

    self:update_all_values_widget_immediate()

    self.ctx.anim{
      start_val = self.all_values_canvas.alpha,
      end_val = 1,
      prop_table = self.all_values_canvas,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }
  end,

  handle_focus_out = function(self)
    pfwidget.PfCanvas.handle_focus_in(self)

    self.draw_order = 0

    self.ctx.anim{
      start_val = self.all_values_canvas.alpha,
      end_val = 0,
      prop_table = self.all_values_canvas,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }
  end,

  update_text = function(self)
    if #self.values > 0 then
      self.text_widget.text = self.values[self.selected_index].name
    else
      self.text_widget.text = pflang.get_entry("no_options")
    end
  end,

  handle_input_event = function(self, event)
    if event.type == "InputEventType_FirstPress" then
      if event.DeviceInput.button == "DeviceButton_left" then
        self.left_pressed_time = pftimer.elapsed_seconds
        self.last_update_time = pftimer.elapsed_seconds
        self.selected_index = self.selected_index - 1
        return true

      elseif event.DeviceInput.button == "DeviceButton_right" then
        self.right_pressed_time = pftimer.elapsed_seconds
        self.last_update_time = pftimer.elapsed_seconds
        self.selected_index = self.selected_index + 1
        return true
      end
    end

    if event.type == "InputEventType_Repeat" then
      return event.DeviceInput.button == "DeviceButton_left" or
        event.DeviceInput.button == "DeviceButton_right"
    end

    if event.type == "InputEventType_Release" then
      if event.DeviceInput.button == "DeviceButton_left" then
        self.left_pressed_time = class.NULL
        return true
      elseif event.DeviceInput.button == "DeviceButton_right" then
        self.right_pressed_time = class.NULL
        return true
      end
    end

    return false
  end,
}

pfwidget.PfTextInput = class.strict(pfwidget.PfCanvas) {
  bg_widget = class.NULL,
  text_widget = class.NULL,
  insert_point_widget = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.bg_widget.width = new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)
      self.bg_widget.height = new
      self.text_widget.y = new / 2 - 1
      self.insert_point_widget.height = new - 20
      self.insert_point_widget.y = new / 2
    end,
  },

  focus = function(self)
    self.focused = true
    return true
  end,

  _text = class.NULL,
  text = class.property {
    get = function(self) return self._text end,
    set = function(self, new)
      local text_changed = self._text ~= new
      self._text = new
      self:_update_text()

      if text_changed then
        self.text_changed_signal:emit(new)
      end
    end,
  },

  _placeholder_text = class.NULL,
  placeholder_text = class.property {
    get = function(self) return self._placeholder_text end,
    set = function(self, new)
      self._placeholder_text = new
      if self.text == "" then
        self:_show_placeholder_text()
      end
    end,
  },

  _insert_point = class.NULL,
  insert_point = class.property {
    get = function(self) return self._insert_point end,
    set = function(self, new)
      self._insert_point = new
      self:_update_text()
    end,
  },

  is_password = class.NULL,

  handle_focus_in = function(self)
    pfwidget.PfCanvas.handle_focus_in(self)

    self.insert_point_widget.visible = true
    self.insert_point = #(self.text)
  end,

  handle_focus_out = function(self)
    pfwidget.PfCanvas.handle_focus_out(self)

    self.insert_point_widget.visible = false
  end,

  text_scale = class.property {
    get = function(self) return self.text_widget.scale end,
    set = function(self, new) self.text_widget.scale = new end,
  },

  text_changed_signal = class.NULL,
  char_inserted_signal = class.NULL,
  char_deleted_signal = class.NULL,
  update_attributes_signal = class.NULL,
  on_return_signal = class.NULL,
  on_escape_signal = class.NULL,

  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    self.is_focusable = true

    self._text = ""
    self._placeholder_text = ""
    self._insert_point = 0
    self.is_password = false
    self.text_changed_signal = PfSignal()
    self.char_inserted_signal = PfSignal()
    self.char_deleted_signal = PfSignal()
    self.update_attributes_signal = PfSignal()
    self.on_return_signal = PfSignal()
    self.on_escape_signal = PfSignal()

    self.bg_widget = pfwidget.PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1, 1)
        actor.color = pfconfig.colors.text_input_background
      end
    )
    self:add_child(self.bg_widget)

    self.text_widget = pfwidget.PfText{}
    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.text_input_text
      end
    )
    self:add_child(self.text_widget)

    self.insert_point_widget = pfwidget.PfRect()
    self.insert_point_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, 0)
        actor.align = Vec2(0, 0.5)
        actor.size = Vec2(4, 1)
        actor.color = pfconfig.colors.text_input_text
        actor.actor:diffuseshift()
        actor.actor:effectcolor1(pfconfig.colors.text_input_insert_point)
        actor.actor:effectcolor2(Alpha(pfconfig.colors.text_input_insert_point, 0))
        actor.visible = false
      end
    )
    self:add_child(self.insert_point_widget)
  end,

  set_text_no_notify = function(self, new)
    self._text = new
    self:_update_text()
  end,

  handle_input_event = function(self, event)
    if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
      if pfinput.is_ctrl_down() and event.DeviceInput.button == "DeviceButton_v" then
        -- Paste
        local clipboard = pfplatform.get_clipboard_text():gsub("\n", " ")
        local length = clipboard:len()

        local prev_insert_point = self._insert_point
        self._insert_point = self._insert_point + length
        self._text = self.text:sub(0, prev_insert_point) .. clipboard .. self.text:sub(prev_insert_point + 1)

        self:_update_text()
        self.text_changed_signal:emit(self._text)
        return true

      elseif event.DeviceInput.button == "DeviceButton_backspace" then
        if self._insert_point > 0 then
          local prev_insert_point = self._insert_point
          self._insert_point = math.max(self._insert_point - 1, 0)
          local c = self.text:sub(prev_insert_point, prev_insert_point)
          self._text = self.text:sub(0, prev_insert_point - 1) .. self.text:sub(prev_insert_point + 1)

          self.char_deleted_signal:emit(c)
          self:_update_text()
          self.text_changed_signal:emit(self._text)
        end
        return true

      elseif event.DeviceInput.button == "DeviceButton_delete" then
          if self._insert_point < #self.text then
            local c = self.text:sub(self._insert_point + 1, self._insert_point + 1)
            self._text = self.text:sub(0, self._insert_point) .. self.text:sub(self._insert_point + 2)

            self.char_deleted_signal:emit(c)
            self:_update_text()
            self.text_changed_signal:emit(self._text)
          end
          return true

      elseif event.DeviceInput.button == "DeviceButton_left" then
        self._insert_point = math.max(self._insert_point - 1, 0)
        self:_update_text()
        return true

      elseif event.DeviceInput.button == "DeviceButton_right" then
        self._insert_point = math.min(self._insert_point + 1, #self.text + 1)
        self:_update_text()
        return true

      elseif event.DeviceInput.button == "DeviceButton_enter" then
        local returns = self.on_return_signal:emit()
        for _, v in pairs(returns) do
          if v then return true end
        end

      elseif event.DeviceInput.button == "DeviceButton_escape" then
        local returns = self.on_escape_signal:emit()
        for _, v in pairs(returns) do
          if v then return true end
        end

      else
        local c = pfinput.device_button_to_char(event.DeviceInput.button)
        if c then
          local prev_insert_point = self._insert_point
          self._insert_point = self._insert_point + 1
          self._text = self.text:sub(0, prev_insert_point) .. c .. self.text:sub(prev_insert_point + 1)

          self.char_inserted_signal:emit(c)
          self:_update_text()
          self.text_changed_signal:emit(self._text)

          return true
        end
      end
    end
  end,

  _show_placeholder_text = function(self)
    self.text_widget.text = self.placeholder_text
    self.text_widget.color = pfconfig.colors.text_input_placeholder_text
  end,

  _update_text = function(self)
    if self.text == "" then
      self:_show_placeholder_text()
      self.insert_point_widget.x = 24
    else
      -- Not the prettiest but it'll work
      local display_text
      if self.is_password then
        display_text = string.rep("*", #self.text)
      else
        display_text = self.text
      end

      self.text_widget.text = display_text:sub(0, self._insert_point)
      self.insert_point_widget.x = self.text_widget.width * self.text_widget.scale_x + 24
      self.text_widget.text = display_text
      self.text_widget.color = pfconfig.colors.text_input_text

      self.update_attributes_signal:emit(self.text, self.text_widget)
    end
  end,
}

pfwidget.PfProgressBar = class.strict(pfwidget.PfCanvas) {
  bg_widget = class.NULL,
  left_shadow_widget = class.NULL,
  right_shadow_widget = class.NULL,
  bar_widget = class.NULL,
  glow_widget = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.bg_widget.width = new
      self.right_shadow_widget.x = new
      self.bar_widget.width = new * self._progress
      self.glow_widget.x = self.width * new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self:_set_height(new)
    end,
  },

  _set_height = function(self, new)
    self.actor:SetHeight(new)
    self.bg_widget.height = new
    self.left_shadow_widget.height = new
    self.right_shadow_widget.height = new
    self.bar_widget.height = new
    self.glow_widget.y = -12
    self.glow_widget.height = new + 24
  end,

  _progress = 0,
  progress = class.property {
    get = function(self) return self._progress end,
    set = function(self, new)
      self._progress = new
      self.bar_widget.width = self.width * new
      self.glow_widget.x = self.width * new
    end,
  },

  __init = function(self)
    pfwidget.PfCanvas.__init(self)

    self.bg_widget = pfwidget.PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1, 1)
        actor.color = pfconfig.colors.button_background
      end
    )
    self:add_child(self.bg_widget)

    self.left_shadow_widget = pfwidget.PfRect()
    self.left_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(1, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_left = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.left_shadow_widget)

    self.right_shadow_widget = pfwidget.PfRect()
    self.right_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_right = 1
        actor.size = Vec2(8, 1)
      end
    )
    self:add_child(self.right_shadow_widget)

    self.bar_widget = pfwidget.PfRect()
    self.bar_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(0, 1)
        actor.color = pfconfig.colors.progress_bar_fill
        actor.color_right = pfconfig.colors.progress_bar_fill_accent
      end
    )
    self:add_child(self.bar_widget)

    self.glow_widget = pfwidget.PfRect()
    self.glow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(4, 0)
        actor.color = pfconfig.colors.progress_bar_glow
        actor.fade_left = 0.2
        actor.fade_right = 0.2
        actor.fade_top = 0.5
        actor.fade_bottom = 0.5
      end
    )
    self:add_child(self.glow_widget)
  end,
}

pfwidget.PfSound = class.strict(pfwidget.PfActor) {
  _init_options = class.NULL,

  volume = class.property {
    get = function(self) error("Cannot get volume from RageSound >.<") end,
    set = function(self, new) self.actor:get():volume(new) end,
  },

  __init = function(self, args)
    pfwidget.PfActor.__init(self)

    self._init_options = {}
    self._init_options.SupportPan = args.support_pan
    self._init_options.SupportRateChanging = args.support_rate_changing
    self._init_options.IsAction = args.is_action
  end,

  load_sound = function(self, path)
    self.actor:load(path)
  end,

  play = function(self)
    self.actor:play()
  end,

  stop = function(self)
    self.actor:stop()
  end,

  set_start_time = function(self, time)
    self.actor:get():SetParam("StartSecond", time);
  end,

  get_sound_length = function(self)
    return self.actor:get():get_length()
  end,

  _get_actor_init_table = function(self)
    local t = pfwidget.PfActor._get_actor_init_table(self)
    for k, v in pairs(self._init_options) do
      t[k] = v
    end

    return t
  end,

  _get_actor_type = function(self)
    return Def.Sound
  end,
}

return pfwidget
