local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")

local pfanim = {}

pfanim.active = {}

function pfanim.on_update(delta_seconds)
  -- Iterate backwards because we remove stuff from the table while iterating
  for i = #pfanim.active, 1, -1 do
    local obj = pfanim.active[i]

    -- Remove from the active list if we're beyond the duration
    if obj.time > obj.duration then
      table.remove(pfanim.active, i)
    end

    obj.time = obj.time + delta_seconds
    obj.prop_table[obj.prop_name] = obj.easing(
      math.min(obj.time, obj.duration),
      obj.start_val,
      obj.end_val - obj.start_val,
      obj.duration
    )
  end

  return #pfanim.active > 0
end

function pfanim.animate(args)
  local obj = {
    start_val = args.start_val or 0,
    end_val = args.end_val or 1,
    prop_table = args.prop_table or nil,
    prop_name = args.prop_name or nil,
    duration = args.duration or 1,
    time = args.time or 0,
    easing = args.easing or pfeasing.linear,
  }

  local toremove = nil
  for k, v in pairs(pfanim.active) do
    if v.prop_table == args.prop_table and v.prop_name == args.prop_name then
      toremove = k
      break
    end
  end

  if toremove then
    table.remove(pfanim.active, toremove)
  end

  table.insert(pfanim.active, obj)

  obj.prop_table[obj.prop_name] = obj.start_val
end

function pfanim.stop_animating_table(prop_table)
  for i = #pfanim.active, 1, -1 do
    local obj = pfanim.active[i]

    if obj.prop_table == prop_table then
      table.remove(pfanim.active, i)
    end
  end
end

function pfanim.stop_animating_property(prop_table, prop_name)
  for i = #pfanim.active, 1, -1 do
    local obj = pfanim.active[i]

    if obj.prop_table == prop_table and obj.prop_name == prop_name then
      table.remove(pfanim.active, i)
    end
  end
end

function pfanim.recurse_stop_animating_actor(actor)
  if actor.pf_widget then
    pfanim.stop_animating_table(actor.pf_widget)
  end

  if #actor == 0 then
    -- It's an actor
    if actor.GetChildren then
      -- It's an actor frame
      for _, v in pairs(actor:GetChildren()) do
        pfanim.recurse_stop_animating_actor(v)
      end
    end

  else
    -- It's a table of actors
    for _, v in pairs(actor) do
      pfanim.recurse_stop_animating_actor(v)
    end
  end
end

pftimer.connect_tick_weak(pfanim.on_update)

return pfanim
