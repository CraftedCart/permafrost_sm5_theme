local pfanim = require("permafrost.pfanim")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")

local PfSignal = pfsignal.PfSignal

local pfscreenctx = {}

function pfscreenctx.new(root_actor)
  local ctx = {}

  ctx.animated_objects = {}

  local anim_overrides = {
    animate = function(args)
      -- The 1 can be *anything*, as long as it's not nil
      -- essentially we're using table keys as a set here
      ctx.animated_objects[args.prop_table] = 1

      return pfanim.animate(args)
    end,
  }
  ctx.anim = setmetatable(
    anim_overrides,
    {
      __index = pfanim,
      __call = function(t, args)
        anim_overrides.animate(args)
      end,
    }
    )

  ctx.delayed_actions = {}
  ctx.tick_connections = {}
  ctx.timer = setmetatable(
    {
      run_in_seconds = function(seconds, func)
        local action = pftimer.run_in_seconds(seconds, func)

        -- The 1 can be *anything*, as long as it's not nil
        -- essentially we're using table keys as a set here
        ctx.delayed_actions[action] = 1

        return action
      end,

      connect_tick = function(slot)
        -- The 1 can be *anything*, as long as it's not nil
        -- essentially we're using table keys as a set here
        ctx.tick_connections[slot] = 1

        return pftimer.connect_tick(slot)
      end,

      connect_tick_weak = function(slot)
        -- The 1 can be *anything*, as long as it's not nil
        -- essentially we're using table keys as a set here
        ctx.tick_connections[slot] = 1

        return pftimer.connect_tick_weak(slot)
      end,
    },
    {__index = pftimer}
    )

  root_actor.end_signal:connect(
    function()
      for k, _ in pairs(ctx.animated_objects) do
        pfanim.stop_animating_table(k)
      end

      for k, _ in pairs(ctx.delayed_actions) do
        pftimer.cancel_delayed_action(k)
      end

      for k, _ in pairs(ctx.tick_connections) do
        pftimer.disconnect_tick(k)
      end
    end
    )

  return ctx
end

return pfscreenctx
