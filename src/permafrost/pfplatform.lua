local pfplatform = {}

-- https://gist.github.com/soulik/82e9d02a818ce12498d1
local function get_os()
  local raw_os_name, raw_arch_name = '', ''

  -- LuaJIT shortcut
  if jit and jit.os and jit.arch then
    raw_os_name = jit.os
    raw_arch_name = jit.arch
  else
    -- is popen supported?
    local popen_status, popen_result
    if io then popen_status, popen_result = pcall(io.popen, "") end
    if popen_status then
      popen_result:close()
      -- Unix-based OS
      raw_os_name = io.popen('uname -s','r'):read('*l')
      raw_arch_name = io.popen('uname -m','r'):read('*l')
    elseif os then
      -- Windows
      local env_OS = os.getenv('OS')
      local env_ARCH = os.getenv('PROCESSOR_ARCHITECTURE')
      if env_OS and env_ARCH then
        raw_os_name, raw_arch_name = env_OS, env_ARCH
      end
    end
  end

  raw_os_name = (raw_os_name):lower()
  raw_arch_name = (raw_arch_name):lower()

  local os_patterns = {
    ['windows'] = 'Windows',
    ['linux'] = 'Linux',
    ['mac'] = 'Mac',
    ['darwin'] = 'Mac',
    ['^mingw'] = 'Windows',
    ['^cygwin'] = 'Windows',
    ['bsd$'] = 'BSD',
    ['SunOS'] = 'Solaris',
  }

  local arch_patterns = {
    ['^x86$'] = 'x86',
    ['i[%d]86'] = 'x86',
    ['amd64'] = 'x86_64',
    ['x86_64'] = 'x86_64',
    ['Power Macintosh'] = 'powerpc',
    ['^arm'] = 'arm',
    ['^mips'] = 'mips',
  }

  local os_name, arch_name = 'unknown', 'unknown'

  for pattern, name in pairs(os_patterns) do
    if raw_os_name:match(pattern) then
      os_name = name
      break
    end
  end
  for pattern, name in pairs(arch_patterns) do
    if raw_arch_name:match(pattern) then
      arch_name = name
      break
    end
  end
  return os_name, arch_name
end

pfplatform.os, pfplatform.arch = get_os()

pfplatform.is_windows = pfplatform.os == "Windows"
pfplatform.is_mac = pfplatform.os == "Mac"
pfplatform.is_linux = pfplatform.os == "Linux"

if pfplatform.is_windows then
  -- TODO
  pfplatform.has_command = function(command) return false end
  pfplatform.get_clipboard_text = function()
    SCREENMAN:SystemMessage("Clipboard unsupported on this platform")
    return ""
  end

elseif pfplatform.mac then
  -- TODO
  pfplatform.has_command = function(command) return false end
  pfplatform.get_clipboard_text = function()
    SCREENMAN:SystemMessage("Clipboard unsupported on this platform")
    return ""
  end

elseif pfplatform.is_linux then
  pfplatform.has_command = function(command)
    local f = io.popen("which " .. command)
    local content = f:read("*all")
    f:close()

    return content ~= ""
  end

  if pfplatform.has_command("xclip") then
    pfplatform.get_clipboard_text = function()
      local f = io.popen("xclip --selection clipboard -o")
      local content = f:read("*all")
      f:close()

      return content
    end
  else
    pfplatform.get_clipboard_text = function()
      SCREENMAN:SystemMessage("Please install 'xclip' for clipboard support on Linux")
      return ""
    end
  end

else
  pfplatform.has_command = function() return false end
  pfplatform.get_clipboard_text = function()
    SCREENMAN:SystemMessage("Clipboard unsupported on this platform")
    return ""
  end

end

if os then
  pfplatform.get_current_time_formatted = function()
    return os.date("%X", os.time())
  end

else
  pfplatform.get_current_time_formatted = function()
    return string.format("%02d:%02d:%02d", Hour(), Minute(), Second())
  end

end

return pfplatform
