local pfinput = {}

-- Enum
pfinput.EInputLayerType = {
  NORMAL = {},
  DEMAND_FOCUS = {},
}

pfinput.keys_down = {}
pfinput.input_layers = {}
pfinput.overlay_input_handlers = {}

function pfinput.handle_event(event)
  if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
    pfinput.keys_down[event.DeviceInput.button] = true
  else
    pfinput.keys_down[event.DeviceInput.button] = false
  end

  local top_input_layer
  if #pfinput.input_layers > 0 then
    top_input_layer = pfinput.input_layers[#pfinput.input_layers]
  end

  if (not top_input_layer) or top_input_layer.layer_type ~= pfinput.EInputLayerType.DEMAND_FOCUS then
    for _, v in pairs(pfinput.overlay_input_handlers) do
      -- Stop processing input if the callback signals it was handled
      if v(event) then return end
    end
  end

  if top_input_layer then
    for _, v in pairs(top_input_layer.handlers) do
      -- Stop processing input if the callback signals it was handled
      if v(event) then return end
    end
  end
end

function pfinput.clear_input_state()
  pfinput.keys_down = {}
end

function pfinput.push_input_layer(layer_type)
  table.insert(
    pfinput.input_layers,
    {
      layer_type = layer_type or pfinput.EInputLayerType.NORMAL,
      handlers = {},
    }
  )
end

function pfinput.pop_input_layer()
  pfinput.input_layers[#pfinput.input_layers] = nil
end

function pfinput.add_input_handler(func)
  table.insert(pfinput.input_layers[#pfinput.input_layers].handlers, func)
end

function pfinput.add_overlay_input_handler(func)
  table.insert(pfinput.overlay_input_handlers, func)
end

function pfinput.is_ctrl_down()
  return pfinput.keys_down["DeviceButton_left ctrl"] or pfinput.keys_down["DeviceButton_right ctrl"]
end

function pfinput.is_shift_down()
  return pfinput.keys_down["DeviceButton_left shift"] or pfinput.keys_down["DeviceButton_right shift"]
end

function pfinput.is_alt_down()
  return pfinput.keys_down["DeviceButton_left alt"] or pfinput.keys_down["DeviceButton_right alt"]
end

function pfinput.is_key_down(key)
  return pfinput.keys_down[key]
end

-- Woo for re-implementing stuff already in StepMania's C++ code base
-- which is bad anyway because this assumes a US QWERTY layout
-- TODO: Keyboard layouts...
local input_lut = Enum.Reverse(DeviceButton)
local char_lut = {
  ["DeviceButton_KP /"] = "/",
  ["DeviceButton_KP *"] = "*",
  ["DeviceButton_KP -"] = "-",
  ["DeviceButton_KP +"] = "+",
  ["DeviceButton_KP ."] = ".",
  ["DeviceButton_KP ="] = "=",
}
function pfinput.device_button_to_char(button)
  local idx = input_lut[button]
  local c = nil

  if idx < 127 then
    c = string.char(idx)
  elseif idx >= input_lut["DeviceButton_KP 0"] and idx <= input_lut["DeviceButton_KP 9"] then -- Keypad numbers
    c = string.char(idx - input_lut["DeviceButton_KP 0"] + string.byte("0"))
  elseif char_lut[button] then
    c = char_lut[button]
  end

  if c == nil then
    return nil
  end

  if pfinput.is_shift_down() then
    return pfinput.shifted_char(c)
  else
    return c
  end
end

local shifted_lut = {
  ["`"] = "~",
  ["1"] = "!",
  ["2"] = "@",
  ["3"] = "#",
  ["4"] = "$",
  ["5"] = "%",
  ["6"] = "^",
  ["7"] = "&",
  ["8"] = "*",
  ["9"] = "(",
  ["0"] = ")",
  ["-"] = "_",
  ["="] = "+",
  ["["] = "{",
  ["]"] = "}",
  ["'"] = "\"",
  ["\\"] = "|",
  [";"] = ":",
  [","] = "<",
  ["."] = ">",
  ["/"] = "?",
}
function pfinput.shifted_char(c)
  c = string.upper(c)
  local shifted = shifted_lut[c]
  if shifted then
    return shifted
  else
    return c
  end
end

return pfinput
