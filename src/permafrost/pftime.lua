local class = require("permafrost.class")
local pflang = require("permafrost.pflang")
local pfmath = require("permafrost.pfmath")
local bitty = require("permafrost.bitty")

local pftime = {}

pftime.MONTHS_PER_YEAR = 12
pftime.DAYS_PER_MONTH = 365 / 12
pftime.HOURS_PER_DAY = 24
pftime.MINUTES_PER_HOUR = 60

pftime.SECONDS_PER_MINUTE = 60
pftime.SECONDS_PER_HOUR = pftime.SECONDS_PER_MINUTE * pftime.MINUTES_PER_HOUR
pftime.SECONDS_PER_DAY = pftime.SECONDS_PER_HOUR * pftime.HOURS_PER_DAY
pftime.SECONDS_PER_MONTH = pftime.SECONDS_PER_DAY * pftime.DAYS_PER_MONTH
pftime.SECONDS_PER_YEAR = pftime.SECONDS_PER_MONTH * pftime.MONTHS_PER_YEAR

pftime.PfDuration = class.strict {
  total_seconds = class.NULL,

  seconds = class.property {
    get = function(self) return self.total_seconds - (self.total_minutes * pftime.SECONDS_PER_MINUTE) end,
    set = function(self, new) error("Cannot set a duration's seconds - use total_seconds instead") end
  },

  total_minutes = class.property {
    get = function(self) return math.floor(self.total_seconds / pftime.SECONDS_PER_MINUTE) end,
    set = function(self, new) error("Cannot set a duration's total minutes - use total_seconds instead") end
  },

  minutes = class.property {
    get = function(self) return self.total_minutes - (self.total_hours * pftime.MINUTES_PER_HOUR) end,
    set = function(self, new) error("Cannot set a duration's minutes - use total_seconds instead") end
  },

  total_hours = class.property {
    get = function(self) return math.floor(self.total_seconds / pftime.SECONDS_PER_HOUR) end,
    set = function(self, new) error("Cannot set a duration's total hours - use total_seconds instead") end
  },

  hours = class.property {
    get = function(self) return self.total_hours - (self.total_days * pftime.HOURS_PER_DAY) end,
    set = function(self, new) error("Cannot set a duration's hours - use total_seconds instead") end
  },

  total_days = class.property {
    get = function(self) return math.floor(self.total_seconds / pftime.SECONDS_PER_DAY) end,
    set = function(self, new) error("Cannot set a duration's total days - use total_seconds instead") end
  },

  days = class.property {
    get = function(self) return self.total_days - (self.total_months * pftime.DAYS_PER_MONTH) end,
    set = function(self, new) error("Cannot set a duration's days - use total_seconds instead") end
  },

  total_months = class.property {
    get = function(self) return math.floor(self.total_seconds / pftime.SECONDS_PER_MONTH) end,
    set = function(self, new) error("Cannot set a duration's total months - use total_seconds instead") end
  },

  months = class.property {
    get = function(self) return self.total_months - (self.total_years * pftime.MONTHS_PER_YEAR) end,
    set = function(self, new) error("Cannot set a duration's days - use total_seconds instead") end
  },

  total_years = class.property {
    get = function(self) return math.floor(self.total_seconds / pftime.SECONDS_PER_YEAR) end,
    set = function(self, new) error("Cannot set a duration's total years - use total_seconds instead") end
  },

  years = class.property {
    get = function(self) return self.total_years end,
    set = function(self, new) error("Cannot set a duration's days - use total_seconds instead") end
  },

  __init = function(self, seconds)
    self.total_seconds = seconds
  end,

  make_ago_str = function(self)
    local years = self.years
    local months = self.months
    if years > 0 then
      return pflang.format_entry{"ago_years_months", years = years, months = months}
    end

    local days = self.days
    if months > 0 then
      return pflang.format_entry{"ago_months_days", months = months, days = days}
    end

    if days > 0 then
      return pflang.format_entry{"ago_days", days = days}
    end

    local hours = self.hours
    local minutes = self.minutes
    if hours > 0 then
      return pflang.format_entry{"ago_hours_minutes", hours = hours, minutes = minutes}
    end

    if minutes > 0 then
      return pflang.format_entry{"ago_minutes", minutes = minutes}
    end

    local seconds = self.seconds
    if seconds > 0 then
      return pflang.format_entry{"ago_seconds", seconds = seconds}
    end

    return pflang.get_entry("ago_just_now")
  end,
}

pftime.PfInstant = class.strict {
  year = class.NULL,
  month = class.NULL,
  day = class.NULL,
  hour = class.NULL,
  minute = class.NULL,
  second = class.NULL,

  __init = function(self, year, month, day, hour, minute, second)
    self.year = year or 0
    self.month = month or 0
    self.day = day or 0
    self.hour = hour or 0
    self.minute = minute or 0
    self.second = second or 0
  end,

  -- Returns the time difference between the instant and the time `elapsed` is called
  elapsed = function(self)
    -- local now = os.time()
    -- local instant = os.time{
      -- year = self.year,
      -- month = self.month,
      -- day = self.day,
      -- hour = self.hour,
      -- min = self.minute,
      -- sec = self.second,
    -- }

    -- I smell timing issues, but StepMania doesn't have anything better as far as I can tell
    -- (Eg: What if the Minute changes between the various calls here)
    --
    -- For all I know, maybe that's not even possible ;P It could very well be possible that StepMania caches the
    -- current time and doesn't change it while Lua is executing
    local now = pftime.mktime{
      year = Year(),
      month = MonthOfYear() + 1,
      day = DayOfMonth(),
      hour = Hour(),
      min = Minute(),
      sec = Second(),
    }
    local instant = pftime.mktime{
      year = self.year,
      month = self.month,
      day = self.day,
      hour = self.hour,
      min = self.minute,
      sec = self.second,
    }

    local diff_seconds = now - instant
    return pftime.PfDuration(diff_seconds)
  end,

  __mt = {
    __eq = function(self, other)
      return self.year == other.year and
      self.month == other.month and
      self.day == other.day and
      self.hour == other.hour and
      self.minute == other.minute and
      self.second == other.second
    end,
  },
}

--- Parse a time string like "2020-07-12 20:47:51"
function pftime.instant_from_str(str)
  local year, month, day, hour, minute, second = str:match("(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)")
  return pftime.PfInstant(year, month, day, hour, minute, second)
end

-- Some of this date/time conversion stuff is taken from musl libc, which is licensed under the MIT
-- Thanks!

local SECS_THROUGH_MONTH = {
  0,
  31 * 86400,
  59 * 86400,
  90 * 86400,
  120 * 86400,
  151 * 86400,
  181 * 86400,
  212 * 86400,
  243 * 86400,
  273 * 86400,
  304 * 86400,
  334 * 86400,
}

local function month_to_secs(month, is_leap)
  local t = SECS_THROUGH_MONTH[month + 1]
  if is_leap and month >= 2 then
    t = t + 86400
  end
  return t
end

local function year_to_secs(year)
  local is_leap

  if year - 2 <= 136 then
    local y = year
    local leaps = bitty.brshift((y - 68), 2)
    if not bitty.band((y - 68), 3) then
      leaps = leaps - 1
      if is_leap then is_leap = true end
    elseif is_leap then
      is_leap = false
    end

    return 31536000 * (y - 70) + 86400 * leaps, is_leap
  end

  local centuries
  local leaps

  local cycles = (year - 100) / 400
  local rem = (year - 100) % 400
  if rem < 0 then
    cycles = cycles - 1
    rem = rem + 400
  end
  if rem == 0 then
    is_leap = true
    centuries = 0
    leaps = 0
  else
    if rem >= 200 then
      if rem >= 300 then
        centuries = 3
        rem = rem - 300
      else
        centuries = 2
        rem = rem - 200
      end
    else
      if rem >= 100 then
        centuries = 1
        rem = rem - 100
      else
        centuries = 0
      end
    end
    if rem == 0 then
      is_leap = false
      leaps = 0
    else
      leaps = rem / 4
      rem = rem % 4
      is_leap = not rem
    end
  end

  leaps = leaps + 97 * cycles + 24 * centuries - (is_leap and 1 or 0)

  return (year - 100) * 31536000 + leaps * 86400 + 946684800 + 86400, is_leap
end

local function tm_to_secs(tm)
  local year = tm.year
  local month = tm.month - 1 -- -1 as we start indexing from 0 here
  if month >= 12 or month < 0 then
    local adj = pfmath.truncate(month / 12)
    month = month % 12
    if month < 0 then
      adj = adj - 1
      month = month + 12
    end
    year = year + adj
  end

  local t, is_leap = year_to_secs(year)
  t = t + month_to_secs(month, is_leap)
  t = t + 86400 * (tm.day - 1)
  t = t + 3600 * tm.hour
  t = t + 60 * tm.min
  t = t + tm.sec

  return t
end

--- Kinda like the C function mktime
if os and os.time then
  pftime.mktime = os.time
else
  pftime.mktime = function(args)
    args.year = args.year or 0
    args.month = args.month or 0
    args.day = args.day or 0
    args.hour = args.hour or 0
    args.min = args.min or 0
    args.sec = args.sec or 0

    local t = tm_to_secs(args)

    -- Don't bother with timezone or DST stuff

    return t
  end
end

return pftime
