local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pfeditableui = require("permafrost.pfeditableui")
local pferrorbarwidget = require("permafrost.widget.pferrorbarwidget")
local pfskipwidget = require("permafrost.widget.pfskipwidget")
local pfcombowidget = require("permafrost.widget.pfcombowidget")
local pflifewidget = require("permafrost.widget.pflifewidget")
local pfautoplaywidget = require("permafrost.widget.pfautoplaywidget")
local pfjudgmentwidget = require("permafrost.widget.pfjudgmentwidget")
local pfsongbarwidget = require("permafrost.widget.pfsongbarwidget")
local pfsongnpswidget = require("permafrost.widget.pfsongnpswidget")
local pfuserconfig = require("permafrost.pfuserconfig")
local pfsignal = require("permafrost.pfsignal")

local Vec2 = pfmath.Vec2
local Aabb2 = pfmath.Aabb2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfSound = pfwidget.PfSound
local PfEditableUi = pfeditableui.PfEditableUi
local PfEditableWidget = pfeditableui.PfEditableWidget
local PfErrorBarWidget = pferrorbarwidget.PfErrorBarWidget
local PfSkipWidget = pfskipwidget.PfSkipWidget
local PfComboWidget = pfcombowidget.PfComboWidget
local PfAutoplayWidget = pfautoplaywidget.PfAutoplayWidget
local PfJudgmentWidget = pfjudgmentwidget.PfJudgmentWidget
local PfLifeWidget = pflifewidget.PfLifeWidget
local PfSongBarWidget = pfsongbarwidget.PfSongBarWidget
local PfSongNpsWidget = pfsongnpswidget.PfSongNpsWidget

local screen_gameplay = {}

function screen_gameplay.make(nf_config)
  local ui = PfEditableUi()

  ui.tap_judgment_signal = PfSignal()
  ui.hold_judgment_signal = PfSignal()
  ui.combo_changed_signal = PfSignal()
  ui.life_changed_signal = PfSignal()

  local canvas = PfCanvas()

  local life_bar = PfLifeWidget()
  life_bar:add_hook(
    "InitCommand",
    function(actor)
      -- actor.position = Vec2(SCREEN_CENTER_X + 160, 0)

      -- We update every tick for when we're in the customize gameplay screen, to have the lifebar move around as you
      -- adjust the notefield scale
      actor.ctx.timer.connect_tick(function(delta_seconds)
        actor.position = Vec2(SCREEN_CENTER_X + (320 * nf_config.scale), 0)
      end)
    end
    )
  ui.life_changed_signal:connect(function(args) life_bar:on_life_changed(args) end)
  canvas:add_child(life_bar)

  table.insert(
    ui.editable_widgets,
    PfEditableWidget(life_bar)
      :with_name("life_bar")
      :with_display_name_key("life_bar")
      :with_bounds(function()
        local pos = life_bar.position:to_vec2() - Vec2(4, 0)
        return Aabb2(pos, pos + Vec2(4, SCREEN_HEIGHT))
      end)
      :with_float_prop{"alpha", "opacity", increment = 0.01, min_value = 0, max_value = 1, format = "%.2f"}
    )

  local playfield_center_canvas = PfCanvas()
  playfield_center_canvas:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
    end
    )
  canvas:add_child(playfield_center_canvas)

  local error_bar = PfErrorBarWidget()
  error_bar:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(0, 60)
      actor.height = 32
    end
    )
  ui.tap_judgment_signal:connect(function(args) error_bar:on_tap_judgment(args) end)
  playfield_center_canvas:add_child(error_bar)

  table.insert(
    ui.editable_widgets,
    PfEditableWidget(error_bar)
      :with_name("error_bar")
      :with_display_name_key("error_bar")
      :with_bounds(function()
        local pos = error_bar.position:to_vec2() + Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
        local width = error_bar:get_display_width()
        return Aabb2(pos - Vec2(width / 2, 0), pos + Vec2(width / 2, error_bar.height))
      end)
      :with_float_prop{"y", "y_offset", increment = 1, format = "%.0f"}
      :with_float_prop{"alpha", "opacity", increment = 0.01, min_value = 0, max_value = 1, format = "%.2f"}
      -- Doesn't really work for now and idk why
      -- :with_float_prop{"width_multiplier", "width_multiplier", increment = 10, min_value = 0, format = "%.0f"} --
      :with_float_prop{"height", "height", increment = 1, min_value = 0, format = "%.0f"}
    )

  local combo_widget = PfComboWidget()
  combo_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(0, 100)
    end
    )
  ui.combo_changed_signal:connect(function(args) combo_widget:on_combo_changed(args) end)
  playfield_center_canvas:add_child(combo_widget)

  table.insert(
    ui.editable_widgets,
    PfEditableWidget(combo_widget)
      :with_name("combo")
      :with_display_name_key("combo")
      :with_bounds(function()
        local pos = combo_widget.position:to_vec2() + Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)
        return Aabb2(pos - Vec2(24, 0), pos + Vec2(24, 20))
      end)
      :with_float_prop{"y", "y_offset", increment = 1, format = "%.0f"}
      :with_float_prop{"alpha", "opacity", increment = 0.01, min_value = 0, max_value = 1, format = "%.2f"}
    )

  local judgment_widget = PfJudgmentWidget()
  judgment_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(0, 24)
    end
    )
  ui.tap_judgment_signal:connect(function(args) judgment_widget:on_tap_judgment(args) end)
  playfield_center_canvas:add_child(judgment_widget)

  table.insert(
    ui.editable_widgets,
    PfEditableWidget(judgment_widget)
      :with_name("judgment")
      :with_display_name_key("judgment")
      :with_bounds(function()
        local pos = judgment_widget.position:to_vec2() + Vec2(SCREEN_CENTER_X, SCREEN_CENTER_Y)

        local width = judgment_widget.judgment_widget.texture.sm_texture:GetImageWidth()
        local height = judgment_widget.judgment_widget.texture.sm_texture:GetImageHeight()

        local num_frames = judgment_widget.judgment_widget.texture.sm_texture:GetNumFrames()
        -- Assume 12 frames is a 2x6 sprite sheet, otherwise a 1x6
        if num_frames == 12 then
          width = width / 2
          height = height / (num_frames / 2)
        else
          height = height / num_frames
        end

        return Aabb2(pos - Vec2(width / 2, height / 2), pos + Vec2(width / 2, height / 2))
      end)
      :with_float_prop{"y", "y_offset", increment = 1, format = "%.0f"}
      :with_float_prop{"alpha", "opacity", increment = 0.01, min_value = 0, max_value = 1, format = "%.2f"}
    )

  local skip_widget = PfSkipWidget()
  skip_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(SCREEN_CENTER_X, SCREEN_HEIGHT)
    end
    )
  canvas:add_child(skip_widget)

  local autoplay_widget = PfAutoplayWidget()
  autoplay_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(SCREEN_CENTER_X, SCREEN_HEIGHT - 80)
    end
    )
  canvas:add_child(autoplay_widget)

  local song_bar_widget = PfSongBarWidget()
  song_bar_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(SCREEN_CENTER_X, 80)
    end
    )
  canvas:add_child(song_bar_widget)

  table.insert(
    ui.editable_widgets,
    PfEditableWidget(song_bar_widget)
      :with_name("song_bar")
      :with_display_name_key("song_bar")
      :with_bounds(function()
        local pos = song_bar_widget.position:to_vec2()
        local size = song_bar_widget.bar_bg_widget.size
        return Aabb2(pos - Vec2(size.x / 2, 0), pos + Vec2(size.x / 2, size.y))
      end)
      :with_float_prop{"alpha", "opacity", increment = 0.01, min_value = 0, max_value = 1, format = "%.2f"}
    )

  local song_nps_widget = PfSongNpsWidget()
  song_nps_widget:add_hook(
    "InitCommand",
    function(actor)
      actor.position = Vec2(SCREEN_CENTER_X - 512, 80 - 12)
      actor.width = 1024
      actor.height = 24
    end
    )
  canvas:add_child(song_nps_widget)

  -- Make hit sound actors
  local hit_sounds_enabled = pfuserconfig.public_config.notefield.hit_sound.file ~= nil
  local hit_sound_path = pfuserconfig.public_config.notefield.hit_sound.file
  local hit_sound_volume = pfuserconfig.public_config.notefield.hit_sound.volume
  ui.hit_sounds = {}
  ui.current_hit_sound_idx = 1

  for i = 1, 20 do
    local hit_sound = PfSound{
      support_pan = false,
      support_rate_changing = false,
      is_action = true,
    }
    hit_sound:add_hook(
      "InitCommand",
      function(actor)
        if hit_sound_path ~= nil then
          actor:load_sound(hit_sound_path)
        end
        actor.actor:get():volume(hit_sound_volume)
      end
      )
    canvas:add_child(hit_sound)

    table.insert(ui.hit_sounds, hit_sound)
  end
  ui.max_hit_sounds = #ui.hit_sounds

  ui.tap_judgment_signal:connect(
    function(args)
      -- Play a hitsound
      if hit_sounds_enabled then
        if args.tap_note_score ~= "TapNoteScore_Miss" then
          ui.hit_sounds[ui.current_hit_sound_idx]:play()
          ui.current_hit_sound_idx = ui.current_hit_sound_idx + 1
          if ui.current_hit_sound_idx > ui.max_hit_sounds then
            ui.current_hit_sound_idx = 1
          end
        end
      end
    end
    )

  ---
  -- @tparam sound permafrost.pffs.FileEntry|nil
  ui.set_hit_sound = function(self, sound)
    if sound ~= nil then
      for _, v in pairs(self.hit_sounds) do
        v:load_sound(sound.path)
      end

      hit_sounds_enabled = true
    else
      hit_sounds_enabled = false
    end
  end

  ui.set_hit_sound_volume = function(self, volume)
    for _, v in pairs(self.hit_sounds) do
      v.volume = volume
    end
  end

  -- Make combo break sound actors
  local combo_break_sounds_enabled = pfuserconfig.public_config.notefield.combo_break_sound.file ~= nil
  local combo_break_sound_path = pfuserconfig.public_config.notefield.combo_break_sound.file
  local combo_break_sound_volume = pfuserconfig.public_config.notefield.combo_break_sound.volume
  ui.combo_break_sounds = {}
  ui.current_combo_break_sound_idx = 1

  for i = 1, 3 do
    local combo_break_sound = PfSound{
      support_pan = false,
      support_rate_changing = false,
      is_action = true,
    }
    combo_break_sound:add_hook(
      "InitCommand",
      function(actor)
        if combo_break_sound_path ~= nil then
          actor:load_sound(combo_break_sound_path)
        end
        actor.actor:get():volume(combo_break_sound_volume)
      end
      )
    canvas:add_child(combo_break_sound)

    table.insert(ui.combo_break_sounds, combo_break_sound)
  end
  ui.max_combo_break_sounds = #ui.combo_break_sounds

  ---
  -- @tparam sound permafrost.pffs.FileEntry|nil
  ui.set_combo_break_sound = function(self, sound)
    if sound ~= nil then
      for _, v in pairs(self.combo_break_sounds) do
        v:load_sound(sound.path)
      end

      combo_break_sounds_enabled = true
    else
      combo_break_sounds_enabled = false
    end
  end

  ui.set_combo_break_sound_volume = function(self, volume)
    for _, v in pairs(self.combo_break_sounds) do
      v.volume = volume
    end
  end

  ui.combo_changed_signal:connect(
    function(args)
      if combo_break_sounds_enabled and args.old_combo > 30 and args.combo == 0 then
        -- Play a combo break sound
        ui.combo_break_sounds[ui.current_combo_break_sound_idx]:play()
        ui.current_combo_break_sound_idx = ui.current_combo_break_sound_idx + 1
        if ui.current_combo_break_sound_idx > ui.max_combo_break_sounds then
          ui.current_combo_break_sound_idx = 1
        end
      end
    end
    )

  ui.root_widget = canvas

  -- Load UI customizations
  local after_everything_canvas = PfCanvas()
  after_everything_canvas:add_hook(
    "InitCommand",
    function(actor)
      ui:deserialize_editable_widgets(pfuserconfig.public_config.notefield.ui_widgets)
    end
    )
  canvas:add_child(after_everything_canvas)

  ui.life_changed_signal:connect(function(args) life_bar:on_life_changed(args) end)
  return ui
end

return screen_gameplay
