local class = require("permafrost.class")

local pfsignal = {}

local get_traceback
if debug ~= nil and debug.traceback ~= nil then
  get_traceback = function()
    return debug.traceback()
  end
else
  get_traceback = function()
    return "<traceback unavailable>"
  end
end

local PfConnectionMeta = class.strict {
  traceback = class.NULL,

  __init = function(self)
    self.traceback = get_traceback()
  end,

  format_error = function(self, message)
    return "Error\n" ..
      "-----\n" ..
      message .. "\n" ..
      "\n" ..
      "Connected at\n" ..
      "------------\n" ..
      self.traceback .. "\n" ..
      "\n"
  end
}

pfsignal.PfSignal = class.strict {
  _connections = class.NULL,
  _connections_weak = class.NULL,
  _connections_once = class.NULL,

  __init = function(self)
    self._connections = {}
    self._connections_weak = {}
    self._connections_once = {}
    setmetatable(self._connections_weak, {__mode = "k"}) -- Weak keys
  end,

  emit = function(self, ...)
    local results = {}
    local errors = {}

    local to_disconnect = {}
    for callable, meta in pairs(self._connections) do
      local ok, ret = pcall(callable, ...)
      if not ok then
        table.insert(errors, meta:format_error(ret))
        table.insert(to_disconnect, v)
      else
        if ret ~= nil then table.insert(results, ret) end
      end
    end

    for callable, meta in pairs(self._connections_weak) do
      local ok, ret = pcall(callable, ...)
      if not ok then
        table.insert(errors, meta:format_error(ret))
        table.insert(to_disconnect, v)
      else
        if ret ~= nil then table.insert(results, ret) end
      end
    end

    for callable, meta in pairs(self._connections_once) do
      local ok, ret = pcall(callable, ...)
      if not ok then
        table.insert(errors, meta:format_error(ret))
      else
        if ret ~= nil then table.insert(results, ret) end
      end
    end
    self._connections_once = {}

    for _, v in pairs(to_disconnect) do
      self:disconnect(v)
    end

    if #errors > 0 then
      error(
        "Errors while emitting signal\n\n" ..
        table.concat(errors, "\n\n\n") ..
        "Emitted at\n" ..
        "----------\n" ..
        get_traceback() ..
        "\n\n\n" ..
        "============================================================\n" ..
        "If stuff looks wonky, try pressing F12 to reload the theme\n" ..
        "============================================================\n\n\n\n"
      )
    end

    return results
  end,

  connect = function(self, slot)
    self._connections[slot] = PfConnectionMeta()
  end,

  connect_weak = function(self, slot)
    self._connections_weak[slot] = PfConnectionMeta()
  end,

  connect_once = function(self, slot)
    self._connections_once[slot] = PfConnectionMeta()
  end,

  disconnect = function(self, slot)
    self._connections[slot] = nil
    self._connections_weak[slot] = nil
  end,
}

return pfsignal
