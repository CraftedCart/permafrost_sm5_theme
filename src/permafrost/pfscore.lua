local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfiter = require("permafrost.pfiter")
local pftime = require("permafrost.pftime")
local pfutil = require("permafrost.pfutil")
local class = require("permafrost.class")

local Iterator = pfiter.Iterator

local pfscore = {}

local PfGrade = class.strict {
  name_key = class.NULL,
  color = class.NULL,

  _cached_name = class.NULL,
  _cached_name_lang = class.NULL,
  name = class.property {
    get = function(self)
      if self._cached_name_lang ~= THEME:GetCurLanguage() then
        self._cached_name = pflang.get_entry(self.name_key)
      end

      return self._cached_name
    end,
    set = function(self, new) error("Cannot set a grade's name - use name_key instead") end,
  },

  __init = function(self, args)
    self.name_key = args.name_key
    self.color = args.color
  end,
}

if pfutil.is_etterna then
  pfscore.EGrade = {
    Grade1 = PfGrade{ name_key = "grade_etterna_1", color = pfconfig.colors.grade_0 }, -- AAAAA
    Grade2 = PfGrade{ name_key = "grade_etterna_2", color = pfconfig.colors.grade_1 }, -- AAAA++
    Grade3 = PfGrade{ name_key = "grade_etterna_3", color = pfconfig.colors.grade_1 }, -- AAAA+
    Grade4 = PfGrade{ name_key = "grade_etterna_4", color = pfconfig.colors.grade_1 }, -- AAAA
    Grade5 = PfGrade{ name_key = "grade_etterna_5", color = pfconfig.colors.grade_2 }, -- AAA++
    Grade6 = PfGrade{ name_key = "grade_etterna_6", color = pfconfig.colors.grade_2 }, -- AAA+
    Grade7 = PfGrade{ name_key = "grade_etterna_7", color = pfconfig.colors.grade_2 }, -- AAA
    Grade8 = PfGrade{ name_key = "grade_etterna_8", color = pfconfig.colors.grade_3 }, -- AA++
    Grade9 = PfGrade{ name_key = "grade_etterna_9", color = pfconfig.colors.grade_3 }, -- AA+
    Grade10 = PfGrade{ name_key = "grade_etterna_10", color = pfconfig.colors.grade_3 }, -- AA
    Grade11 = PfGrade{ name_key = "grade_etterna_11", color = pfconfig.colors.grade_4 }, -- A++
    Grade12 = PfGrade{ name_key = "grade_etterna_12", color = pfconfig.colors.grade_4 }, -- A+
    Grade13 = PfGrade{ name_key = "grade_etterna_13", color = pfconfig.colors.grade_4 }, -- A
    Grade14 = PfGrade{ name_key = "grade_etterna_14", color = pfconfig.colors.grade_5 }, -- B
    Grade15 = PfGrade{ name_key = "grade_etterna_15", color = pfconfig.colors.grade_6 }, -- C
    Grade16 = PfGrade{ name_key = "grade_etterna_16", color = pfconfig.colors.grade_7 }, -- D
    Grade17 = PfGrade{ name_key = "grade_17", color = pfconfig.colors.grade_7 },
    Grade18 = PfGrade{ name_key = "grade_18", color = pfconfig.colors.grade_7 },
    Grade19 = PfGrade{ name_key = "grade_19", color = pfconfig.colors.grade_7 },
    Grade20 = PfGrade{ name_key = "grade_20", color = pfconfig.colors.grade_7 },
    GradeFail = PfGrade{ name_key = "grade_fail", color = pfconfig.colors.grade_fail }, -- Fail
  }
else
  pfscore.EGrade = {
    Grade1 = PfGrade{ name_key = "grade_1", color = pfconfig.colors.grade_1 }, -- AAAA
    Grade2 = PfGrade{ name_key = "grade_2", color = pfconfig.colors.grade_2 }, -- AAA
    Grade3 = PfGrade{ name_key = "grade_3", color = pfconfig.colors.grade_3 }, -- AA
    Grade4 = PfGrade{ name_key = "grade_4", color = pfconfig.colors.grade_4 }, -- A
    Grade5 = PfGrade{ name_key = "grade_5", color = pfconfig.colors.grade_5 }, -- B
    Grade6 = PfGrade{ name_key = "grade_6", color = pfconfig.colors.grade_6 }, -- C
    Grade7 = PfGrade{ name_key = "grade_7", color = pfconfig.colors.grade_7 }, -- D
    Grade8 = PfGrade{ name_key = "grade_8", color = pfconfig.colors.grade_7 }, -- E
    Grade9 = PfGrade{ name_key = "grade_9", color = pfconfig.colors.grade_7 }, -- F
    Grade10 = PfGrade{ name_key = "grade_10", color = pfconfig.colors.grade_7 }, -- G
    Grade11 = PfGrade{ name_key = "grade_11", color = pfconfig.colors.grade_7 }, -- H
    Grade12 = PfGrade{ name_key = "grade_12", color = pfconfig.colors.grade_7 }, -- I
    Grade13 = PfGrade{ name_key = "grade_13", color = pfconfig.colors.grade_7 }, -- J
    Grade14 = PfGrade{ name_key = "grade_14", color = pfconfig.colors.grade_7 }, -- K
    Grade15 = PfGrade{ name_key = "grade_15", color = pfconfig.colors.grade_7 }, -- L
    Grade16 = PfGrade{ name_key = "grade_16", color = pfconfig.colors.grade_7 }, -- M
    Grade17 = PfGrade{ name_key = "grade_17", color = pfconfig.colors.grade_7 }, -- N
    Grade18 = PfGrade{ name_key = "grade_18", color = pfconfig.colors.grade_7 }, -- O
    Grade19 = PfGrade{ name_key = "grade_19", color = pfconfig.colors.grade_7 }, -- P
    Grade20 = PfGrade{ name_key = "grade_20", color = pfconfig.colors.grade_7 }, -- Q
    GradeFail = PfGrade{ name_key = "grade_fail", color = pfconfig.colors.grade_fail }, -- Fail
  }
end

pfscore.grade_lookup_map = {
  Grade_Tier01 = pfscore.EGrade.Grade1,
  Grade_Tier02 = pfscore.EGrade.Grade2,
  Grade_Tier03 = pfscore.EGrade.Grade3,
  Grade_Tier04 = pfscore.EGrade.Grade4,
  Grade_Tier05 = pfscore.EGrade.Grade5,
  Grade_Tier06 = pfscore.EGrade.Grade6,
  Grade_Tier07 = pfscore.EGrade.Grade7,
  Grade_Tier08 = pfscore.EGrade.Grade8,
  Grade_Tier09 = pfscore.EGrade.Grade9,
  Grade_Tier10 = pfscore.EGrade.Grade10,
  Grade_Tier11 = pfscore.EGrade.Grade11,
  Grade_Tier12 = pfscore.EGrade.Grade12,
  Grade_Tier13 = pfscore.EGrade.Grade13,
  Grade_Tier14 = pfscore.EGrade.Grade14,
  Grade_Tier15 = pfscore.EGrade.Grade15,
  Grade_Tier16 = pfscore.EGrade.Grade16,
  Grade_Tier17 = pfscore.EGrade.Grade17,
  Grade_Tier18 = pfscore.EGrade.Grade18,
  Grade_Tier19 = pfscore.EGrade.Grade19,
  Grade_Tier20 = pfscore.EGrade.Grade20,
  Grade_Failed = pfscore.EGrade.GradeFail,
}

pfscore.PfScore = class.strict {
  music_rate = class.NULL,
  sm_highscore = class.NULL,

  grade = class.property {
    get = function(self)
      if pfutil.is_etterna then
        return pfscore.grade_lookup_map[self.sm_highscore:GetWifeGrade()]
      else
        return pfscore.grade_lookup_map[self.sm_highscore:GetGrade()]
      end
    end,
    set = function(self, new) error("Cannot set a score's grade") end,
  },

  percentage_score = class.property {
    get = function(self)
      if pfutil.is_etterna then
        -- TODO: Rescore old Wife scores -> Wife3?
        -- The _fallback theme has some funcs to do with rescoring at least
        return self.sm_highscore:GetWifeScore()
      else
        return self.sm_highscore:GetPercentDP()
      end
    end,
    set = function(self, new) error("Cannot set a score's score") end,
  },

  -- Etterna only
  etterna_wife_version = class.property {
    get = function(self)
      if pfutil.is_etterna then
        return self.sm_highscore:GetWifeVers()
      else
        error("Cannot get a score's Wife version outside Etterna")
      end
    end,
    set = function(self, new) error("Cannot set a score's Wife version") end,
  },

  etterna_rating_overall = class.property {
    get = function(self)
      if pfutil.is_etterna then
        return self.sm_highscore:GetSkillsetSSR("Overall")
      else
        error("Cannot get a score's Wife version outside Etterna")
      end
    end,
    set = function(self, new) error("Cannot set a score's score-specifc-rating") end,
  },

  w1_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_W1") end,
    set = function(self, new) error("Cannot set a score's W1 count") end,
  },

  w2_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_W2") end,
    set = function(self, new) error("Cannot set a score's W2 count") end,
  },

  w3_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_W3") end,
    set = function(self, new) error("Cannot set a score's W3 count") end,
  },

  w4_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_W4") end,
    set = function(self, new) error("Cannot set a score's W4 count") end,
  },

  w5_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_W5") end,
    set = function(self, new) error("Cannot set a score's W5 count") end,
  },

  miss_count = class.property {
    get = function(self) return self.sm_highscore:GetTapNoteScore("TapNoteScore_Miss") end,
    set = function(self, new) error("Cannot set a score's miss count") end,
  },

  max_combo = class.property {
    get = function(self) return self.sm_highscore:GetMaxCombo() end,
    set = function(self, new) error("Cannot set a score's max combo") end,
  },

  time = class.property {
    get = function(self)
      return pftime.instant_from_str(self.sm_highscore:GetDate())
    end,
    set = function(self, new) error("Cannot set a score's time") end,
  },

  time_str = class.property {
    get = function(self)
      return self.sm_highscore:GetDate()
    end,
    set = function(self, new) error("Cannot set a score's timestring ") end,
  },

  __mt = {
    __eq = function(self, other)
      return self.time == other.time
    end,
  },
}

pfscore.PfScoreList = class.strict {
  scores = class.NULL,

  __init = function(self)
    self.scores = {}
  end,

  push = function(self, new)
    table.insert(self.scores, new)
  end,

  iter = function(self)
    return pfscore.PfScoreListIter(self)
  end,
}

pfscore.PfScoreListIter = class.strict(Iterator) {
  score_list = class.NULL,
  idx = 1,

  __init = function(self, score_list)
    Iterator.__init(self)

    self.score_list = score_list
  end,

  internal_next = function(self)
    local val = self.score_list.scores[self.idx]
    self.idx = self.idx + 1

    return val
  end,
}

function pfscore.get_scores_for_step(step, player)
  if pfutil.is_etterna then
    return pfscore.get_etterna_scores_for_step(step)
  else
    return pfscore.get_stepmania_scores_for_step(step, player)
  end
end

function pfscore.get_eval_score()
  if pfutil.is_etterna then
    local sm_score = SCOREMAN:GetMostRecentScore()
    if not sm_score then sm_score = SCOREMAN:GetTempReplayScore() end

    local score = pfscore.PfScore()
    score.sm_highscore = sm_score

    return score
  else
    local sm_stage_stats = SCREENMAN:GetTopScreen():GetStageStats()
    local score = pfscore.PfScore()
    score.sm_highscore = sm_stage_stats:GetPlayerStageStats(PLAYER_1):GetHighScore()

    return score
  end
end

function pfscore.get_stepmania_scores_for_step(step, player)
  local profile = PROFILEMAN:GetProfile(player)
  local sm_scores = profile:GetHighScoreListIfExists(step.song.sm_song, step.sm_step)

  if sm_scores == nil then
    -- Empty score list
    return pfscore.PfScoreList()
  end

  local scores = pfscore.PfScoreList()

  for _, sm_score in pairs(sm_scores:GetHighScores()) do
    local score = pfscore.PfScore()
    score.sm_highscore = sm_score

    scores:push(score)
  end

  return scores
end

function pfscore.get_etterna_scores_for_step(step)
  local key = step.sm_step:GetChartKey()
  local sm_scores = SCOREMAN:GetScoresByKey(key)

  if not sm_scores then
    -- Empty score list
    return pfscore.PfScoreList()
  end

  -- GetScoresByKey returns something like...
  -- {
  --   ['1.0x'] = 'ScoresAtRate (0x55bfe025d558)',
  --   ['1.2x'] = 'ScoresAtRate (0x55bfe025c8f8)'
  -- }
  -- BECAUSE WHO NEEDS NUMBERS ANYWAY WHEN WE CAN USE STRINGS!
  -- Fuck you Etterna...
  -- Also fun is that this is *completely undocumented* as far as I can tell

  local scores = pfscore.PfScoreList()

  for rate_str, sm_score_list in pairs(sm_scores) do
    -- Ugh, string parsing
    local rate = tonumber(rate_str:sub(1, #rate_str - 1))

    local sm_highscore_list = sm_score_list:GetScores() -- Returns a table of HighScores
    for _, sm_score in pairs(sm_highscore_list) do
      local score = pfscore.PfScore()
      score.music_rate = rate
      score.sm_highscore = sm_score

      scores:push(score)
    end
  end

  return scores
end

return pfscore
