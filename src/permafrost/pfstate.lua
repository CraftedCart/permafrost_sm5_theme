--- State that should persist between screen switches
-- @module permafrost.pfstate

local pfstate = {}

pfstate.music_select_search_query = ""

return pfstate
