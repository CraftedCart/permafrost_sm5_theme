local pftiming = {}

-- Timing windows are at https://github.com/stepmania/stepmania/blob/8ab7c7fab937acc684392b909b6b30b47d9a8c7b/src/Player.cpp#L105
-- Timing window scales are at https://github.com/stepmania/stepmania/blob/8ab7c7fab937acc684392b909b6b30b47d9a8c7b/src/ScreenOptionsMasterPrefs.cpp#L490
-- There's also TimingWindowAdd but we don't talk about that...

-- Timing at judge difficulty = 4
pftiming.timing_windows = {
  w1 = 0.0225, -- Flawless
  w2 = 0.045, -- Perfect
  w3 = 0.090, -- Great
  w4 = 0.135, -- Good
  w5 = 0.180, -- Bad
  mine = 0.090, -- Same as great
  hold = 0.25,
  roll = 0.5,
  attack = 0.135,
  checkpoint = 0.1664,
}

pftiming.timing_window_multipliers = {
  1.50, -- Judge difficulty 1
  1.33,
  1.16,
  1.00,
  0.84,
  0.66,
  0.50,
  0.33, -- Judge difficulty 8
  0.20, -- Justice
}

--- If judge_difficulty is nil, use the current judge difficulty
function pftiming.scale_timing_window(window, judge_difficulty)
  judge_difficulty = judge_difficulty or GetTimingDifficulty()
  return window * pftiming.timing_window_multipliers[judge_difficulty]
end

function pftiming.get_window_for_offset(offset, judge_difficulty)
  local abs_offset = math.abs(offset)

  for _, v in ipairs{"w1", "w2", "w3", "w4", "w5"} do
    if abs_offset < pftiming.scale_timing_window(pftiming.timing_windows[v], judge_difficulty) then
      return v
    end
  end

  return "miss"
end

return pftiming
