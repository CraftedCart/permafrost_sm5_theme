local pfsignal = require("permafrost.pfsignal")
local PfSignal = pfsignal.PfSignal

local pftimer = {}

pftimer.update_signal = PfSignal()
pftimer.elapsed_frames = 0
pftimer.elapsed_seconds = 0

-- Table containing actions to execute at a future tick
pftimer.delayed_actions = {}

-- Called by ScreenSystemLayer
function pftimer.update(delta_seconds)
  pftimer.elapsed_frames = pftimer.elapsed_frames + 1
  pftimer.elapsed_seconds = pftimer.elapsed_seconds + delta_seconds

  pftimer.update_signal:emit(delta_seconds)

  -- Check delayed actions
  -- Iterate backwards because we remove stuff from the table while iterating
  for i = #pftimer.delayed_actions, 1, -1 do
    local obj = pftimer.delayed_actions[i]

    if obj.time <= pftimer.elapsed_seconds then
      obj.func()
      table.remove(pftimer.delayed_actions, i)
    end
  end
end

function pftimer.run_in_seconds(seconds, func)
  local action = {
    time = pftimer.elapsed_seconds + seconds,
    func = func,
  }
  table.insert(pftimer.delayed_actions, action)

  return action
end

function pftimer.cancel_delayed_action(action)
  -- Iterate backwards because we remove stuff from the table while iterating
  for i = #pftimer.delayed_actions, 1, -1 do
    local obj = pftimer.delayed_actions[i]

    if obj == action then
      table.remove(pftimer.delayed_actions, i)
    end
  end
end

function pftimer.connect_tick(slot)
  pftimer.update_signal:connect(slot)
end

function pftimer.connect_tick_weak(slot)
  pftimer.update_signal:connect_weak(slot)
end

function pftimer.disconnect_tick(slot)
  pftimer.update_signal:disconnect(slot)
end

return pftimer
