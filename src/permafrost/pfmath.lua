local class = require("permafrost.class")

local pfmath = {}

pfmath.Vec2 = class {
  __init = function(self, x, y)
    self.x = x or 0
    self.y = y or 0
  end,

  -- Set metatable members
  __mt = {
    __eq = function(self, other) return self.x == other.x and self.y == other.y end,
    __add = function(self, other) return pfmath.Vec2(self.x + other.x, self.y + other.y) end,
    __sub = function(self, other) return pfmath.Vec2(self.x - other.x, self.y - other.y) end,
    __mul = function(self, other)
      if type(other) == "number" then
        return pfmath.Vec2(self.x * other, self.y * other)
      else
        -- Assume it's a vec
        return pfmath.Vec2(self.x * other.x, self.y * other.y)
      end
    end,
    __div = function(self, other)
      if type(other) == "number" then
        return pfmath.Vec2(self.x / other, self.y / other)
      else
        -- Assume it's a vec
        return pfmath.Vec2(self.x / other.x, self.y / other.y)
      end
    end,
    __len = function(self) return math.sqrt(self.x ^ 2 + self.y ^ 2) end,
    __unm = function(self) return pfmath.Vec2(-self.x, -self.y) end,
    __tostring = function(self) return string.format("(%f, %f)", self.x, self.y) end,
  },
}

pfmath.Aabb2 = class {
  __init = function(self, min, max)
    self.min = min or pfmath.Vec2()
    self.max = max or pfmath.Vec2()
  end,
}

pfmath.Vec3 = class {
  __init = function(self, x, y, z)
    self.x = x or 0
    self.y = y or 0
    self.z = z or 0
  end,

  to_vec2 = function(self)
    return pfmath.Vec2(self.x, self.y)
  end,

  -- Set metatable members
  __mt = {
    __eq = function(self, other) return self.x == other.x and self.y == other.y and self.z == other.z end,
    __add = function(self, other) return pfmath.Vec3(self.x + other.x, self.y + other.y, self.z + other.z) end,
    __sub = function(self, other) return pfmath.Vec3(self.x - other.x, self.y - other.y, self.z - other.z) end,
    __mul = function(self, other) return pfmath.Vec3(self.x * other.x, self.y * other.y, self.z * other.z) end,
    __div = function(self, other) return pfmath.Vec3(self.x / other.x, self.y / other.y, self.z / other.z) end,
    __len = function(self) return math.sqrt(self.x ^ 2 + self.y ^ 2 + self.z ^ 2) end,
    __unm = function(self) return pfmath.Vec3(-self.x, -self.y, -self.z) end,
    __tostring = function(self) return string.format("(%f, %f, %f)", self.x, self.y, self.z) end,
  },
}

function pfmath.round(val)
  return math.floor(val + 0.5)
end

--- Round toward zero
function pfmath.truncate(val)
  return val >= 0 and math.floor(val + 0.5) or math.ceil(val - 0.5)
end

function pfmath.lerp(a, b, alpha)
  return ((b - a) * alpha) + a
end

function pfmath.clamp(value, min, max)
  return math.min(math.max(value, min), max)
end

function pfmath.is_nan(val)
  -- IEEE 754 states that a NaN value is not equal to itself
  return val ~= val
end

function pfmath.random_from(t)
  return t[math.random(#t)]
end

function pfmath.random_weighted_from(t)
  local val = math.random()
  for _, v in ipairs(t) do
    if val < v.weight then
      return v.item
    end
  end
end

function pfmath.random_float(lower, upper)
  -- STEPMANIA WHY DO YOU BREAK MATH.RANDOM COMPAT
  -- return math.random() + math.random(lower, upper)

  return lower + math.random() * (upper - lower)
end

-- lower inclusive, upper exclusive
function pfmath.random_int(lower, upper)
  return math.floor(pfmath.random_float(lower, upper))
end

function pfmath.nearly_equal(a, b, threshold)
  threshold = threshold or 0.000001

  local diff = math.abs(a - b)
  return diff < threshold
end

function pfmath.next_pow2(value)
  return 2 ^ math.ceil(math.log(value) / math.log(2))
end

function pfmath.median(t)
  if math.fmod(#t, 2) == 0 then
    -- Return mean value of middle two elements
    return (t[#t / 2] + t[(#t / 2) + 1]) / 2
  else
    -- Return middle element
    return t[math.ceil(#t / 2)]
  end
end

function pfmath.mean(t)
  local total = 0
  for _, v in pairs(t) do
    total = total + v
  end

  return total / #t
end

return pfmath
