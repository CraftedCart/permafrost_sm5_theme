local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pflang = require("permafrost.pflang")
local pftime = require("permafrost.pftime")
local pfconfig = require("permafrost.pfconfig")
local pfutil = require("permafrost.pfutil")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfAnchored = pfwidget.PfAnchored
local PfHBox = pfwidget.PfHBox
local ESizeMode = pfwidget.ESizeMode
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local pfleaderboardwidget = {}

local STAR32_PATH = THEME:GetPathG("", "star32.png")
local ENTRY_HEIGHT = 60
local RANK_WIDTH = 40

local function make_count_text(color)
  local count_text = PfText{}
  count_text:add_hook(
    "InitCommand",
    function(actor)
      actor.slot.distribute_mode = pfwidget.EDistributeMode.EVEN
      actor.slot.height_mode = ESizeMode.KEEP
      actor.align = Vec2(0, 0)
      actor.scale = 0.4
      actor.color = color
    end
    )

  return count_text
end

pfleaderboardwidget.PfLeaderboardEntryWidget = class.strict(PfCanvas) {
  canvas = class.NULL,

  bg_rect = class.NULL,
  rank_rect = class.NULL,
  rank_text = class.NULL,
  dim_right_rect = class.NULL, -- The rect on the right, tinted with the grade color
  grade_text = class.NULL,
  percentage_text = class.NULL,
  rating_icon_rect = class.NULL,
  rating_text = class.NULL,
  name_text = class.NULL,

  counts_hbox = class.NULL,
  w1_count_text = class.NULL,
  w2_count_text = class.NULL,
  w3_count_text = class.NULL,
  w4_count_text = class.NULL,
  w5_count_text = class.NULL,
  miss_count_text = class.NULL,
  max_combo_text = class.NULL,

  relative_time_text = class.NULL,

  -- width = class.property {
    -- get = function(self) return self.actor:GetWidth() end,
    -- set = function(self, new)
      -- self.actor:SetWidth(new)

      -- self.bg_rect.width = new
      -- self.dim_right_rect.x = new
      -- self.grade_text.x = new - 4
      -- self.rating_text.x = new - 4
      -- self.percentage_text.x = new - 4
    -- end,
  -- },

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)

      self.canvas.width = new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)

      self.canvas.height = new
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.canvas = PfAnchored()
    self.canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.height = ENTRY_HEIGHT
      end
      )
    self:add_child(self.canvas)

    self.bg_rect = PfRect()
    self.bg_rect:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 0)
        actor.slot.anchors.max = Vec2(1, 0)
        actor.slot.size.y = ENTRY_HEIGHT
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.leaderboard_bg
        actor.alpha = 0.8
      end
      )
    self.canvas:add_child(self.bg_rect)

    self.rank_rect = PfRect()
    self.rank_rect:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 0)
        actor.slot.anchors.max = Vec2(0, 0)
        actor.slot.size = Vec2(RANK_WIDTH, ENTRY_HEIGHT)
        actor.align = Vec2(0, 0)
        actor.height = ENTRY_HEIGHT
        actor.color = pfconfig.colors.leaderboard_rank_bg
        -- actor.alpha = 0.8
      end
      )
    self.canvas:add_child(self.rank_rect)

    self.rank_text = PfText{}
    self.rank_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 0)
        actor.slot.anchors.max = Vec2(0, 0)
        actor.slot.offset = Vec2(RANK_WIDTH / 2, ENTRY_HEIGHT / 2)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        -- actor.position = Vec2(RANK_WIDTH / 2, ENTRY_HEIGHT / 2)
        actor.align = Vec2(0.5, 0.5)
        actor.scale = 0.5
      end
      )
    self.canvas:add_child(self.rank_text)

    self.dim_right_rect = PfRect()
    self.dim_right_rect:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(1, 0)
        actor.slot.anchors.max = Vec2(1, 0)
        actor.align = Vec2(1, 0)
        actor.slot.size = Vec2(96, ENTRY_HEIGHT)
        actor.fade_left = 1
      end
    )
    self.canvas:add_child(self.dim_right_rect)

    self.grade_text = PfText{}
    self.grade_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(1, 0)
        actor.slot.anchors.max = Vec2(1, 0)
        actor.slot.offset = Vec2(-4, 4)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        -- actor.position = Vec2(4, 4)
        actor.align = Vec2(1, 0)
        actor.scale = 0.5
      end
      )
    self.canvas:add_child(self.grade_text)

    self.percentage_text = PfText{}
    self.percentage_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(1, 0)
        actor.slot.anchors.max = Vec2(1, 0)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        actor.slot.offset = Vec2(-4, 26)
        actor.align = Vec2(1, 0)
        actor.scale = 0.4
      end
      )
    self.canvas:add_child(self.percentage_text)

    self.rating_icon_rect = PfRect()
    self.rating_icon_rect:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(1, 1)
        actor.slot.anchors.max = Vec2(1, 1)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        actor:load_texture(STAR32_PATH)
        actor.slot.offset = Vec2(-4, -3)
        actor.align = Vec2(1, 1)
        actor.scale = 0.5
      end
    )
    self.canvas:add_child(self.rating_icon_rect)

    self.rating_text = PfText{}
    self.rating_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(1, 1)
        actor.slot.anchors.max = Vec2(1, 1)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        actor.slot.offset = Vec2(-24, -4)
        actor.align = Vec2(1, 1)
        actor.scale = 0.4
      end
      )
    self.canvas:add_child(self.rating_text)

    self.name_text = PfText{}
    self.name_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 0)
        actor.slot.anchors.max = Vec2(0, 0)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        actor.slot.offset = Vec2(RANK_WIDTH + 4, 4)
        actor.align = Vec2(0, 0)
        actor.scale = 0.5
      end
      )
    self.canvas:add_child(self.name_text)

    self.counts_hbox = PfHBox()
    self.counts_hbox:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 0)
        actor.slot.anchors.max = Vec2(1, 0)
        actor.slot.offset = Vec2(RANK_WIDTH + 4, 26)
        actor.slot.size.x = -150
        actor.align = Vec2(0.5, 0.5)
        actor.spacing = Vec2(20, 0)
      end
      )
    self.canvas:add_child(self.counts_hbox)

    self.w1_count_text = make_count_text(pfconfig.colors.judge_w1)
    self.counts_hbox:add_child(self.w1_count_text)
    self.w2_count_text = make_count_text(pfconfig.colors.judge_w2)
    self.counts_hbox:add_child(self.w2_count_text)
    self.w3_count_text = make_count_text(pfconfig.colors.judge_w3)
    self.counts_hbox:add_child(self.w3_count_text)
    self.w4_count_text = make_count_text(pfconfig.colors.judge_w4)
    self.counts_hbox:add_child(self.w4_count_text)
    self.w5_count_text = make_count_text(pfconfig.colors.judge_w5)
    self.counts_hbox:add_child(self.w5_count_text)
    self.miss_count_text = make_count_text(pfconfig.colors.judge_miss)
    self.counts_hbox:add_child(self.miss_count_text)

    self.max_combo_text = make_count_text(pfconfig.palette.white)
    self.counts_hbox:add_child(self.max_combo_text)

    self.relative_time_text = PfText{}
    self.relative_time_text:add_hook(
      "InitCommand",
      function(actor)
        actor.slot.anchors.min = Vec2(0, 1)
        actor.slot.anchors.max = Vec2(0, 1)
        actor.slot.offset = Vec2(RANK_WIDTH + 4, -4)
        actor.slot.width_mode = ESizeMode.KEEP
        actor.slot.height_mode = ESizeMode.KEEP
        actor.align = Vec2(0, 1)
        actor.scale = 0.4
        actor.alpha = 0.5
      end
      )
    self.canvas:add_child(self.relative_time_text)
  end,

  set_score = function(self, score)
    if score == nil then
      self.canvas.visible = false
      return
    end

    -- Wife has changed over time in Etterna - if a score is using an old Wife score, show which version
    local wife_prefix = ""
    if pfutil.is_etterna then
      if score.etterna_wife_version < pfutil.ETTERNA_CURRENT_WIFE_VERSION then
        wife_prefix = "[Wife " .. score.etterna_wife_version .. "] "
      end
    end

    self.dim_right_rect.color = score.grade.color
    self.dim_right_rect.alpha = 0.2

    if pfutil.is_etterna then
      self.rating_text.text = string.format("%04.2f", score.etterna_rating_overall)
    end

    self.grade_text.text = pflang.get_entry(score.grade.name_key)
    self.grade_text.color = score.grade.color

    self.percentage_text.text = wife_prefix .. string.format("%04.2f%%", score.percentage_score * 100)
    self.percentage_text.color = score.grade.color

    -- self.name_text.text = "CraftedCart"
    self.name_text.text = pflang.get_entry("local_player")

    local instant = score.time
    local elapsed = instant:elapsed()

    -- self.relative_time_text.text = tostring(elapsed.seconds) .. " seconds ago"
    self.relative_time_text.text = elapsed:make_ago_str()

    self.w1_count_text.text = tostring(score.w1_count)
    self.w2_count_text.text = tostring(score.w2_count)
    self.w3_count_text.text = tostring(score.w3_count)
    self.w4_count_text.text = tostring(score.w4_count)
    self.w5_count_text.text = tostring(score.w5_count)
    self.miss_count_text.text = tostring(score.miss_count)

    self.w1_count_text.alpha = score.w1_count > 0 and 1 or 0.2
    self.w2_count_text.alpha = score.w2_count > 0 and 1 or 0.2
    self.w3_count_text.alpha = score.w3_count > 0 and 1 or 0.2
    self.w4_count_text.alpha = score.w4_count > 0 and 1 or 0.2
    self.w5_count_text.alpha = score.w5_count > 0 and 1 or 0.2
    self.miss_count_text.alpha = score.miss_count > 0 and 1 or 0.2

    self.max_combo_text.text = tostring(score.max_combo) .. "x"
  end,

  set_rank = function(self, rank)
    self.rank_text.text = rank
  end,

  -- In the evaluation screen, the obtained score should be highlighted in the local leaderboard
  set_highlight = function(self, highlight)
    self.rank_rect.color = highlight and pfconfig.colors.leaderboard_rank_bg_highlight or pfconfig.colors.leaderboard_rank_bg
  end,
}

pfleaderboardwidget.PfLeaderboardWidget = class.strict(PfCanvas) {
  entry_widgets = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)

      for _, v in pairs(self.entry_widgets) do
        v.width = new
      end
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.entry_widgets = {}
    for i = 1, 10 do
      local entry = pfleaderboardwidget.PfLeaderboardEntryWidget()
      entry:add_hook(
          "InitCommand",
          function(actor)
            actor.position = Vec2(0, (i - 1) * (ENTRY_HEIGHT + 4))
            actor.alpha = 0
          end
        )
      self:add_child(entry)
      table.insert(self.entry_widgets, entry)
    end

    -- TODO THIS IS TEMPOARY
    self:add_hook(
      "OnCommand",
      function(actor)
        local pfeval = require("permafrost.pfeval")
        local pfscore = require("permafrost.pfscore")
        local stage_stats = pfeval.get_stage_stats()
        local player_stats = stage_stats.player_stats[PLAYER_1]

        local scores = pfscore.get_scores_for_step(player_stats.played_steps[1], PLAYER_1)
        :iter()
        :filter(function(val)
          return val.music_rate == class.NULL or -- Vanilla StepMania
          pfmath.nearly_equal(val.music_rate, 1) -- Etterna
        end)
        :sort(function(a, b)
          -- Sort descending, largest score first
          return a.percentage_score > b.percentage_score
        end)

        for k, v in ipairs(self.entry_widgets) do
          local score = scores:next()

          v:set_score(score)
          v:set_rank(k)

          if player_stats.high_score == score then
            v:set_highlight(true)
          end
        end
      end
      )
  end,

  show = function(self)
    for k, v in ipairs(self.entry_widgets) do
      v.ctx.timer.run_in_seconds(k * 0.2, function()
        v.ctx.anim{
          start_val = v.alpha,
          end_val = 1,
          prop_table = v,
          prop_name = "alpha",
          duration = 0.5,
          easing = pfeasing.linear,
        }

        v.ctx.anim{
          start_val = v.x + 128,
          end_val = v.x,
          prop_table = v,
          prop_name = "x",
          duration = 0.5,
          easing = pfeasing.out_expo,
        }
      end)
    end
  end,
}

return pfleaderboardwidget
