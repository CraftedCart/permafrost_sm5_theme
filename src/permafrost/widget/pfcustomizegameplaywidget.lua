local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pfnotefieldwidget = require("permafrost.widget.pfnotefieldwidget")
local pfnotepreview= require("permafrost.widget.pfnotepreview")
local pftimer = require("permafrost.pftimer")
local pftiming = require("permafrost.pftiming")
local pfnotefieldconfig = require("permafrost.pfnotefieldconfig")
local pfinput = require("permafrost.pfinput")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfutil = require("permafrost.pfutil")
local pffs = require("permafrost.pffs")
local pfeditableui = require("permafrost.pfeditableui")
local pfuserconfig = require("permafrost.pfuserconfig")
local screen_gameplay = require("permafrost.screen.screen_gameplay")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local Aabb2 = pfmath.Aabb2
local PfCanvas = pfwidget.PfCanvas
local PfVBox = pfwidget.PfVBox
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfButton = pfwidget.PfButton
local PfToggleButton = pfwidget.PfToggleButton
local PfOptionButton = pfwidget.PfOptionButton
local PfOptionGroup = pfwidget.PfOptionGroup
local PfSpinBox = pfwidget.PfSpinBox
local PfComboBox = pfwidget.PfComboBox
local PfMesh = pfwidget.PfMesh
local PfNoteFieldWidget = pfnotefieldwidget.PfNoteFieldWidget
local PfNotePreview = pfnotepreview.PfNotePreview
local ESizeMode = pfwidget.ESizeMode
local PfNoteFieldConfig = pfnotefieldconfig.PfNoteFieldConfig
local EScrollMode = pfnotefieldconfig.EScrollMode
local PfEditableFloatProp = pfeditableui.PfEditableFloatProp
local FileEntry = pffs.FileEntry

local pfcustomizegameplaywidget = {}

local SIDEBAR_WIDTH = 400
local MISSING_BACKGROUND_PATH = THEME:GetPathG("", "bg_logo.png")

local function make_option_heading(text, scale)
  scale = scale or 0.6

  local widget = PfText{}
  widget:add_hook(
    "InitCommand",
    function(actor)
      actor.slot.width_mode = ESizeMode.KEEP
      actor.margins.top = 16
      actor.margins.bottom = 4
      actor.align = Vec2(0, 0)
      actor.scale = scale
      actor.color = pfconfig.colors.panel_text
      actor.text = text
    end
  )

  return widget
end

local function make_line(vert_table, from, to, width, from_color, to_color)
  local opp = to.x - from.x
  local adj = to.y - from.y
  local hyp = (opp * opp + adj * adj) ^ 0.5

  local opp_div_hyp = opp / hyp
  local adj_div_hyp = adj / hyp

  local x_dist = adj_div_hyp * width / 2
  local y_dist = opp_div_hyp * width / 2

  -- local half_width = width / 2

  -- Add a circle
  -- table.insert(vert_table, {{to.x - half_width, to.y - half_width, 0}, to_color, {0, 0}})
  -- table.insert(vert_table, {{to.x + half_width, to.y - half_width, 0}, to_color, {1, 0}})
  -- table.insert(vert_table, {{to.x + half_width, to.y + half_width, 0}, to_color, {1, 1}})
  -- table.insert(vert_table, {{to.x - half_width, to.y + half_width, 0}, to_color, {0, 1}})

  -- Add the line
  table.insert(vert_table, {{from.x + x_dist, from.y - y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{from.x - x_dist, from.y + y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{to.x - x_dist  , to.y + y_dist  , 0}, to_color  , {0.5, 0.5}})
  table.insert(vert_table, {{to.x + x_dist  , to.y - y_dist  , 0}, to_color  , {0.5, 0.5}})
end

local Page = class.strict(PfCanvas) {
  widgets = class.NULL,

  __init = function(self, args)
    self.widgets = {}

    for _, v in ipairs(args) do
      table.insert(self.widgets, v)
    end
  end,

  activate = function(self)
    for _, v in pairs(self.widgets) do
      v.visible = true
    end

    self.widgets[1]:focus()
  end,

  deactivate = function(self)
    for _, v in pairs(self.widgets) do
      v.visible = false
    end
  end,
}

pfcustomizegameplaywidget.PfCustomizeGameplayWidget = class.strict(PfCanvas) {
  anim_canvas = class.NULL,
  bg_widget = class.NULL,

  current_page = 1,
  pages = class.NULL,

  preview_song_bg = class.NULL,
  preview_bg_dim = class.NULL,

  highlight_mesh = class.NULL,
  highlight_aabb = class.NULL,
  highlight_target_aabb = class.NULL,

  xmod_button = class.NULL,
  cmod_button = class.NULL,
  mmod_button = class.NULL,
  amod_button = class.NULL, -- StepMania 5.3+ only
  speed_spinbox = class.NULL,

  rate_spinbox = class.NULL,
  rate_affects_pitch_toggle = class.NULL,

  noteskin_combobox = class.NULL,
  hit_sound_combobox = class.NULL,
  combo_break_sound_combobox = class.NULL,

  notefield_scale_spinbox = class.NULL,

  background_dim_spinbox = class.NULL,

  -- Notefield config
  nf_config = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      self.ctx.anim_canvas.width = new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)
      self.ctx.anim_canvas.height = new
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    local song = GAMESTATE:GetCurrentSong()
    local bg_image = song:GetBackgroundPath()
    if not bg_image then
      bg_image = MISSING_BACKGROUND_PATH
    end

    self.nf_config = PfNoteFieldConfig()
    self.nf_config:load_from_player(1)

    self.ctx.anim_canvas = PfCanvas()
    self:add_child(self.ctx.anim_canvas)

    self.bg_widget = PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.size = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.screen_background
      end
    )
    self.ctx.anim_canvas:add_child(self.bg_widget)

    local title_text = PfText{font = pfconfig.fonts.light_40}
    title_text:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(SCREEN_WIDTH / 2, 32)
        actor.color = pfconfig.colors.panel_text
        actor.text = pflang.get_entry("customize_gameplay")
      end
    )
    self.ctx.anim_canvas:add_child(title_text)

    -- Preview {{{

    local preview_canvas = PfCanvas()
    preview_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(SIDEBAR_WIDTH, ((SCREEN_HEIGHT - 50) / 2) + 50)
        actor.size = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT)
        actor.scale = (SCREEN_WIDTH - SIDEBAR_WIDTH - 20) / SCREEN_WIDTH
        actor.align = Vec2(0.5, 1) -- Somehow this centers it vertically
      end
    )
    self.ctx.anim_canvas:add_child(preview_canvas)

    -- Preview background {{{

    self.preview_song_bg = PfRect()
    self.preview_song_bg:add_hook(
      "InitCommand",
      function(actor)
        actor:load_texture_background(bg_image)
        actor:scale_to_cover(Vec2(0, 0), Vec2(SCREEN_WIDTH, SCREEN_HEIGHT))
        actor:crop_to(Vec2(SCREEN_WIDTH, SCREEN_HEIGHT))
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
      end
    )
    preview_canvas:add_child(self.preview_song_bg)

    self.preview_bg_dim = PfRect()
    self.preview_bg_dim:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.size = Vec2(SCREEN_WIDTH, SCREEN_HEIGHT)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.palette.black
        actor.alpha = 0
      end
    )
    preview_canvas:add_child(self.preview_bg_dim)

    -- }}}

    -- TODO: Check for center 1 player
    local notefield_widget = PfNoteFieldWidget(NOTESKIN:GetNoteSkinNames())
    notefield_widget:add_hook(
      "InitCommand",
      function(actor)
        local receptor_y = THEME:GetMetric("Player", "ReceptorArrowsYStandard")
        actor.position = Vec2(SCREEN_CENTER_X, -receptor_y * 1.5) -- TODO: This isn't quite right, I think..
      end
    )
    preview_canvas:add_child(notefield_widget)

    local note_preview_widget = PfNotePreview(NOTESKIN:GetNoteSkinNames())
    note_preview_widget:add_hook(
      "InitCommand",
      function(actor)
        local receptor_y = THEME:GetMetric("Player", "ReceptorArrowsYStandard")
        actor.position = Vec2(24, 24)
        actor.scale = 0.5
      end
    )
    preview_canvas:add_child(note_preview_widget)

    local game_ui = screen_gameplay.make(self.nf_config)
    preview_canvas:add_child(game_ui.root_widget)

    -- Highlight box for when a UI widget is selected
    self.highlight_aabb = Aabb2(Vec2(), Vec2(SCREEN_WIDTH, SCREEN_HEIGHT))
    self.highlight_target_aabb = Aabb2(Vec2(), Vec2(SCREEN_WIDTH, SCREEN_HEIGHT))
    self.highlight_mesh = PfMesh()
    self.highlight_mesh:add_hook(
      "InitCommand",
      function(actor)
        actor.visible = false
      end
    )
    preview_canvas:add_child(self.highlight_mesh)

    -- }}}

    -- Preview player {{{

    local preview_combo = 0
    local preview_w1_count = 0
    local preview_w2_count = 0
    local preview_w3_count = 0
    local preview_wother_count = 0

    local preview_life = 0.5
    local life_change_w1 = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeW1"))
    local life_change_w2 = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeW2"))
    local life_change_w3 = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeW3"))
    local life_change_w4 = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeW4"))
    local life_change_w5 = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeW5"))
    local life_change_miss = tonumber(THEME:GetMetric("LifeMeterBar", "LifePercentChangeMiss"))

    local last_beat = math.floor(song:GetTimingData():GetBeatFromElapsedTime(GAMESTATE:GetCurMusicSeconds()) * 2)
    local update_func = function(delta_seconds)
      local cur_beat = math.floor(song:GetTimingData():GetBeatFromElapsedTime(GAMESTATE:GetCurMusicSeconds()) * 2)
      if cur_beat ~= last_beat then
        last_beat = cur_beat

        local tns = pfmath.random_weighted_from{
          { item = "TapNoteScore_Miss", weight = 1 - 0.7201 - 0.2105 - 0.0625 - 0.0100 - 0.0031 },
          { item = "TapNoteScore_W5", weight = 1 - 0.7201 - 0.2105 - 0.0625 - 0.0100 },
          { item = "TapNoteScore_W4", weight = 1 - 0.7201 - 0.2105 - 0.0625 },
          { item = "TapNoteScore_W3", weight = 1 - 0.7201 - 0.2105 },
          { item = "TapNoteScore_W2", weight = 1 - 0.7201 },
          { item = "TapNoteScore_W1", weight = 1 },
        }

        local old_combo = preview_combo

        local offset
        if tns == "TapNoteScore_W1" then
          offset = pfmath.random_float(
            0,
            pftiming.scale_timing_window(pftiming.timing_windows.w1)
          )
          preview_combo = preview_combo + 1
          preview_w1_count = preview_w1_count + 1
          preview_life = pfmath.clamp(preview_life + life_change_w1, 0, 1) -- TODO: Multiply by life difficulty

        elseif tns == "TapNoteScore_W2" then
          offset = pfmath.random_float(
            pftiming.scale_timing_window(pftiming.timing_windows.w1),
            pftiming.scale_timing_window(pftiming.timing_windows.w2)
          )
          preview_combo = preview_combo + 1
          preview_w2_count = preview_w2_count + 1
          preview_life = pfmath.clamp(preview_life + life_change_w2, 0, 1) -- TODO: Multiply by life difficulty

        elseif tns == "TapNoteScore_W3" then
          offset = pfmath.random_float(
            pftiming.scale_timing_window(pftiming.timing_windows.w2),
            pftiming.scale_timing_window(pftiming.timing_windows.w3)
          )
          preview_w3_count = preview_w3_count + 1
          preview_combo = preview_combo + 1
          preview_life = pfmath.clamp(preview_life + life_change_w3, 0, 1) -- TODO: Multiply by life difficulty

        elseif tns == "TapNoteScore_W4" then
          offset = pfmath.random_float(
            pftiming.scale_timing_window(pftiming.timing_windows.w3),
            pftiming.scale_timing_window(pftiming.timing_windows.w4)
          )
          preview_combo = 0
          preview_wother_count = preview_wother_count + 1
          preview_life = pfmath.clamp(preview_life + life_change_w4, 0, 1) -- TODO: Multiply by life difficulty

        elseif tns == "TapNoteScore_W5" then
          offset = pfmath.random_float(
            pftiming.scale_timing_window(pftiming.timing_windows.w4),
            pftiming.scale_timing_window(pftiming.timing_windows.w5)
          )
          preview_combo = 0
          preview_wother_count = preview_wother_count + 1
          preview_life = pfmath.clamp(preview_life + life_change_w5, 0, 1) -- TODO: Multiply by life difficulty

        elseif tns == "TapNoteScore_Miss" then
          offset = pfmath.random_float(
            pftiming.scale_timing_window(pftiming.timing_windows.w5),
            pftiming.scale_timing_window(0.5)
          )
          preview_combo = 0
          preview_wother_count = preview_wother_count + 1
          preview_life = pfmath.clamp(preview_life + life_change_miss, 0, 1) -- TODO: Multiply by life difficulty
        end

        if math.random() > 0.5 then
          offset = offset * -1
        end

        game_ui.tap_judgment_signal:emit{
          tap_note_offset = offset,
          tap_note_score = tns,
        }

        game_ui.combo_changed_signal:emit{
          combo = preview_combo,
          old_combo = old_combo,
          w1_full_combo = not (preview_w2_count > 0 or preview_w3_count > 0 or preview_wother_count > 0),
          w2_full_combo = not (preview_w3_count > 0 or preview_wother_count > 0),
          w3_full_combo = not (preview_wother_count > 0),
        }

        game_ui.life_changed_signal:emit{
          life = preview_life,
        }
      end

      self:update_highlight_mesh(delta_seconds)
    end
    pftimer.connect_tick(update_func)

    self:add_hook(
      "EndCommand",
      function(self)
        pftimer.disconnect_tick(update_func)
      end
    )

    -- }}}

    self.pages = {}

    -- General options page {{{

    local options_vbox = PfVBox()
    options_vbox:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 50)
        actor.size = Vec2(SIDEBAR_WIDTH, SCREEN_HEIGHT - 50)
      end
    )
    self.ctx.anim_canvas:add_child(options_vbox)

    -- Scroll mode/speed {{{

    options_vbox:add_child(make_option_heading(pflang.get_entry("scroll_speed")))

    local speed_mod_option_group = PfOptionGroup()

    self.cmod_button = PfOptionButton()
    self.cmod_button:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.text = "C-Mod"
      end
    )
    options_vbox:add_child(self.cmod_button)
    speed_mod_option_group:add_button(self.cmod_button, EScrollMode.CMod)

    self.xmod_button = PfOptionButton()
    self.xmod_button:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.text = "X-Mod"
      end
    )
    options_vbox:add_child(self.xmod_button)
    speed_mod_option_group:add_button(self.xmod_button, EScrollMode.XMod)

    self.mmod_button = PfOptionButton()
    self.mmod_button:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.text = "M-Mod"
      end
    )
    options_vbox:add_child(self.mmod_button)
    speed_mod_option_group:add_button(self.mmod_button, EScrollMode.MMod)

    self.speed_spinbox = PfSpinBox()
    self.speed_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.scroll_speed
        actor.min_value = 1
      end
    )
    options_vbox:add_child(self.speed_spinbox)

    self:add_hook(
      "InitCommand",
      function(actor)
        speed_mod_option_group:select(self.nf_config.scroll_mode)
      end
    )

    self.speed_spinbox.value_changed_signal:connect(
      function(new)
        self.nf_config.scroll_speed = new
      end
    )

    speed_mod_option_group.selection_changed_signal:connect(
      function(new, old)
        self.nf_config.scroll_mode = new

        if
          (old == EScrollMode.CMod or old == EScrollMode.MMod) and
          new == EScrollMode.XMod
        then
          self.speed_spinbox.value = self.speed_spinbox.value / 100
        end

        if old == EScrollMode.XMod then
          self.speed_spinbox.value = self.speed_spinbox.value * 100
        end

        if new == EScrollMode.XMod then
          self.speed_spinbox.increment_size = 0.01
          self.speed_spinbox.text_format = "%.2fx"
        else
          self.speed_spinbox.increment_size = 1.00
          self.speed_spinbox.value = math.floor(self.speed_spinbox.value)

          if new == EScrollMode.CMod then
            self.speed_spinbox.text_format = "C%.0f"
          elseif new == EScrollMode.MMod then
            self.speed_spinbox.text_format = "M%.0f"
          end
        end
      end
    )

    -- }}}

    -- Music rate {{{

    options_vbox:add_child(make_option_heading(pflang.get_entry("music_rate")))

    self.rate_spinbox = PfSpinBox()
    self.rate_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.music_rate
        actor.min_value = 0.1
        actor.max_value = 3
        actor.increment_size = 0.05
        actor.text_format = "%.2fx"
      end
    )
    options_vbox:add_child(self.rate_spinbox)

    self.rate_spinbox.value_changed_signal:connect(
      function(new)
        self.nf_config.music_rate = new
      end
    )

    if pfutil.is_etterna then
      self.rate_affects_pitch_toggle = PfToggleButton()
      self.rate_affects_pitch_toggle:add_hook(
        "InitCommand",
        function(actor)
          actor.height = 32
          actor.color = pfconfig.colors.panel_text
          actor.value = self.nf_config.rate_affects_pitch
          actor.text = pflang.get_entry("rate_affects_pitch")
        end
      )
      options_vbox:add_child(self.rate_affects_pitch_toggle)

      self.rate_affects_pitch_toggle.value_changed_signal:connect(
        function(new)
          self.nf_config.rate_affects_pitch = new
        end
      )
    end

    -- }}}

    -- Note skin {{{

    options_vbox:add_child(make_option_heading(pflang.get_entry("note_skin")))

    self.noteskin_combobox = PfComboBox()
    self.noteskin_combobox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text

        local values = {}
        for _, v in pairs(NOTESKIN:GetNoteSkinNames()) do
          table.insert(values, { name = v, userdata = v })
        end
        actor.values = values

        actor.selected_value = self.nf_config.note_skin
      end
    )
    options_vbox:add_child(self.noteskin_combobox)

    self.noteskin_combobox.value_changed_signal:connect(
      function(new, index)
        self.nf_config.note_skin = new.userdata
        notefield_widget:set_noteskin(new.userdata)
        note_preview_widget:set_noteskin(new.userdata)
      end
    )

    -- }}}

    -- Notefield scale {{{

    options_vbox:add_child(make_option_heading("Notefield scale"))

    self.notefield_scale_spinbox = PfSpinBox()
    self.notefield_scale_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.scale
        actor.min_value = 0.1
        actor.max_value = 3
        actor.increment_size = 0.01
        actor.display_multiplier = 100
        actor.text_format = "%.0f%%"
      end
    )
    options_vbox:add_child(self.notefield_scale_spinbox)

    self.notefield_scale_spinbox.value_changed_signal:connect(
      function(new)
        self.nf_config.scale = new
        notefield_widget.scale = new
      end
    )

    -- }}}

    options_vbox:add_child(make_option_heading(pflang.get_entry("background_dim")))

    self.background_dim_spinbox = PfSpinBox()
    self.background_dim_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.background_dim
        actor.min_value = 0
        actor.max_value = 1
        actor.increment_size = 0.01
        actor.display_multiplier = 100
        actor.text_format = "%.0f%%"
      end
    )
    options_vbox:add_child(self.background_dim_spinbox)

    self.background_dim_spinbox.value_changed_signal:connect(
      function(new)
        self.nf_config.background_dim = new
        self.preview_bg_dim.alpha = new
      end
    )

    options_vbox:add_child(make_option_heading(pflang.get_entry("notefield_dim")))
    options_vbox:add_child(make_option_heading(pflang.get_entry("fail_mode")))
    options_vbox:add_child(make_option_heading(pflang.get_entry("judge_difficulty")))
    options_vbox:add_child(make_option_heading(pflang.get_entry("life_difficulty")))

    table.insert(self.pages, Page{options_vbox})

    -- }}}

    -- Mods page {{{

    local mods_vbox = PfVBox()
    mods_vbox:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 50)
        actor.size = Vec2(SIDEBAR_WIDTH, SCREEN_HEIGHT - 50)
        actor.visible = false
      end
    )
    self.ctx.anim_canvas:add_child(mods_vbox)

    mods_vbox:add_child(make_option_heading(pflang.get_entry("appearance_modifiers")))
    mods_vbox:add_child(make_option_heading(pflang.get_entry("perspective_modifiers")))
    mods_vbox:add_child(make_option_heading(pflang.get_entry("turn_modifiers")))
    mods_vbox:add_child(make_option_heading(pflang.get_entry("scroll_modifiers")))
    mods_vbox:add_child(make_option_heading(pflang.get_entry("mine_modifiers")))

    table.insert(self.pages, Page{mods_vbox})

    -- }}}

    -- Sounds page {{{

    local sounds_vbox = PfVBox()
    sounds_vbox:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 50)
        actor.size = Vec2(SIDEBAR_WIDTH, SCREEN_HEIGHT - 50)
        actor.visible = false
      end
    )
    self.ctx.anim_canvas:add_child(sounds_vbox)

    -- Hit sound {{{

    sounds_vbox:add_child(make_option_heading(pflang.get_entry("hit_sound")))

    self.hit_sound_combobox = PfComboBox()
    self.hit_sound_combobox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text

        local values = {
          { name = pflang.get_entry("none"), userdata = nil }
        }
        for _, v in pairs(pfutil.get_hit_sounds()) do
          table.insert(values, { name = v.basename, userdata = v })
        end
        actor.values = values

        actor.selected_value = FileEntry(self.nf_config.hit_sound_file)

        self.hit_sound_combobox.value_changed_signal:connect(
          function(new, index)
            self.nf_config.hit_sound_file = new.userdata and new.userdata.path or nil
            game_ui:set_hit_sound(new.userdata)
          end
        )
      end
    )
    sounds_vbox:add_child(self.hit_sound_combobox)

    local hit_sound_volume_spinbox = PfSpinBox()
    hit_sound_volume_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.hit_sound_volume * 100
        actor.min_value = 0
        actor.max_value = 300
        actor.increment_size = 1
        actor.text_format = pflang.get_entry("volume_spinbox_format")

        hit_sound_volume_spinbox.value_changed_signal:connect(function(value)
          self.nf_config.hit_sound_volume = value / 100
          game_ui:set_hit_sound_volume(value / 100)
        end)
      end
      )
    sounds_vbox:add_child(hit_sound_volume_spinbox)

    -- }}}

    -- Combo break sound {{{

    sounds_vbox:add_child(make_option_heading(pflang.get_entry("combo_break_sound")))

    self.combo_break_sound_combobox = PfComboBox()
    self.combo_break_sound_combobox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text

        local values = {
          { name = pflang.get_entry("none"), userdata = nil }
        }
        for _, v in pairs(pfutil.get_combo_break_sounds()) do
          table.insert(values, { name = v.basename, userdata = v })
        end
        actor.values = values

        actor.selected_value = FileEntry(self.nf_config.combo_break_sound_file)

        self.combo_break_sound_combobox.value_changed_signal:connect(
          function(new, index)
            self.nf_config.combo_break_sound_file = new.userdata and new.userdata.path or nil
            game_ui:set_combo_break_sound(new.userdata)

            -- Preview the sound
            game_ui.combo_changed_signal:emit{
              combo = 0,
              old_combo = 100,
              w1_full_combo = 100,
              w2_full_combo = 0,
              w3_full_combo = 0,
            }
          end
        )
      end
    )
    sounds_vbox:add_child(self.combo_break_sound_combobox)

    local combo_break_sound_volume_spinbox = PfSpinBox()
    combo_break_sound_volume_spinbox:add_hook(
      "InitCommand",
      function(actor)
        actor.height = 32
        actor.color = pfconfig.colors.panel_text
        actor.value = self.nf_config.combo_break_sound_volume * 100
        actor.min_value = 0
        actor.max_value = 300
        actor.increment_size = 1
        actor.text_format = pflang.get_entry("volume_spinbox_format")

        combo_break_sound_volume_spinbox.value_changed_signal:connect(function(value)
          self.nf_config.combo_break_sound_volume = value / 100
          game_ui:set_combo_break_sound_volume(value / 100)
        end)
      end
      )
    sounds_vbox:add_child(combo_break_sound_volume_spinbox)

    -- }}}

    table.insert(self.pages, Page{sounds_vbox})

    -- }}}

    -- UI edit page {{{

    local ui_edit_canvas = PfCanvas()
    ui_edit_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.visible = false
      end
    )
    self.ctx.anim_canvas:add_child(ui_edit_canvas)

    local widget_editors = {}

    local ui_edit_vbox = PfVBox()
    ui_edit_vbox:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 50)
        actor.size = Vec2(SIDEBAR_WIDTH, SCREEN_HEIGHT - 50)
      end
    )
    ui_edit_canvas:add_child(ui_edit_vbox)

    ui_edit_vbox:add_child(make_option_heading(pflang.get_entry("ui_widgets")))

    for _, widget in ipairs(game_ui.editable_widgets) do
      local widget_button

      local set_highlight_bounds = function()
        local bounds = widget.bounds_getter()
        self:set_highlight_immediate(Aabb2(bounds.min - Vec2(16, 16), bounds.max + Vec2(16, 16)))
      end

      -------- Editor --------

      local widget_editor_vbox = PfVBox()
      widget_editor_vbox:add_hook(
        "InitCommand",
        function(actor)
          actor.position = Vec2(0, 50)
          actor.size = Vec2(SIDEBAR_WIDTH, SCREEN_HEIGHT - 50)
          actor.visible = false
        end
      )
      ui_edit_canvas:add_child(widget_editor_vbox)

      -- Back
      local back_button = PfButton()
      back_button:add_hook(
        "InitCommand",
        function(actor)
          actor.height = 32
          actor.color = pfconfig.colors.panel_text
          actor.text = pflang.get_entry("back")
        end
      )
      back_button.clicked_signal:connect(function()
        widget_editor_vbox.visible = false
        ui_edit_vbox.visible = true
        ui_edit_vbox:focus()
      end)
      widget_editor_vbox:add_child(back_button)

      -- Heading
      widget_editor_vbox:add_child(make_option_heading(pflang.get_entry(widget.display_name_key), 0.8))

      -- All custom props
      for _, prop in ipairs(widget.props) do
        widget_editor_vbox:add_child(make_option_heading(pflang.get_entry(prop.display_name_key)))

        -- Float
        if prop:__instance_of(PfEditableFloatProp) then
          local prop_spinbox = PfSpinBox()
          prop_spinbox:add_hook(
            "InitCommand",
            function(actor)
              actor.height = 32
              actor.color = pfconfig.colors.panel_text
              actor.value = prop.getter(widget.widget)
              actor.increment_size = prop.increment
              actor.text_format = prop.format
              if prop.min_value ~= class.NULL then actor.min_value = prop.min_value end
              if prop.max_value ~= class.NULL then actor.max_value = prop.max_value end
            end
            )
          prop_spinbox.value_changed_signal:connect(function(value)
            prop.setter(widget.widget, value)
            set_highlight_bounds()
          end)
          widget_editor_vbox:add_child(prop_spinbox)
        else
          error("Invalid prop")
        end
      end

      -------- Button --------

      local widget_button = PfButton()
      widget_button:add_hook(
        "InitCommand",
        function(actor)
          actor.height = 32
          actor.color = pfconfig.colors.panel_text
          actor.text = pflang.get_entry(widget.display_name_key)
        end
      )
      widget_button.focus_in_signal:connect(function()
        local bounds = widget.bounds_getter()
        self:set_highlight(Aabb2(bounds.min - Vec2(16, 16), bounds.max + Vec2(16, 16)))
      end)
      widget_button.clicked_signal:connect(function()
        ui_edit_vbox.visible = false
        widget_editor_vbox.visible = true
        widget_editor_vbox:focus()
      end)

      ui_edit_vbox:add_child(widget_button)
    end

    table.insert(self.pages, Page{ui_edit_canvas, self.highlight_mesh})

    -- }}}

    self:add_hook(
      "EndCommand",
      function(actor)
        -- Save config
        self.nf_config:apply_to_player(1)
        pfuserconfig.public_config.notefield.ui_widgets = game_ui:serialize_editable_widgets()
        pfuserconfig.save_to_file()
      end
    )

  local function handle_input(event)
    if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
      if event.DeviceInput.button == "DeviceButton_]" then
        if self.current_page + 1 > #self.pages then
          self:switch_page(1)
        else
          self:switch_page(self.current_page + 1)
        end
      elseif event.DeviceInput.button == "DeviceButton_[" then
        if self.current_page - 1 < 1 then
          self:switch_page(#self.pages)
        else
          self:switch_page(self.current_page - 1)
        end
      end
    end
  end

  self:add_hook(
    "OnCommand",
    function(actor, args)
      pfinput.add_input_handler(handle_input)
    end
    )
  end,

  switch_page = function(self, page)
    self.pages[self.current_page]:deactivate()
    self.pages[page]:activate()

    self.current_page = page
  end,

  update_highlight_mesh = function(self, delta_seconds)
    local line_verts = {}

    local highlight_color = pfconfig.colors.select
    -- Top line
    make_line(line_verts,
      Vec2(self.highlight_aabb.min.x, self.highlight_aabb.min.y),
      Vec2(self.highlight_aabb.max.x, self.highlight_aabb.min.y),
      3,
      highlight_color,
      highlight_color
      )
    -- Right line
    make_line(line_verts,
      Vec2(self.highlight_aabb.max.x, self.highlight_aabb.min.y),
      Vec2(self.highlight_aabb.max.x, self.highlight_aabb.max.y),
      3,
      highlight_color,
      highlight_color
      )
    -- Bottom line
    make_line(line_verts,
      Vec2(self.highlight_aabb.min.x, self.highlight_aabb.max.y),
      Vec2(self.highlight_aabb.max.x, self.highlight_aabb.max.y),
      3,
      highlight_color,
      highlight_color
      )
    -- Left line
    make_line(line_verts,
      Vec2(self.highlight_aabb.min.x, self.highlight_aabb.min.y),
      Vec2(self.highlight_aabb.min.x, self.highlight_aabb.max.y),
      3,
      highlight_color,
      highlight_color
      )

    self.highlight_mesh.vertices = line_verts
    self.highlight_mesh.actor:SetDrawState{Mode = "DrawMode_Quads", First = 1, Num = #line_verts}

    -- Lerp towards the target
    self.highlight_aabb.min = pfmath.lerp(self.highlight_aabb.min, self.highlight_target_aabb.min, math.min(delta_seconds * 16, 1))
    self.highlight_aabb.max = pfmath.lerp(self.highlight_aabb.max, self.highlight_target_aabb.max, math.min(delta_seconds * 16, 1))
  end,

  set_highlight = function(self, aabb)
    self.highlight_target_aabb = aabb
  end,

  set_highlight_immediate = function(self, aabb)
    self.highlight_aabb = aabb
    self.highlight_target_aabb = aabb
  end,
}

return pfcustomizegameplaywidget
