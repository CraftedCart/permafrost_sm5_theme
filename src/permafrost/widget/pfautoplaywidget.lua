local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pflang = require("permafrost.pflang")
local pfconfig = require("permafrost.pfconfig")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local pfautoplaywidget = {}

pfautoplaywidget.PfAutoplayWidget = class.strict(PfCanvas) {
  anim_canvas = class.NULL,
  text_widget = class.NULL,

  prev_controller = "PlayerController_Human",

  __init = function(self)
    PfCanvas.__init(self)

    self.ctx.anim_canvas = PfCanvas()
    self.ctx.anim_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.y = 64
        actor.alpha = 0
      end
    )
    self:add_child(self.ctx.anim_canvas)

    local background = PfRect()
    background:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -24)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(SCREEN_WIDTH, 50)
        actor.color = pfconfig.colors.skip_background
      end
    )
    self.ctx.anim_canvas:add_child(background)

    self.text_widget = PfText{}
    self.text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.scale = 0.8
        actor.color = pfconfig.colors.skip_text
      end
    )
    self.ctx.anim_canvas:add_child(self.text_widget)


    local tick_func = function(delta_seconds) self:update(delta_seconds) end
    pftimer.connect_tick(tick_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.disconnect_tick(tick_func)
      end
    )
  end,

  update = function(self, delta_seconds)
    local controller = GAMESTATE:GetPlayerState(1):GetPlayerController()

    if controller ~= self.prev_controller then
      if controller == "PlayerController_Human" then
        self:hide()
      elseif controller == "PlayerController_Autoplay" then
        self.text_widget.text = pflang.get_entry("autoplay")
        self:show()
      elseif controller == "PlayerController_Cpu" then
        self.text_widget.text = pflang.get_entry("autoplay_cpu")
        self:show()
      end

      self.prev_controller = controller
    end
  end,

  show = function(self)
    self.ctx.anim{
      start_val = self.ctx.anim_canvas.alpha,
      end_val = 1,
      prop_table = self.ctx.anim_canvas,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = self.ctx.anim_canvas.y,
      end_val = 0,
      prop_table = self.ctx.anim_canvas,
      prop_name = "y",
      duration = 0.2,
      easing = pfeasing.out_expo,
    }
  end,

  hide = function(self)
    self.ctx.anim{
      start_val = self.ctx.anim_canvas.alpha,
      end_val = 0,
      prop_table = self.ctx.anim_canvas,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = self.ctx.anim_canvas.y,
      end_val = 64,
      prop_table = self.ctx.anim_canvas,
      prop_name = "y",
      duration = 0.2,
      easing = pfeasing.in_expo,
    }
  end,
}

return pfautoplaywidget
