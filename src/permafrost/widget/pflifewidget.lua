local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfutil = require("permafrost.pfutil")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local pflifewidget = {}

pflifewidget.PfLifeWidget = class.strict(PfCanvas) {
  life_bar_widget = class.NULL,
  target_life = class.NULL,

  __init = function(self)
    PfCanvas.__init(self)

    self.life_bar_widget = PfRect()
    self.life_bar_widget:add_hook(
      "OnCommand",
      function(actor)
        actor.position = Vec2(0, SCREEN_HEIGHT)
        actor.align = Vec2(1, 1)
        actor.size = Vec2(4, SCREEN_HEIGHT - pfconfig.top_bar_height)
        actor.scale_y = 0
        actor.color = pfconfig.colors.life
      end
    )
    self:add_child(self.life_bar_widget)

    self.target_life = 0

    local tick_func = function(delta_seconds) self:update(delta_seconds) end
    pftimer.connect_tick(tick_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.disconnect_tick(tick_func)
      end
    )
  end,

  on_life_changed = function(self, args)
    self.target_life = args.life
  end,

  update = function(self, delta_seconds)
    self.life_bar_widget.scale_y = pfmath.lerp(self.life_bar_widget.scale_y, self.target_life, math.min(delta_seconds / 0.1, 1))
  end,
}

return pflifewidget
