local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local NUM_BARS = 201
local TRIANGLE_PATH = THEME:GetPathG("", "triangle.png")

local pfoffsetdistribwidget = {}

pfoffsetdistribwidget.PfOffsetDistribWidget = class.strict(PfCanvas) {
  bg_widget = class.NULL,
  judge_bg_canvas = class.NULL,
  judge_backgrounds = class.NULL,
  zero_line_widget = class.NULL,
  median_line_widget = class.NULL,
  median_arrow_widget = class.NULL,
  mean_line_widget = class.NULL,
  mean_arrow_widget = class.NULL,
  early_text = class.NULL,
  late_text = class.NULL,
  bars = class.NULL,

  circle4_texture = class.NULL,
  init_seconds = class.NULL,

  width_changed_signal = class.NULL,
  height_changed_signal = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)

      self.bg_widget.width = new
      self.judge_bg_canvas.x = new / 2
      self.early_text.x = (-new / 2) + 4
      self.late_text.x = (new / 2) - 4

      self.width_changed_signal:emit(new)
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)

      self.bg_widget.height = new
      self.zero_line_widget.height = new
      self.median_line_widget.height = new
      self.mean_line_widget.height = new
      -- self.early_text.y = (new / 2) - 4
      -- self.late_text.y = (new / 2) - 4
      for _, widget in pairs(self.judge_backgrounds) do
        widget.height = new
      end

      self.height_changed_signal:emit(new)
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.width_changed_signal = PfSignal()
    self.height_changed_signal = PfSignal()

    self.judge_bg_canvas = PfCanvas()
    self.judge_bg_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.alpha = 0
      end
    )
    self:add_child(self.judge_bg_canvas)

    self.bg_widget = PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.color = pfconfig.palette.gray_dark
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.bg_widget)

    self.zero_line_widget = PfRect()
    self.zero_line_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.width = 2
        actor.color = pfconfig.colors.judge_w1
        actor.alpha = 0.5
      end
    )
    self.judge_bg_canvas:add_child(self.zero_line_widget)

    -- Make judge backgrounds
    local ms_text = pflang.get_entry("milliseconds_short")
    self.judge_backgrounds = {}
    local last_window = 0
    local max_window = pftiming.scale_timing_window(pftiming.timing_windows["w5"])
    for _, v in ipairs{
      {"w1", pfconfig.colors.judge_w1},
      {"w2", pfconfig.colors.judge_w2},
      {"w3", pfconfig.colors.judge_w3},
      {"w4", pfconfig.colors.judge_w4},
      {"w5", pfconfig.colors.judge_w5},
    } do
      local window = pftiming.scale_timing_window(pftiming.timing_windows[v[1]])

      local left = PfRect()
      left:add_hook(
        "InitCommand",
        function(actor)
          actor.align = Vec2(1, 0.5)
          actor.width = 12
          actor.color = v[2]
          actor.alpha = 0.1
          actor.fade_left = 1
        end
      )
      self.judge_bg_canvas:add_child(left)
      table.insert(self.judge_backgrounds, left)

      local left_text = PfText{}
      left_text:add_hook(
        "InitCommand",
        function(actor)
          actor.rotation_z = -90
          actor.scale = 0.35
          actor.align = Vec2(1, 0)
          actor.color = pfconfig.colors.title_text
          actor.text = string.format("-%03.0f %s", window * 1000, ms_text)
          actor.color = v[2]
          actor.alpha = 0.8
        end
      )
      self.judge_bg_canvas:add_child(left_text)

      local right = PfRect()
      right:add_hook(
        "InitCommand",
        function(actor)
          actor.align = Vec2(0, 0.5)
          actor.width = 12
          actor.color = v[2]
          actor.alpha = 0.1
          actor.fade_right = 1
        end
      )
      self.judge_bg_canvas:add_child(right)
      table.insert(self.judge_backgrounds, right)

      local right_text = PfText{}
      right_text:add_hook(
        "InitCommand",
        function(actor)
          actor.rotation_z = -90
          actor.scale = 0.35
          actor.align = Vec2(1, 1)
          actor.color = pfconfig.colors.title_text
          actor.text = string.format("+%03.0f %s", window * 1000, ms_text)
          actor.color = v[2]
          actor.alpha = 0.8
        end
      )
      self.judge_bg_canvas:add_child(right_text)

      local capture_last_window = last_window
      self.width_changed_signal:connect(function(new)
          local offset = (capture_last_window / max_window) * (new / 2)
          local text_offset = (window / max_window) * (new / 2)
          -- local height = ((window - capture_last_window) / max_window) * (new / 2)

          left.x = -offset
          left_text.x = -text_offset + 2
          right.x = offset
          right_text.x = text_offset - 2
      end)
      self.height_changed_signal:connect(function(new)
          left_text.y = -new / 2 + 2
          right_text.y = -new / 2 + 2
      end)

      last_window = window
    end

    self.early_text = PfText{}
    self.early_text:add_hook(
      "InitCommand",
      function(actor)
        actor.rotation_z = -90
        actor.scale = 0.4
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.get_entry("error_early")
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.early_text)

    self.late_text = PfText{}
    self.late_text:add_hook(
      "InitCommand",
      function(actor)
        actor.rotation_z = -90
        actor.scale = 0.4
        actor.align = Vec2(0.5, 1)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.get_entry("error_late")
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.late_text)

    -- Fetch and sort note offsets
    local player_stats = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1)
    local offsets = player_stats:GetOffsetVector()

    local note_offsets = {}
    for i = 1, #offsets do
      local offset = offsets[i] / 1000
      if offset <= max_window then -- Don't let misses beyond the max window count towards median/mean
        table.insert(note_offsets, offset)
      end
    end
    -- Sort for median calculation later
    table.sort(note_offsets)

    local window_size = max_window / (NUM_BARS / 2)

    local offset_histogram = {}
    for i = 1, NUM_BARS do offset_histogram[i] = 0 end
    for _, v in pairs(note_offsets) do
      local i = 1
      for window_end = -max_window + window_size, max_window + window_size, max_window / (NUM_BARS / 2) do

        if v < window_end then
          offset_histogram[i] = offset_histogram[i] + 1
          break
        end

        i = i + 1
      end
    end

    local histogram_max = 1
    for _, v in pairs(offset_histogram) do
      if v > histogram_max then
        histogram_max = v
      end
    end

    -- Make the bars {{{

    self.bars = {}

    local i = 1
    for window_start = -max_window, max_window, max_window / (NUM_BARS / 2) do
      if i == NUM_BARS + 1 then break end

      local window_middle = window_start + (window_size / 2)

      local i_capture = i
      local bar = PfRect()
      bar:add_hook(
        "InitCommand",
        function(actor)
          actor.align = Vec2(0.5, 1)
          actor.fade_left = 0.3
          actor.fade_right = 0.3
          actor.scale_y = 0
          -- actor.width = 2

          -- Figure out what color this bar should be
          for _, v in ipairs{
            {"w1", pfconfig.colors.judge_w1},
            {"w2", pfconfig.colors.judge_w2},
            {"w3", pfconfig.colors.judge_w3},
            {"w4", pfconfig.colors.judge_w4},
            {"w5", pfconfig.colors.judge_w5},
          } do
            local window = pftiming.scale_timing_window(pftiming.timing_windows[v[1]])
            if math.abs(window_middle) < window then
              actor.color = v[2]
              actor.actor:diffusebottomedge(Brightness(v[2], 0.5))
              break
            end
          end
        end
      )
      self.judge_bg_canvas:add_child(bar)
      table.insert(self.bars, bar)

      local offset = window_start
      self.width_changed_signal:connect(function(new)
        bar.x = ((offset + (window_size / 2)) / max_window * (new - 4)) / 2
        bar.width = new / NUM_BARS
      end)
      self.height_changed_signal:connect(function(new)
        bar.y = new / 2
        bar.height = (offset_histogram[i_capture] / histogram_max) * new
      end)

      i = i + 1
    end

    -- }}}

    -- Make median line {{{

    local median = pfmath.median(note_offsets)

    self.median_line_widget = PfRect()
    self.median_line_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.width = 2
        actor.color = pfconfig.palette.white
        actor.alpha = 0.5
      end
    )
    self.judge_bg_canvas:add_child(self.median_line_widget)

    self.median_arrow_widget = PfRect()
    self.median_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -8)
        actor.align = Vec2(0.5, 1)
        actor.color = pfconfig.palette.white
        -- actor.alpha = 0.5
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
      end
    )
    self.judge_bg_canvas:add_child(self.median_arrow_widget)

    local median_text = PfText{}
    median_text:add_hook(
      "InitCommand",
      function(actor)
        actor.scale = 0.35
        actor.align = Vec2(0.5, 1)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.format_entry{"median_offset", offset = median * 1000}
        actor.color = pfconfig.palette.white
        actor.alpha = 1
      end
    )
    self.judge_bg_canvas:add_child(median_text)

    self.width_changed_signal:connect(function(new)
      local x = (median / max_window) * (new / 2)

      self.median_line_widget.x = x
      self.median_arrow_widget.x = x
      median_text.x = x
    end)

    self.height_changed_signal:connect(function(new)
      self.median_arrow_widget.y = -(new / 2) + 12
      median_text.y = -(new / 2) - 8
    end)

    -- }}}

    -- Make mean line {{{

    local mean = pfmath.mean(note_offsets)

    self.mean_line_widget = PfRect()
    self.mean_line_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0.5)
        actor.width = 2
        actor.color = pfconfig.palette.white
        actor.alpha = 0.5
      end
    )
    self.judge_bg_canvas:add_child(self.mean_line_widget)

    self.mean_arrow_widget = PfRect()
    self.mean_arrow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.rotation_z = 180
        actor.position = Vec2(0, -8)
        actor.align = Vec2(0.5, 1)
        actor.color = pfconfig.palette.white
        -- actor.alpha = 0.5
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
      end
    )
    self.judge_bg_canvas:add_child(self.mean_arrow_widget)

    local mean_text = PfText{}
    mean_text:add_hook(
      "InitCommand",
      function(actor)
        actor.scale = 0.35
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.format_entry{"mean_offset", offset = mean * 1000}
        actor.color = pfconfig.palette.white
        actor.alpha = 1
      end
    )
    self.judge_bg_canvas:add_child(mean_text)

    self.width_changed_signal:connect(function(new)
      local x = (mean / max_window) * (new / 2)

      self.mean_line_widget.x = x
      self.mean_arrow_widget.x = x
      mean_text.x = x
    end)

    self.height_changed_signal:connect(function(new)
      self.mean_arrow_widget.y = (new / 2) - 12
      mean_text.y = (new / 2) + 8
    end)

    -- }}}
  end,

  show = function(self)
    self.init_seconds = pftimer.elapsed_seconds

    self.ctx.anim{
      start_val = 0,
      end_val = 1,
      prop_table = self.judge_bg_canvas,
      prop_name = "alpha",
      duration = 2,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = 0,
      end_val = 1,
      prop_table = self.judge_bg_canvas,
      prop_name = "scale_y",
      duration = 2,
      easing = pfeasing.out_expo,
    }

    for i = 0, #self.bars / 2 do
      local bar_left = self.bars[math.floor(#self.bars / 2) - i]
      if bar_left == nil then break end
      local bar_right = self.bars[math.ceil(#self.bars / 2) + i]
      if bar_right == nil then break end

      self.ctx.timer.run_in_seconds(
        (i / #self.bars) * 4,
        function()
          self.ctx.anim{
            start_val = 2,
            end_val = 1,
            prop_table = bar_left,
            prop_name = "scale_y",
            duration = 2,
            easing = pfeasing.out_expo,
          }

          self.ctx.anim{
            start_val = 0,
            end_val = 1,
            prop_table = bar_left,
            prop_name = "alpha",
            duration = 2,
            easing = pfeasing.out_expo,
          }

          self.ctx.anim{
            start_val = 2,
            end_val = 1,
            prop_table = bar_right,
            prop_name = "scale_y",
            duration = 2,
            easing = pfeasing.out_expo,
          }

          self.ctx.anim{
            start_val = 0,
            end_val = 1,
            prop_table = bar_right,
            prop_name = "alpha",
            duration = 2,
            easing = pfeasing.out_expo,
          }
        end
      )
    end
  end,
}

return pfoffsetdistribwidget
