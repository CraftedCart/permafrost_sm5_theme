local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pfeasing = require("permafrost.pfeasing")
local pfconfig = require("permafrost.pfconfig")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfText = pfwidget.PfText

local pfcombowidget = {}

pfcombowidget.PfComboWidget = class.strict(PfCanvas) {
  combo_text_widget = class.NULL,
  prev_combo = 0,

  __init = function(self)
    PfCanvas.__init(self)

    self.combo_text_widget = PfText{}
    self.combo_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0)
        actor.scale = 0.7
        actor.color = pfconfig.colors.combo_any
      end
    )
    self:add_child(self.combo_text_widget)

    self:add_hook(
      "EndCommand",
      function(actor)
        self.ctx.anim.stop_animating_table(self.combo_text_widget)
      end
    )
  end,

  on_combo_changed = function(self, args)
    local combo = args.combo

    -- We don't set the combo text text outside the if statement as if we do,
    -- it says 0 before even hitting any notes

    if combo > 0 then
      self.combo_text_widget.text = tostring(combo)
      self.ctx.anim.stop_animating_table(self.combo_text_widget)
      self.combo_text_widget.y = 0
      self.combo_text_widget.alpha = 1

      if args.w1_full_combo then -- Flawless
        self.combo_text_widget.color = pfconfig.colors.combo_w1
      elseif args.w2_full_combo then -- Perfect
        self.combo_text_widget.color = pfconfig.colors.combo_w2
      elseif args.w3_full_combo then -- Great
        self.combo_text_widget.color = pfconfig.colors.combo_w3
      else -- Dropped combo at some point
        self.combo_text_widget.color = pfconfig.colors.combo_any
      end
    elseif self.prev_combo > 0 then
      self.combo_text_widget.text = "0"
      self.combo_text_widget.color = pfconfig.colors.combo_any

      self.ctx.anim{
        start_val = 0,
        end_val = 32,
        prop_table = self.combo_text_widget,
        prop_name = "y",
        duration = 0.2,
        easing = pfeasing.in_expo,
      }

      self.ctx.anim{
        start_val = 1,
        end_val = 0,
        prop_table = self.combo_text_widget,
        prop_name = "alpha",
        duration = 0.2,
        easing = pfeasing.linear,
      }
    end

    self.prev_combo = combo
  end,
}

return pfcombowidget
