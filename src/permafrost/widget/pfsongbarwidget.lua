local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pfconfig = require("permafrost.pfconfig")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local pfsongbarwidget = {}

pfsongbarwidget.PfSongBarWidget = class.strict(PfCanvas) {
  bar_bg_widget = class.NULL,
  bar_fg_widget = class.NULL,
  song_name_text_widget = class.NULL,
  song_time_text_widget = class.NULL,

  on_tick = class.NULL,

  __init = function(self)
    PfCanvas.__init(self)

    self.bar_bg_widget = PfRect()
    self.bar_bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(1024, 24)
        actor.color = pfconfig.colors.song_bar_bg
      end
    )
    self:add_child(self.bar_bg_widget)

    self.bar_fg_widget = PfRect()
    self.bar_fg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(-512, 0)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(1024, 24)
        actor.color = pfconfig.colors.song_bar_fg

        local song = GAMESTATE:GetCurrentSong()
        local song_len = song:GetLastSecond()

        self.on_tick = function()
          actor.scale_x = math.min(1, math.max(0, GAMESTATE:GetSongPosition():GetMusicSeconds()) / song_len)
        end

        actor.ctx.timer.connect_tick(self.on_tick)
      end
    )
    self:add_child(self.bar_fg_widget)

    self.song_name_text_widget = PfText{}
    self.song_name_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(-512 + 4, 4)
        actor.align = Vec2(0, 0)
        actor.scale = 0.5
        actor.color = pfconfig.colors.song_bar_title

        local song = GAMESTATE:GetCurrentSong()
        actor.text = song:GetDisplayMainTitle()
      end
    )
    self:add_child(self.song_name_text_widget)

    self.song_time_text_widget = PfText{}
    self.song_time_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(512 - 4, 4)
        actor.align = Vec2(1, 0)
        actor.scale = 0.5
        actor.color = pfconfig.colors.song_bar_title

        local song = GAMESTATE:GetCurrentSong()
        local song_len = song:GetLastSecond()
        local song_len_mins = math.floor(song_len / 60)
        local song_len_secs = song_len - (song_len_mins * 60)

        actor.text = string.format("%d:%02d", song_len_mins, song_len_secs)
      end
    )
    self:add_child(self.song_time_text_widget)
  end,
}

return pfsongbarwidget
