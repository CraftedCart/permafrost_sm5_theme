local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local class = require("permafrost.class")

local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect

local pfjudgmentwidget = {}

local frame_lut = {
  TapNoteScore_W1 = 0,
  TapNoteScore_W2 = 1,
  TapNoteScore_W3 = 2,
  TapNoteScore_W4 = 3,
  TapNoteScore_W5 = 4,
  TapNoteScore_Miss = 5
}

pfjudgmentwidget.PfJudgmentWidget = class.strict(PfCanvas) {
  judgment_widget = class.NULL,
  hide_delayed_action = class.NULL,
  texture = class.NULL,

  __init = function(self)
    PfCanvas.__init(self)

    self.judgment_widget = PfRect()
    self.judgment_widget:add_hook(
      "InitCommand",
      function(actor)
        actor:load_texture("/Assets/Judgments/Judgment Normal 2x6 (Doubleres).png")
        -- actor:load_texture(THEME:GetPathG("", "Judgment Normal"))
        actor:tween_pause() -- Don't animate between the sprite frames
        actor.alpha = 0
      end
    )
    self:add_child(self.judgment_widget)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.cancel_delayed_action(self.hide_delayed_action)
      end
    )
  end,

  on_tap_judgment = function(self, args)
    local frame = frame_lut[args.tap_note_score]

    if self.judgment_widget.texture_state_num == 12 then
      frame = frame * 2

      if args.tap_note_offset > 0 then
        frame = frame + 1
      end
    end

    pftimer.cancel_delayed_action(self.hide_delayed_action)
    self.ctx.anim.stop_animating_table(self.judgment_widget)

    self.judgment_widget.texture_state = frame
    self.judgment_widget.alpha = 1

    self.hide_delayed_action = self.ctx.timer.run_in_seconds(
      2,
      function()
        self.ctx.anim{
          start_val = 1,
          end_val = 0,
          prop_table = self.judgment_widget,
          prop_name = "alpha",
          duration = 0.2,
          easing = pfeasing.linear,
        }
      end
    )
  end,
}

return pfjudgmentwidget
