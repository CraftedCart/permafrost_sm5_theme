local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfMesh = pfwidget.PfMesh
local PfText = pfwidget.PfText

local CIRCLE4_PATH = THEME:GetPathG("", "circle4.png")

-- TODO: Consider only partially updating the vertex buffers so we can use a higher number of points here
local NPS_GRAPH_POINTS = 512

local function make_line(vert_table, from, to, width, from_color, to_color)
  local opp = to.x - from.x
  local adj = to.y - from.y
  local hyp = (opp * opp + adj * adj) ^ 0.5

  local opp_div_hyp = opp / hyp
  local adj_div_hyp = adj / hyp

  local x_dist = adj_div_hyp * width / 2
  local y_dist = opp_div_hyp * width / 2

  local half_width = width / 2

  -- Add a circle
  table.insert(vert_table, {{to.x - half_width, to.y - half_width, 0}, to_color, {0, 0}})
  table.insert(vert_table, {{to.x + half_width, to.y - half_width, 0}, to_color, {1, 0}})
  table.insert(vert_table, {{to.x + half_width, to.y + half_width, 0}, to_color, {1, 1}})
  table.insert(vert_table, {{to.x - half_width, to.y + half_width, 0}, to_color, {0, 1}})

  -- Add the line
  table.insert(vert_table, {{from.x + x_dist, from.y - y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{from.x - x_dist, from.y + y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{to.x - x_dist  , to.y + y_dist  , 0}, to_color  , {0.5, 0.5}})
  table.insert(vert_table, {{to.x + x_dist  , to.y - y_dist  , 0}, to_color  , {0.5, 0.5}})
end

local pfsongnpswidget = {}

local PfRecentNote = class.strict {
  time = class.NULL,
  chord_size = class.NULL,

  __init = function(self, time, chord_size)
    self.time = time
    self.chord_size = chord_size
  end,
}

pfsongnpswidget.PfSongNpsWidget = class.strict(PfCanvas) {
  bg_widget = class.NULL,
  line_mesh = class.NULL,
  nps_text_widget = class.NULL,

  circle4_texture = class.NULL,

  -- The NPS value for each X point in the graph
  graph_data = class.NULL,

  -- Used for calculating NPS from the last few notes
  -- A list of PfRecentNote values
  recent_notes = class.NULL,

  max_scale = 1,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)

      self.bg_widget.width = new
      -- self:remake_plot_mesh()
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)

      self.bg_widget.height = new
      self.line_mesh.y = new / 2
      -- self:remake_plot_mesh()
    end,
  },

  -- TODO: Sum chord sizes here instead of just doing #self.recent_notes
  _current_nps = class.property {
    get = function(self) return #self.recent_notes end,
    set = function(self, new) error("Can't set _current_nps") end,
  },
  _current_smoothed_nps = 0,

  _prev_graph_point = 1,

  -- Returns the index into self.graph_data that corresponds to the current song position
  -- May return <1 if the song hasn't started yet
  _current_graph_point = class.property {
    get = function(self)
      local song = GAMESTATE:GetCurrentSong()
      local song_len = song:GetLastSecond()
      local song_percent = GAMESTATE:GetSongPosition():GetMusicSeconds() / song_len

      return math.min(NPS_GRAPH_POINTS, math.max(0, math.floor(song_percent * NPS_GRAPH_POINTS)))
    end,
    set = function(self, new) error("Can't set _current_graph_point") end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.bg_widget = PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0.5)
        actor.color = pfconfig.palette.gray_dark
        actor.alpha = 0.8
      end
    )
    self:add_child(self.bg_widget)

    -- STEPMANIA WHYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
    -- (There doesn't seem to be any good way to load a RageTexture)
    local useless_texture_loader = PfRect()
    useless_texture_loader:add_hook(
      "InitCommand",
      function(actor)
        actor.visible = false
        actor:load_texture(CIRCLE4_PATH)
        self.circle4_texture = actor.texture
      end
    )
    self:add_child(useless_texture_loader)

    self.line_mesh = PfMesh()
    self:add_child(self.line_mesh)

    self.nps_text_widget = PfText{}
    self.nps_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(1024 - 4, 0)
        actor.align = Vec2(1, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.song_bar_title
      end
    )
    self:add_child(self.nps_text_widget)

    -- Init graph data to all zeroes
    self.graph_data = {}
    for i = 1, NPS_GRAPH_POINTS do
      table.insert(self.graph_data, 0)
    end

    -- Track notes on judgment
    self:add_hook(
      "JudgmentMessageCommand",
      function(actor, args)
        if args.TapNoteOffset then -- If this is a tap...
          -- The notes parameter contains a table where the table indices correspond to the columns in game. The items
          -- in the table either contains a TapNote object (if there is a note) or be simply nil (if there are no notes)

          -- Since we only want to count the number of notes in a chord, we just iterate over the table and count the
          -- ones that aren't nil. Set chordsize to 1 if notes are counted separately.
          local chord_size = 0
          if GAMESTATE:GetCurrentGame():CountNotesSeparately() then
            chord_size = 1
          else
            for i = 1, GAMESTATE:GetCurrentStyle():ColumnsPerPlayer() do
              if args.notes ~= nil and args.notes[i] ~= nil then
                chord_size = chord_size + 1
              end
            end
          end

          table.insert(self.recent_notes, PfRecentNote(GetTimeSinceStart(), chord_size))
        end
      end
    )

    self.ctx.timer.connect_tick(function(delta_seconds)
      local now = GetTimeSinceStart()

      -- Interpolate current_smoothed_nps to current_nps over 0.1s
      local current_nps = self._current_nps
      self._current_smoothed_nps = pfmath.lerp(
        self._current_smoothed_nps,
        current_nps,
        math.min(1, delta_seconds / 0.1)
      )

      local current_graph_point = self._current_graph_point
      if current_graph_point >= 1 then
        -- Update the scale if we have a new peak NPS
        if self._current_smoothed_nps > self.max_scale then
          self.max_scale = self._current_smoothed_nps
          want_update_graph = true
        end

        local want_update_graph = false

      -- Update the range from prev-current NPS points on the graph
        local prev_nps = self.graph_data[self._prev_graph_point]
        local range_size = current_graph_point - self._prev_graph_point
        for i = self._prev_graph_point, current_graph_point do
          local range_percent = (i - self._prev_graph_point) / range_size
          if pfmath.is_nan(range_percent) then
            range_percent = 0
          end

          if self.graph_data[i] < self._current_smoothed_nps then
            self.graph_data[i] = pfmath.lerp(prev_nps, self._current_smoothed_nps, range_percent)
            want_update_graph = true
          end
        end

        -- If we're at 0 NPS, we want to update the graph anyway as it progresses through the song
        if current_graph_point > self._prev_graph_point then
          want_update_graph = true
        end

        if want_update_graph then
          self:remake_plot_mesh()
        end

        self._prev_graph_point = current_graph_point
      end

      -- Remove notes older than a second each frame
      -- Iterate backwards because we remove stuff from the table while iterating
      for i = #self.recent_notes, 1, -1 do
        local note = self.recent_notes[i]
        if note.time + 1 < now then
          table.remove(self.recent_notes, i)
        end
      end

      -- Update the nps text
      self.nps_text_widget.text = pflang.format_entry{"nps_value", nps = current_nps}
    end)

    self:add_hook(
      "OnCommand",
      function(actor)
        actor:remake_plot_mesh()
      end
    )
  end,

  remake_plot_mesh = function(self)
    -- Loop through all graph points
    local prev_x = 0
    local prev_y = 0

    local dot_verts = {}
    local line_verts = {}

    local max_x_point = self._current_graph_point
    if max_x_point < 1 then return end

    for i = 1, max_x_point do
      local nps = self.graph_data[i]

      local x = (i / NPS_GRAPH_POINTS) * self.width
      local y = (nps * -self.height) / self.max_scale

      -- Add a line
      local line_from = Vec2(prev_x, prev_y)
      local line_to = Vec2(x, y)
      local line_from_color = pfconfig.colors.progress_bar_fill_accent
      local line_to_color = pfconfig.colors.progress_bar_fill_accent

      make_line(line_verts, line_from, line_to, 2, line_from_color, line_to_color)

      local trans_from = Alpha(line_from_color, 0.4)
      local trans_to = Alpha(line_to_color, 0.4)
      table.insert(line_verts, {{line_from.x, line_from.y, 0}, trans_from, {0.5, 0.5}})
      table.insert(line_verts, {{line_to.x  , line_to.y  , 0}, trans_to  , {0.5, 0.5}})
      table.insert(line_verts, {{line_to.x  , 0          , 0}, trans_to  , {0.5, 0.5}})
      table.insert(line_verts, {{line_from.x, 0          , 0}, trans_from, {0.5, 0.5}})

      prev_x = x
      prev_y = y
    end

    self.line_mesh.vertices = line_verts
    self.line_mesh.actor:SetDrawState{Mode = "DrawMode_Quads", First = 1, Num = #line_verts}
  end,
}

return pfsongnpswidget
