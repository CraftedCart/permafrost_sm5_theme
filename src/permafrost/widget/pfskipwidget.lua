local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfconfig = require("permafrost.pfconfig")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

-- Ok so StepMania doesn't let me set the music position
-- And speeding up to 3x speed and slowing back down when we're about to start makes the music go all warbly and screwey
-- >.<
-- So yeah, this is more of a countdown than a skip widget

local pfskipwidget = {}

pfskipwidget.PfSkipWidget = class.strict(PfCanvas) {
  skip_text_widget = class.NULL,
  -- accelerating_text_widget = class.NULL,
  time_text_widget = class.NULL,
  bar_widget = class.NULL,

  initial_delta = class.NULL,
  -- initial_rate = class.NULL,
  has_hidden = class.NULL,

  __init = function(self)
    PfCanvas.__init(self)

    local background = PfRect()
    background:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -180 - 24)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(SCREEN_WIDTH, 40 + 24 + 24 + 4)
        actor.color = pfconfig.colors.skip_background
      end
    )
    self:add_child(background)

    -- self.skip_text_widget = PfText{font = pfconfig.fonts.light_40}
    -- self.skip_text_widget:add_hook(
    --   "InitCommand",
    --   function(actor)
    --     actor.position = Vec2(0, -256)
    --     actor.align = Vec2(0.5, 0)
    --     actor.scale = 1
    --     actor.color = pfconfig.colors.text_input_text
    --     actor.text = "Skip" -- TODO: Use lang if this ever gets revived
    --   end
    -- )
    -- self:add_child(self.skip_text_widget)

    -- self.accelerating_text_widget = PfText{font = pfconfig.fonts.light_40}
    -- self.accelerating_text_widget:add_hook(
    --   "InitCommand",
    --   function(actor)
    --     actor.position = Vec2(-64, -256)
    --     actor.align = Vec2(0.5, 0)
    --     actor.scale = 1
    --     actor.color = pfconfig.colors.skip_text
    --     actor.text = ">>"
    --     actor.alpha = 0
    --   end
    -- )
    -- self:add_child(self.accelerating_text_widget)

    self.time_text_widget = PfText{}
    self.time_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -180)
        actor.align = Vec2(0.5, 0)
        actor.scale = 0.8
        actor.color = pfconfig.colors.skip_text
      end
    )
    self:add_child(self.time_text_widget)

    self.bar_widget = PfRect()
    self.bar_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -140)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(SCREEN_WIDTH, 4)
        actor.color = pfconfig.colors.skip_bar
      end
    )
    self:add_child(self.bar_widget)

    local tick_func = function(delta_seconds) self:update(delta_seconds) end
    pftimer.connect_tick(tick_func)

    self:add_hook(
      "EndCommand",
      function(actor)
        pftimer.disconnect_tick(tick_func)
      end
    )

    self:add_hook(
      "OnCommand",
      function(actor)
        self.alpha = 1
        self.has_hidden = false
        self.initial_delta = class.NULL
      end
    )

    -- pfinput.add_input_handler(function(event) self:handle_input(event) end)

    self.has_hidden = false
  end,

  -- handle_input = function(self, event)
  --   if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
  --     if event.DeviceInput.button == "DeviceButton_space" then
  --       local opts = GAMESTATE:GetSongOptionsObject("ModsLevel_Song")
  --       opts:MusicRate(3, 10000)

  --       self.accelerating_text_widget.actor:diffuseshift()
  --       self.accelerating_text_widget.actor:effectcolor1(pfconfig.colors.text_input_text)
  --       self.accelerating_text_widget.actor:effectcolor2(Alpha(pfconfig.colors.text_input_text, 0.5))

  --       self.ctx.anim{
  --         start_val = 1,
  --         end_val = 0,
  --         prop_table = self.skip_text_widget,
  --         prop_name = "alpha",
  --         duration = 0.2,
  --         easing = pfeasing.linear,
  --       }

  --       self.ctx.anim{
  --         start_val = 0,
  --         end_val = 64,
  --         prop_table = self.skip_text_widget,
  --         prop_name = "x",
  --         duration = 0.2,
  --         easing = pfeasing.in_expo,
  --       }

  --       self.ctx.anim{
  --         start_val = 0,
  --         end_val = 1,
  --         prop_table = self.accelerating_text_widget,
  --         prop_name = "alpha",
  --         duration = 0.2,
  --         easing = pfeasing.linear,
  --       }

  --       self.ctx.anim{
  --         start_val = -64,
  --         end_val = 0,
  --         prop_table = self.accelerating_text_widget,
  --         prop_name = "x",
  --         duration = 0.2,
  --         easing = pfeasing.out_expo,
  --       }
  --     end
  --   end
  -- end,

  update = function(self, delta_seconds)
    local delta = self:get_time_to_start()

    if delta < 2 and not self.has_hidden then
      self:hide()
    end

    if self.initial_delta == class.NULL then
      -- local opts = GAMESTATE:GetSongOptionsObject("ModsLevel_Song")

      self.initial_delta = delta
      -- self.initial_rate = opts:MusicRate()
    end

    self.time_text_widget.text = tostring(math.ceil(math.max(0, delta)))
    self.bar_widget.scale_x = math.max(0, delta / self.initial_delta)
  end,

  hide = function(self)
    self.has_hidden = true

    -- local opts = GAMESTATE:GetSongOptionsObject("ModsLevel_Song")
    -- opts:MusicRate(self.initial_rate, 10)

    self.ctx.anim{
      start_val = 1,
      end_val = 0,
      prop_table = self,
      prop_name = "alpha",
      duration = 0.2,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = self.y,
      end_val = self.y + 64,
      prop_table = self,
      prop_name = "y",
      duration = 0.2,
      easing = pfeasing.in_expo,
    }
  end,

  get_time_to_start = function(self)
    local song = GAMESTATE:GetCurrentSong()
    local start = song:GetFirstSecond()
    local now = GAMESTATE:GetCurMusicSeconds()

    return start - now
  end,
}

return pfskipwidget
