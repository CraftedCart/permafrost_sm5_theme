local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pffs = require("permafrost.pffs")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local FileEntry = pffs.FileEntry
local DirListing = pffs.DirListing

local pffilepicker = {}

local Y_PADDING = 64
local Y_PADDING_2 = Y_PADDING * 2
local HEIGHT = SCREEN_HEIGHT - Y_PADDING_2

pffilepicker.PfFilePicker = class.strict(PfCanvas) {
  path = class.NULL,
  path_text_widget = class.NULL,
  dir_listing = class.NULL,

  dir_content_text_widget = class.NULL,

  __init = function(self)
    PfCanvas.__init(self)

    -- self.path = FileEntry("/")
    self.path = FileEntry("/Themes/permafrost/src/permafrost/")
    self.dir_listing = DirListing()

    -- Background and shadows {{{

    local background = PfRect()
    background:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, Y_PADDING)
        actor.align = Vec2(0, 0)
        actor.size = Vec2(SCREEN_WIDTH, HEIGHT)
        actor.color = pfconfig.colors.screen_background
      end
    )
    self:add_child(background)

    local top_shadow_widget = pfwidget.PfRect()
    top_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, Y_PADDING)
        actor.align = Vec2(0, 1)
        actor.color = pfconfig.colors.shadow
        actor.fade_top = 1
        actor.size = Vec2(SCREEN_WIDTH, 8)
      end
    )
    self:add_child(top_shadow_widget)

    local bottom_shadow_widget = pfwidget.PfRect()
    bottom_shadow_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, SCREEN_HEIGHT - Y_PADDING)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_bottom = 1
        actor.size = Vec2(SCREEN_WIDTH, 8)
      end
    )
    self:add_child(bottom_shadow_widget)

    -- }}}

    -- Title {{{

    local title_title_bg = PfRect()
    title_title_bg:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, Y_PADDING)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.palette.purple -- TODO: Not use preset colors
        actor.size = Vec2(SCREEN_WIDTH, 64)
      end
    )
    self:add_child(title_title_bg)

    local title_title_shadow_bottom = PfRect()
    title_title_shadow_bottom:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, Y_PADDING + 64)
        actor.align = Vec2(0, 0)
        actor.color = pfconfig.colors.shadow
        actor.fade_bottom = 1
        actor.size = Vec2(SCREEN_WIDTH, 8)
      end
    )
    self:add_child(title_title_shadow_bottom)

    local title_text = PfText{font = pfconfig.fonts.light_40}
    title_text:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(SCREEN_CENTER_X, Y_PADDING + 16)
        actor.align = Vec2(0.5, 0)
        actor.color = color("1, 1, 1, 1")
        actor.scale = 1
        actor.text = pflang.get_entry("file_browser")
      end
    )
    self:add_child(title_text)

    -- }}}

    self.path_text_widget = PfText{}
    self.path_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, SCREEN_HEIGHT - Y_PADDING_2)
        actor.align = Vec2(0, 0)
        actor.scale = 0.8
        actor.color = pfconfig.palette.white
        actor.text = self.path.path
      end
    )
    self:add_child(self.path_text_widget)

    self.dir_content_text_widget = PfText{}
    self.dir_content_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(24, Y_PADDING_2 + 64 - 24)
        actor.align = Vec2(0, 0)
        actor.scale = 0.5
        actor.color = pfconfig.palette.white
      end
    )
    self:add_child(self.dir_content_text_widget)

    self:add_hook(
      "InitCommand",
      function(actor)
        self:update_listing()
      end
      )
  end,

  update_listing = function(self)
    self.dir_listing = self.path:list_dir()

    self.dir_content_text_widget.text = table.concat(
      self.dir_listing:iter():map(
        function(entry)
          if entry.is_dir then
            return entry.basename .. "/"
          else
            return entry.basename
          end
        end
        ):collect(),
      "\n"
      )
  end,

  -- handle_input = function(self, event)
  --   if event.type == "InputEventType_FirstPress" or event.type == "InputEventType_Repeat" then
  --     if event.DeviceInput.button == "DeviceButton_space" then
  --       local opts = GAMESTATE:GetSongOptionsObject("ModsLevel_Song")
  --       opts:MusicRate(3, 10000)

  --       self.accelerating_text_widget.actor:diffuseshift()
  --       self.accelerating_text_widget.actor:effectcolor1(pfconfig.colors.text_input_text)
  --       self.accelerating_text_widget.actor:effectcolor2(Alpha(pfconfig.colors.text_input_text, 0.5))

  --       self.ctx.anim{
  --         start_val = 1,
  --         end_val = 0,
  --         prop_table = self.skip_text_widget,
  --         prop_name = "alpha",
  --         duration = 0.2,
  --         easing = pfeasing.linear,
  --       }

  --       self.ctx.anim{
  --         start_val = 0,
  --         end_val = 64,
  --         prop_table = self.skip_text_widget,
  --         prop_name = "x",
  --         duration = 0.2,
  --         easing = pfeasing.in_expo,
  --       }

  --       self.ctx.anim{
  --         start_val = 0,
  --         end_val = 1,
  --         prop_table = self.accelerating_text_widget,
  --         prop_name = "alpha",
  --         duration = 0.2,
  --         easing = pfeasing.linear,
  --       }

  --       self.ctx.anim{
  --         start_val = -64,
  --         end_val = 0,
  --         prop_table = self.accelerating_text_widget,
  --         prop_name = "x",
  --         duration = 0.2,
  --         easing = pfeasing.out_expo,
  --       }
  --     end
  --   end
  -- end,
}

return pffilepicker

