local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfMesh = pfwidget.PfMesh
local PfRenderTarget = pfwidget.PfRenderTarget
local PfText = pfwidget.PfText

local pfpiewidget = {}

pfpiewidget.PfPieSegment = class.strict {
  value = class.NULL,
  color = class.NULL,

  __init = function(self, value, color)
    self.value = value
    self.color = color
  end,
}

pfpiewidget.PfPieWidget = class.strict(PfCanvas) {
  pie_mesh = class.NULL,

  segments = class.NULL,
  radius = 128,
  radius_inner = 118,

  _percent_circle = 1,
  percent_circle = class.property {
    get = function(self) return self._percent_circle end,
    set = function(self, new)
      self._percent_circle = new
      self:remake_pie_mesh()
    end
  },

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)
      -- TODO
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)
       -- TODO
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.pie_mesh = PfMesh()
    self:add_child(self.pie_mesh)

    self.segments = {}
  end,

  add_segment = function(self, segment)
    table.insert(self.segments, segment)
  end,

  remake_pie_mesh = function(self)
    local total_value = 0
    for _, v in pairs(self.segments) do
      total_value = total_value + v.value
    end

    local pie_verts = {}
    local prev_x = self.radius * math.sin(0)
    local prev_y = self.radius * math.cos(0) * -1
    local prev_x_inner = self.radius_inner * math.sin(0)
    local prev_y_inner = self.radius_inner * math.cos(0) * -1
    for angle = 0, math.pi * 2 * self.percent_circle, math.pi / 180 do
      local percent = angle / (math.pi * 2 * self.percent_circle)
      local x = self.radius * math.sin(angle)
      local y = self.radius * math.cos(angle) * -1
      local x_inner = self.radius_inner * math.sin(angle)
      local y_inner = self.radius_inner * math.cos(angle) * -1

      local segment
      local segment_color
      local segment_start_value

      -- Figure out what segment this part of the pie uses
      local iter_percent = 0
      local iter_value = 0
      for _, v in ipairs(self.segments) do
        if iter_percent > percent then break end

        segment = v
        segment_color = v.color
        segment_start_value = iter_value
        iter_percent = iter_percent + (v.value / total_value)
        iter_value = iter_value + v.value
      end

      local segment_end_value = segment_start_value + segment.value
      local cur_value = total_value * percent
      local segment_percent = (cur_value - segment_start_value) / (segment_end_value - segment_start_value)

      local color_mult = math.min(1, segment_percent + 0.5)
      segment_color = {
        segment_color[1] * color_mult,
        segment_color[2] * color_mult,
        segment_color[3] * color_mult,
        segment_color[4],
      }

      -- Add the verts
      table.insert(pie_verts, {{prev_x_inner, prev_y_inner, 0}, segment_color})
      table.insert(pie_verts, {{prev_x      , prev_y      , 0}, segment_color})
      table.insert(pie_verts, {{x           , y           , 0}, segment_color})
      table.insert(pie_verts, {{x_inner     , y_inner     , 0}, segment_color})

      prev_x = x
      prev_y = y
      prev_x_inner = x_inner
      prev_y_inner = y_inner
    end
    self.pie_mesh.vertices = pie_verts
    self.pie_mesh.actor:SetDrawState{Mode = "DrawMode_Quads", First = 1, Num = #pie_verts}
  end,
}

return pfpiewidget
