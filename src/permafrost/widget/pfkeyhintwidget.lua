local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText

local pfkeyhintwidget = {}

pfkeyhintwidget.PfKeyHintWidget = class.strict(PfCanvas) {
  action_text = class.NULL,
  key_text = class.NULL,

  -- width = class.property {
  --   get = function(self) return self.actor:GetWidth() end,
  --   set = function(self, new)
  --     self.actor:SetWidth(new)

  --     self.bg_widget.width = new
  --     self.judge_bg_canvas.x = new / 2
  --     self.early_text.x = (-new / 2) + 4
  --     self.late_text.x = (new / 2) - 4
  --   end,
  -- },

  -- height = class.property {
  --   get = function(self) return self.actor:GetHeight() end,
  --   set = function(self, new)
  --     self.actor:SetHeight(new)

  --     self.bg_widget.height = new
  --     self.zero_line_widget.height = new
  --     self.median_line_widget.height = new
  --     self.mean_line_widget.height = new
  --     -- self.early_text.y = (new / 2) - 4
  --     -- self.late_text.y = (new / 2) - 4
  --     for _, widget in pairs(self.judge_backgrounds) do
  --       widget.height = new
  --     end
  --   end,
  -- },

  __init = function(self)
    PfCanvas.__init(self)

    self.action_text = PfText{}
    self.action_text:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(4, 4)
        actor.align = Vec2(0, 0)
        actor.scale = 0.5
      end
    )
    self:add_child(self.action_text)

    self.key_text = PfText{}
    self.key_text:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(4, 24)
        actor.align = Vec2(0, 0)
        actor.scale = 0.4
        actor.alpha = 0.5
      end
    )
    self:add_child(self.key_text)
  end,

  set_hint = function(self, action_text, key_text)
    self.action_text.text = action_text
    self.key_text.text = key_text

    -- Update our width for layout purposes
    self.ctx.timer.run_in_seconds(0, function()
      local action_text_width = self.action_text.width * self.action_text.scale_x
      local key_text_width = self.key_text.width * self.key_text.scale_x

      -- Add some spacing
      self.width = math.max(action_text_width, key_text_width) + 24
    end)
  end,
}

return pfkeyhintwidget
