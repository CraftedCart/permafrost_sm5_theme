local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pferrorbarwidget = require("permafrost.widget.pferrorbarwidget")
local pfcombowidget = require("permafrost.widget.pfcombowidget")
local pflifewidget = require("permafrost.widget.pflifewidget")
local pfjudgmentwidget = require("permafrost.widget.pfjudgmentwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfsignal = require("permafrost.pfsignal")
local pfnotefieldconfig = require("permafrost.pfnotefieldconfig")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfutil = require("permafrost.pfutil")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfAnchored = pfwidget.PfAnchored
local PfVBox = pfwidget.PfVBox
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfButton = pfwidget.PfButton
local PfToggleButton = pfwidget.PfToggleButton
local PfOptionButton = pfwidget.PfOptionButton
local PfOptionGroup = pfwidget.PfOptionGroup
local PfSpinBox = pfwidget.PfSpinBox
local PfComboBox = pfwidget.PfComboBox
local PfErrorBarWidget = pferrorbarwidget.PfErrorBarWidget
local PfComboWidget = pfcombowidget.PfComboWidget
local PfJudgmentWidget = pfjudgmentwidget.PfJudgmentWidget
local PfLifeWidget = pflifewidget.PfLifeWidget
local ESizeMode = pfwidget.ESizeMode
local PfNoteFieldConfig = pfnotefieldconfig.PfNoteFieldConfig
local EScrollMode = pfnotefieldconfig.EScrollMode

local pfnotepreview = {}

pfnotepreview.PfNotePreview = class.strict(PfCanvas) {
  -- Key: Note skin name, Value: PfCanvas
  noteskin_canvases = class.NULL,

  __init = function(self, noteskins, noteskin_name)
    PfCanvas.__init(self)

    if not noteskins then error("No noteskin list given") end
    noteskin_name = noteskin_name or noteskins[1]

    self.noteskin_canvases = {}

    local internal_canvas = PfCanvas()
    internal_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.scale = 2.25 -- StepMania magic
      end
    )
    self:add_child(internal_canvas)

    local style = GAMESTATE:GetCurrentStyle()
    local total_width = style:GetWidth(PLAYER_1)
    local columns = style:ColumnsPerPlayer()
    local column_width = total_width / columns

    for _, v in pairs(noteskins) do
      local ns_canvas = PfCanvas()
      if v ~= noteskin_name then
        ns_canvas:add_hook(
          "InitCommand",
          function(actor)
            actor.visible = false
          end
        )
      end
      internal_canvas:add_child(ns_canvas)

      self.noteskin_canvases[v] = ns_canvas

      local noteskin_widget1 = NOTESKIN:LoadActorForNoteSkin("Left", "Tap Note", v)
      local noteskin_widget2 = NOTESKIN:LoadActorForNoteSkin("Down", "Tap Note", v)
      local noteskin_widget3 = NOTESKIN:LoadActorForNoteSkin("Up", "Tap Note", v)
      local noteskin_widget4 = NOTESKIN:LoadActorForNoteSkin("Right", "Tap Note", v)

      noteskin_widget1.InitCommand = function(actor) actor:xy(  0 + 32, 0) end
      noteskin_widget2.InitCommand = function(actor) actor:xy( 64 + 32, 0) end
      noteskin_widget3.InitCommand = function(actor) actor:xy(128 + 32, 0) end
      noteskin_widget4.InitCommand = function(actor) actor:xy(192 + 32, 0) end

      ns_canvas:add_child(noteskin_widget1)
      ns_canvas:add_child(noteskin_widget2)
      ns_canvas:add_child(noteskin_widget3)
      ns_canvas:add_child(noteskin_widget4)

      -- noteskin_widget1 = NOTESKIN:LoadActorForNoteSkin("Left", "Hold TopCap Inactive", v)
      -- noteskin_widget2 = NOTESKIN:LoadActorForNoteSkin("Down", "Hold TopCap Active", v)
      -- noteskin_widget3 = NOTESKIN:LoadActorForNoteSkin("Up", "Roll TopCap Inactive", v)
      -- noteskin_widget4 = NOTESKIN:LoadActorForNoteSkin("Right", "Roll TopCap Active", v)

      -- noteskin_widget1.InitCommand = function(actor) actor:xy(  0 + 32, 64) actor:valign(1) end
      -- noteskin_widget2.InitCommand = function(actor) actor:xy( 64 + 32, 64) actor:valign(1) end
      -- noteskin_widget3.InitCommand = function(actor) actor:xy(128 + 32, 64) actor:valign(1) end
      -- noteskin_widget4.InitCommand = function(actor) actor:xy(192 + 32, 64) actor:valign(1) end

      -- ns_canvas:add_child(noteskin_widget1)
      -- ns_canvas:add_child(noteskin_widget2)
      -- ns_canvas:add_child(noteskin_widget3)
      -- ns_canvas:add_child(noteskin_widget4)

      noteskin_widget1 = NOTESKIN:LoadActorForNoteSkin("Left", "Hold Body Inactive", v)
      noteskin_widget2 = NOTESKIN:LoadActorForNoteSkin("Down", "Hold Body Active", v)
      noteskin_widget3 = NOTESKIN:LoadActorForNoteSkin("Up", "Roll Body Inactive", v)
      noteskin_widget4 = NOTESKIN:LoadActorForNoteSkin("Right", "Roll Body Active", v)

      noteskin_widget1.InitCommand = function(actor) actor:xy(  0 + 32, 64) actor:valign(0) end
      noteskin_widget2.InitCommand = function(actor) actor:xy( 64 + 32, 64) actor:valign(0) end
      noteskin_widget3.InitCommand = function(actor) actor:xy(128 + 32, 64) actor:valign(0) end
      noteskin_widget4.InitCommand = function(actor) actor:xy(192 + 32, 64) actor:valign(0) end

      ns_canvas:add_child(noteskin_widget1)
      ns_canvas:add_child(noteskin_widget2)
      ns_canvas:add_child(noteskin_widget3)
      ns_canvas:add_child(noteskin_widget4)

      -- noteskin_widget1 = NOTESKIN:LoadActorForNoteSkin("Left", "Hold BottomCap Inactive", v)
      -- noteskin_widget2 = NOTESKIN:LoadActorForNoteSkin("Down", "Hold BottomCap Active", v)
      -- noteskin_widget3 = NOTESKIN:LoadActorForNoteSkin("Up", "Roll BottomCap Inactive", v)
      -- noteskin_widget4 = NOTESKIN:LoadActorForNoteSkin("Right", "Roll BottomCap Active", v)

      -- noteskin_widget1.InitCommand = function(actor) actor:xy(  0 + 32, 128) actor:valign(0) end
      -- noteskin_widget2.InitCommand = function(actor) actor:xy( 64 + 32, 128) actor:valign(0) end
      -- noteskin_widget3.InitCommand = function(actor) actor:xy(128 + 32, 128) actor:valign(0) end
      -- noteskin_widget4.InitCommand = function(actor) actor:xy(192 + 32, 128) actor:valign(0) end

      -- ns_canvas:add_child(noteskin_widget1)
      -- ns_canvas:add_child(noteskin_widget2)
      -- ns_canvas:add_child(noteskin_widget3)
      -- ns_canvas:add_child(noteskin_widget4)
    end
  end,

  set_noteskin = function(self, noteskin_name)
    for k, v in pairs(self.noteskin_canvases) do
      v.visible = k == noteskin_name
    end
  end,
}

return pfnotepreview
