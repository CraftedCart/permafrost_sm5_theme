local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfiter = require("permafrost.pfiter")
local pfutil = require("permafrost.pfutil")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local ListIter = pfiter.ListIter

local pferrorbarwidget = {}

local TRIANGLE_PATH = THEME:GetPathG("", "triangle.png")

-- local PERCENTILES = {
  -- 0.00,
  -- 0.05,
  -- 0.10,
  -- 0.15,
  -- 0.20,
  -- 0.25,
  -- 0.30,
  -- 0.35,
  -- 0.40,
  -- 0.45,
-- }

pferrorbarwidget.PfErrorBarWidget = class.strict(PfCanvas) {
  judge_backgrounds = class.NULL,
  middle_marker_widget = class.NULL,
  early_text_widget = class.NULL,
  late_text_widget = class.NULL,
  error_marker_widgets = class.NULL,
  avg_error_marker_widget = class.NULL,
  -- percentile_bar_widgets = class.NULL,
  -- min_max_error_marker_widget = class.NULL,
  last_offsets = class.NULL,
  error_marker_index = 1,
  error_marker_count = 32,
  error_marker_time = 0.75, -- How long markers take to fade out

  avg_offset = 0,
  -- low_percentiles = class.NULL,
  -- high_percentiles = class.NULL,

  _width_multiplier = 3000,
  width_multiplier = class.property {
    get = function(self) return self._width_multiplier end,
    set = function(self, new)
      local last_window = 0
      for k, v in ipairs{
        {"w1", pfconfig.colors.judge_w1},
        {"w2", pfconfig.colors.judge_w2},
        {"w3", pfconfig.colors.judge_w3},
        {"w4", pfconfig.colors.judge_w4},
        {"w5", pfconfig.colors.judge_w5},
      } do
        local window = pftiming.scale_timing_window(pftiming.timing_windows[v[1]])
        local offset = last_window * self.width_multiplier
        local width = (window - last_window) * self.width_multiplier

        local left = self.judge_backgrounds[(k - 1) * 2 + 1]
        left.x = -offset
        left.width = width

        local right = self.judge_backgrounds[(k - 1) * 2 + 2]
        right.x = offset
        right.width = width

        last_window = window
      end

      self._width_multiplier = new
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)
      self.middle_marker_widget.height = new
      self.early_text_widget.y = new / 2
      self.late_text_widget.y = new / 2

      for _, widget in pairs(self.error_marker_widgets) do
        widget.height = new
      end

      for _, widget in pairs(self.judge_backgrounds) do
        widget.y = new / 2
      end

      -- for _, widget in pairs(self.percentile_bar_widgets) do
        -- widget.y = new / 2
      -- end
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    -- self.percentile_bar_widgets = {}
    -- self.low_percentiles = {}
    -- self.high_percentiles = {}
    -- for _, v in ipairs(PERCENTILES) do
      -- local percentile_error_marker_widget = PfRect()
      -- percentile_error_marker_widget:add_hook(
        -- "InitCommand",
        -- function(actor)
          -- actor.position = Vec2(0, 0)
          -- actor.align = Vec2(0, 0.5)
          -- actor.size = Vec2(0, 4)
          -- actor.color = pfconfig.palette.white
          -- actor.alpha = 0.05
        -- end
      -- )
      -- self:add_child(percentile_error_marker_widget)

      -- table.insert(self.percentile_bar_widgets, percentile_error_marker_widget)
      -- table.insert(self.low_percentiles, 0)
      -- table.insert(self.high_percentiles, 0)
    -- end

    -- Make judge backgrounds
    self.judge_backgrounds = {}
    local last_window = 0
    for _, v in ipairs{
      {"w1", pfconfig.colors.judge_w1},
      {"w2", pfconfig.colors.judge_w2},
      {"w3", pfconfig.colors.judge_w3},
      {"w4", pfconfig.colors.judge_w4},
      {"w5", pfconfig.colors.judge_w5},
    } do
      local window = pftiming.scale_timing_window(pftiming.timing_windows[v[1]])
      local offset = last_window * self.width_multiplier
      local width = (window - last_window) * self.width_multiplier

      local left = pfwidget.PfRect()
      left:add_hook(
        "InitCommand",
        function(actor)
          actor.position = Vec2(-offset, 0)
          actor.align = Vec2(1, 0.5)
          actor.size = Vec2(width, 4)
          actor.color = v[2]
          actor.alpha = 0.2
        end
      )
      self:add_child(left)
      table.insert(self.judge_backgrounds, left)

      local right = PfRect()
      right:add_hook(
        "InitCommand",
        function(actor)
          actor.position = Vec2(offset, 0)
          actor.align = Vec2(0, 0.5)
          actor.size = Vec2(width, 4)
          actor.color = v[2]
          actor.alpha = 0.2
        end
      )
      self:add_child(right)
      table.insert(self.judge_backgrounds, right)

      last_window = window
    end

    self.early_text_widget = PfText{}
    self.early_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(-64, 0)
        actor.align = Vec2(1, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.text_input_text
        actor.text = pflang.get_entry("error_early")

        self.ctx.anim{
          start_val = 1,
          end_val = 0,
          prop_table = self.early_text_widget,
          prop_name = "alpha",
          duration = 3,
          easing = pfeasing.linear,
        }

        self.ctx.anim{
          start_val = -32,
          end_val = -64,
          prop_table = self.early_text_widget,
          prop_name = "x",
          duration = 3,
          easing = pfeasing.linear,
        }
      end
    )
    self:add_child(self.early_text_widget)

    self.late_text_widget = PfText{}
    self.late_text_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(64, 0)
        actor.align = Vec2(0, 0.5)
        actor.scale = 0.5
        actor.color = pfconfig.colors.text_input_text
        actor.text = pflang.get_entry("error_late")

        self.ctx.anim{
          start_val = 1,
          end_val = 0,
          prop_table = self.late_text_widget,
          prop_name = "alpha",
          duration = 3,
          easing = pfeasing.linear,
        }

        self.ctx.anim{
          start_val = 32,
          end_val = 64,
          prop_table = self.late_text_widget,
          prop_name = "x",
          duration = 3,
          easing = pfeasing.linear,
        }
      end
    )
    self:add_child(self.late_text_widget)

    self:add_hook(
      "EndCommand",
      function()
        self.ctx.anim.stop_animating_table(self.early_text_widget)
        self.ctx.anim.stop_animating_table(self.late_text_widget)
      end
    )

    self.error_marker_widgets = {}

    for _ = 1, self.error_marker_count do
      local w = pfwidget.PfRect()
      w:add_hook(
        "InitCommand",
        function(actor)
          actor.position = Vec2(0, 0)
          actor.align = Vec2(0.5, 0)
          actor.size = Vec2(4, 1)
          actor.alpha = 0
        end
      )
      self:add_child(w)

      table.insert(self.error_marker_widgets, w)
    end

    self.last_offsets = {}
    self.avg_error_marker_widget = PfRect()
    self.avg_error_marker_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, -8)
        actor.align = Vec2(0.5, 0)
        actor.color = pfconfig.palette.white
        -- actor.alpha = 0.5
        actor:load_texture(TRIANGLE_PATH)
        actor.size = Vec2(16, 16)
      end
    )
    self:add_child(self.avg_error_marker_widget)

    self.middle_marker_widget = PfRect()
    self.middle_marker_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0.5, 0)
        actor.size = Vec2(4, 1)
        actor.color = pfconfig.palette.white
        -- actor.alpha = 0.5
      end
    )
    self:add_child(self.middle_marker_widget)

    self.ctx.timer.connect_tick(function(delta_seconds) self:update(delta_seconds) end)
  end,

  update = function(self, delta_seconds)
    -- Fade away all marker bars
    for _, widget in pairs(self.error_marker_widgets) do
      widget.alpha = widget.alpha - (delta_seconds * self.error_marker_time)
    end

    local lerp_alpha = math.min(delta_seconds / 0.25, 1)

    -- Update error arrow
    local avg_target_x = self.avg_offset * self.width_multiplier
    self.avg_error_marker_widget.x = pfmath.lerp(self.avg_error_marker_widget.x, avg_target_x, lerp_alpha)

    -- Update percentile error bars
    -- for k, _ in pairs(PERCENTILES) do
      -- local percentile_low_target_x = self.low_percentiles[k] * self.width_multiplier
      -- local percentile_high_target_x = self.high_percentiles[k] * self.width_multiplier

      -- local percentile_low_x = pfmath.lerp(self.percentile_bar_widgets[k].x, percentile_low_target_x, lerp_alpha)
      -- local percentile_high_x = pfmath.lerp(self.percentile_bar_widgets[k].x + self.percentile_bar_widgets[k].width, percentile_high_target_x, lerp_alpha)

      -- self.percentile_bar_widgets[k].x = percentile_low_x
      -- self.percentile_bar_widgets[k].width = percentile_high_x - percentile_low_x
    -- end
  end,

  on_tap_judgment = function(self, args)
    if args.tap_note_score == "TapNoteScore_Miss" then return end

    local w = self.error_marker_widgets[self.error_marker_index]
    w.x = args.tap_note_offset * self.width_multiplier
    w.alpha = 1
    w.color = pfconfig.tap_note_score_colors[args.tap_note_score]

    self.error_marker_index = self.error_marker_index + 1
    if self.error_marker_index > self.error_marker_count then
      self.error_marker_index = 1
    end

    table.insert(self.last_offsets, args.tap_note_offset)
    if #self.last_offsets > 16 then
      table.remove(self.last_offsets, 1)
    end

    -- Recalc avg offet
    local all_offsets = ListIter(self.last_offsets):sum()
    local avg_offset = all_offsets / #self.last_offsets
    self.avg_offset = avg_offset

    -- Recalc percentiles
    -- local sorted_offsets = pfutil.table_copy(self.last_offsets)
    -- table.sort(sorted_offsets)

    -- for k, v in pairs(PERCENTILES) do
      -- local index_low = math.ceil((#sorted_offsets) * v)
      -- local index_high = math.floor((#sorted_offsets) * (1 - v))

      -- self.low_percentiles[k] = sorted_offsets[index_low] or 0
      -- self.high_percentiles[k] = sorted_offsets[index_high] or 0
    -- end
  end,

  get_display_width = function(self)
    local window = pftiming.scale_timing_window(pftiming.timing_windows["w5"])
    return window * self.width_multiplier * 2
  end,
}

return pferrorbarwidget
