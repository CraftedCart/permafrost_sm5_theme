local pfmath = require("permafrost.pfmath")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfsignal = require("permafrost.pfsignal")
local pfeasing = require("permafrost.pfeasing")
local pftiming = require("permafrost.pftiming")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local class = require("permafrost.class")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfMesh = pfwidget.PfMesh
local PfRenderTarget = pfwidget.PfRenderTarget
local PfText = pfwidget.PfText

local CIRCLE4_PATH = THEME:GetPathG("", "circle4.png")

local timing_colors = {
  w1 = pfconfig.colors.judge_w1,
  w2 = pfconfig.colors.judge_w2,
  w3 = pfconfig.colors.judge_w3,
  w4 = pfconfig.colors.judge_w4,
  w5 = pfconfig.colors.judge_w5,
  miss = pfconfig.colors.judge_miss,
}

local function make_line(vert_table, from, to, width, from_color, to_color)
  local opp = to.x - from.x
  local adj = to.y - from.y
  local hyp = (opp * opp + adj * adj) ^ 0.5

  local opp_div_hyp = opp / hyp
  local adj_div_hyp = adj / hyp

  local x_dist = adj_div_hyp * width / 2
  local y_dist = opp_div_hyp * width / 2

  local half_width = width / 2

  -- Add a circle
  table.insert(vert_table, {{to.x - half_width, to.y - half_width, 0}, to_color, {0, 0}})
  table.insert(vert_table, {{to.x + half_width, to.y - half_width, 0}, to_color, {1, 0}})
  table.insert(vert_table, {{to.x + half_width, to.y + half_width, 0}, to_color, {1, 1}})
  table.insert(vert_table, {{to.x - half_width, to.y + half_width, 0}, to_color, {0, 1}})

  -- Add the line
  table.insert(vert_table, {{from.x + x_dist, from.y - y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{from.x - x_dist, from.y + y_dist, 0}, from_color, {0.5, 0.5}})
  table.insert(vert_table, {{to.x - x_dist  , to.y + y_dist  , 0}, to_color  , {0.5, 0.5}})
  table.insert(vert_table, {{to.x + x_dist  , to.y - y_dist  , 0}, to_color  , {0.5, 0.5}})
end

local NoteOffset = class.strict {
  time = 0,
  offset = 0,

  __init = function(self, time, offset)
    self.time = time
    self.offset = offset
  end,
}

local pfoffsetgraphwidget = {}

pfoffsetgraphwidget.PfOffsetGraphWidget = class.strict(PfCanvas) {
  bg_widget = class.NULL,
  judge_bg_canvas = class.NULL,
  judge_backgrounds = class.NULL,
  zero_line_widget = class.NULL,
  plot_mesh_rt = class.NULL,
  plot_mesh = class.NULL,
  plot_mesh_tex = class.NULL,
  line_mesh_rt = class.NULL,
  line_mesh = class.NULL,
  line_mesh_tex = class.NULL,
  early_text = class.NULL,
  late_text = class.NULL,

  circle4_texture = class.NULL,
  note_offsets = class.NULL,
  init_seconds = class.NULL,

  height_changed_signal = class.NULL,

  width = class.property {
    get = function(self) return self.actor:GetWidth() end,
    set = function(self, new)
      self.actor:SetWidth(new)

      self.bg_widget.width = new
      self.zero_line_widget.width = new
      self.plot_mesh_rt.width = new
      self.line_mesh_rt.width = new
      self.early_text.x = new - 4
      self.late_text.x = new - 4
      for _, widget in pairs(self.judge_backgrounds) do
        widget.width = new
      end
      -- self:remake_plot_mesh()
    end,
  },

  height = class.property {
    get = function(self) return self.actor:GetHeight() end,
    set = function(self, new)
      self.actor:SetHeight(new)

      self.bg_widget.height = new
      self.judge_bg_canvas.y = new / 2
      self.plot_mesh_rt.height = new
      self.line_mesh_rt.height = new
      self.plot_mesh.y = new / 2
      self.line_mesh.y = new / 2
      self.early_text.y = new / 2 - 4
      self.late_text.y = -(new / 2) + 4
      -- self:remake_plot_mesh()

      self.height_changed_signal:emit(new)
    end,
  },

  __init = function(self)
    PfCanvas.__init(self)

    self.height_changed_signal = PfSignal()

    self.judge_bg_canvas = PfCanvas()
    self.judge_bg_canvas:add_hook(
      "InitCommand",
      function(actor)
        actor.alpha = 0
      end
    )
    self:add_child(self.judge_bg_canvas)

    self.bg_widget = PfRect()
    self.bg_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0.5)
        actor.color = pfconfig.palette.gray_dark
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.bg_widget)

    self.zero_line_widget = PfRect()
    self.zero_line_widget:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0.5)
        actor.height = 2
        actor.color = pfconfig.colors.judge_w1
        actor.alpha = 0.5
      end
    )
    self.judge_bg_canvas:add_child(self.zero_line_widget)

    -- Make judge backgrounds
    local ms_text = pflang.get_entry("milliseconds_short")
    self.judge_backgrounds = {}
    local last_window = 0
    local max_window = pftiming.scale_timing_window(pftiming.timing_windows["w5"])
    for _, v in ipairs{
      {"w1", pfconfig.colors.judge_w1},
      {"w2", pfconfig.colors.judge_w2},
      {"w3", pfconfig.colors.judge_w3},
      {"w4", pfconfig.colors.judge_w4},
      {"w5", pfconfig.colors.judge_w5},
    } do
      local window = pftiming.scale_timing_window(pftiming.timing_windows[v[1]])

      local bottom = PfRect()
      bottom:add_hook(
        "InitCommand",
        function(actor)
          actor.align = Vec2(0, 0)
          actor.height = 12
          actor.color = v[2]
          actor.alpha = 0.1
          actor.fade_bottom = 1
        end
      )
      self.judge_bg_canvas:add_child(bottom)
      table.insert(self.judge_backgrounds, bottom)

      local bottom_text = PfText{}
      bottom_text:add_hook(
        "InitCommand",
        function(actor)
          actor.x = 2
          actor.scale = 0.35
          actor.align = Vec2(0, 1)
          actor.color = pfconfig.colors.title_text
          actor.text = string.format("-%03.0f %s", window * 1000, ms_text)
          actor.color = v[2]
          actor.alpha = 0.8
        end
      )
      self.judge_bg_canvas:add_child(bottom_text)

      local top = PfRect()
      top:add_hook(
        "InitCommand",
        function(actor)
          actor.align = Vec2(0, 1)
          actor.height = 12
          actor.color = v[2]
          actor.alpha = 0.1
          actor.fade_top = 1
        end
      )
      self.judge_bg_canvas:add_child(top)
      table.insert(self.judge_backgrounds, top)

      local top_text = PfText{}
      top_text:add_hook(
        "InitCommand",
        function(actor)
          actor.x = 2
          actor.scale = 0.35
          actor.align = Vec2(0, 0)
          actor.color = pfconfig.colors.title_text
          actor.text = string.format("+%03.0f %s", window * 1000, ms_text)
          actor.color = v[2]
          actor.alpha = 0.8
        end
      )
      self.judge_bg_canvas:add_child(top_text)

      local capture_last_window = last_window
      self.height_changed_signal:connect(function(new)
          local offset = (capture_last_window / max_window) * (new / 2)
          local text_offset = (window / max_window) * (new / 2)
          -- local height = ((window - capture_last_window) / max_window) * (new / 2)

          bottom.y = offset
          bottom_text.y = text_offset - 2
          top.y = -offset
          top_text.y = -text_offset + 2
      end)

      last_window = window
    end

    self.early_text = PfText{}
    self.early_text:add_hook(
      "InitCommand",
      function(actor)
        actor.scale = 0.4
        actor.align = Vec2(1, 1)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.get_entry("error_early")
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.early_text)

    self.late_text = PfText{}
    self.late_text:add_hook(
      "InitCommand",
      function(actor)
        actor.scale = 0.4
        actor.align = Vec2(1, 0)
        actor.color = pfconfig.colors.title_text
        actor.text = pflang.get_entry("error_late")
        actor.alpha = 0.8
      end
    )
    self.judge_bg_canvas:add_child(self.late_text)

    -- STEPMANIA WHYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
    -- (There doesn't seem to be any good way to load a RageTexture)
    local useless_texture_loader = PfRect()
    useless_texture_loader:add_hook(
      "InitCommand",
      function(actor)
        actor.visible = false
        actor:load_texture(CIRCLE4_PATH)
        self.circle4_texture = actor.texture
      end
    )
    self:add_child(useless_texture_loader)

    self.plot_mesh_rt = PfRenderTarget()
    self.plot_mesh_rt:add_hook(
      "InitCommand",
      function(actor)
        actor.use_alpha_buffer = true
      end
    )
    self:add_child(self.plot_mesh_rt)

    self.plot_mesh = PfMesh()
    self.plot_mesh:add_hook(
      "InitCommand",
      function(actor)
        actor.texture = self.circle4_texture
      end
    )
    self.plot_mesh_rt:add_child(self.plot_mesh)

    self.plot_mesh_tex = PfRect()
    self.plot_mesh_tex:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.visible = false
      end
    )
    self:add_child(self.plot_mesh_tex)

    self.line_mesh_rt = PfRenderTarget()
    self.line_mesh_rt:add_hook(
      "InitCommand",
      function(actor)
        actor.use_alpha_buffer = true
      end
    )
    self:add_child(self.line_mesh_rt)

    self.line_mesh = PfMesh()
    self.line_mesh_rt:add_child(self.line_mesh)

    self.line_mesh_tex = PfRect()
    self.line_mesh_tex:add_hook(
      "InitCommand",
      function(actor)
        actor.position = Vec2(0, 0)
        actor.align = Vec2(0, 0)
        actor.visible = false
      end
    )
    self:add_child(self.line_mesh_tex)

    -- Fetch and sort note offsets
    local player_stats = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1)
    local offsets = player_stats:GetOffsetVector()
    local rows = player_stats:GetNoteRowVector()
    local timing_data = GAMESTATE:GetCurrentSteps(PLAYER_1):GetTimingData()
    local note_times = {}
    for i = 1, #rows do
      note_times[i] = timing_data:GetElapsedTimeFromNoteRow(rows[i])
    end

    self.note_offsets = {}
    for i = 1, #offsets do
      local time = note_times[i]
      local offset = offsets[i] / 1000
      table.insert(self.note_offsets, NoteOffset(time, offset))
    end
    -- Sort by time (since it's possible to hit notes out-of-order)
    table.sort(self.note_offsets, function(a, b) return a.time < b.time end)
  end,

  remake_plot_mesh = function(self)
    local max_window = pftiming.scale_timing_window(pftiming.timing_windows["w5"])
    local length = GAMESTATE:GetCurrentSong(PLAYER_1):GetLastSecond()

    local last_offsets = {}
    local avg_offset = 0
    local avg_color
    local prev_x = 0
    local prev_avg_y = 0
    local prev_avg_color = timing_colors.w1

    -- Loop through all note offsets
    local dot_verts = {}
    local line_verts = {}
    for _, note_offset in ipairs(self.note_offsets) do
      local offset = note_offset.offset
      local timing_window = pftiming.get_window_for_offset(offset)

      if timing_window ~= "miss" then
        table.insert(last_offsets, offset)
        if #last_offsets > 16 then
          table.remove(last_offsets, 1)
        end
      end

      if #last_offsets ~= 0 then
        for _, v in pairs(last_offsets) do
          avg_offset = avg_offset + v
        end
        avg_offset = avg_offset / #last_offsets
        avg_color = timing_colors[pftiming.get_window_for_offset(avg_offset)]
      end

      local x = (note_offset.time / length) * self.width
      local target_y = math.max((offset / max_window) * (self.height / 2) * -1, -self.height / 2)

      -- Add a line
      local avg_y = math.max((avg_offset / max_window) * (self.height / 2) * -1, -self.height / 2)
      local line_from = Vec2(prev_x, prev_avg_y)
      local line_to = Vec2(x, avg_y)
      local line_from_color = prev_avg_color
      local line_to_color = lerp_color(0.1, prev_avg_color, avg_color)

      make_line(line_verts, line_from, line_to, 3, line_from_color, line_to_color)

      local trans_from = Alpha(line_from_color, 0.4)
      local trans_to = Alpha(line_to_color, 0.4)
      table.insert(line_verts, {{line_from.x, line_from.y, 0}, trans_from, {0.5, 0.5}})
      table.insert(line_verts, {{line_to.x  , line_to.y  , 0}, trans_to  , {0.5, 0.5}})
      table.insert(line_verts, {{line_to.x  , 0          , 0}, trans_to  , {0.5, 0.5}})
      table.insert(line_verts, {{line_from.x, 0          , 0}, trans_from, {0.5, 0.5}})

      -- Add a dot
      local window_color = Alpha(timing_colors[timing_window], 0.7)
      local y = target_y

      table.insert(dot_verts, {{x - 2, y - 2, 0}, window_color, {0, 0}})
      table.insert(dot_verts, {{x + 2, y - 2, 0}, window_color, {1, 0}})
      table.insert(dot_verts, {{x + 2, y + 2, 0}, window_color, {1, 1}})
      table.insert(dot_verts, {{x - 2, y + 2, 0}, window_color, {0, 1}})

      prev_x = x
      prev_avg_y = avg_y
      prev_avg_color = line_to_color
    end

    self.plot_mesh.vertices = dot_verts
    self.plot_mesh.actor:SetDrawState{Mode = "DrawMode_Quads", First = 1, Num = #dot_verts}
    self.line_mesh.vertices = line_verts
    self.line_mesh.actor:SetDrawState{Mode = "DrawMode_Quads", First = 1, Num = #line_verts}

    self.plot_mesh_rt:capture()
    self.line_mesh_rt:capture()
  end,

  show = function(self)
    self.init_seconds = pftimer.elapsed_seconds

    self:remake_plot_mesh()

    local next_width = pfmath.next_pow2(self.width + 1)
    local next_height = pfmath.next_pow2(self.height)

    self.plot_mesh_tex.texture = self.plot_mesh_rt.texture
    self.plot_mesh_tex.width = next_width
    self.plot_mesh_tex.height = next_height
    self.line_mesh_tex.texture = self.line_mesh_rt.texture
    self.line_mesh_tex.width = next_width
    self.line_mesh_tex.height = next_height
    self.line_mesh_tex.visible = true

    local FADE_PIXELS = 256
    local width_percent = (self.width + FADE_PIXELS) / next_width
    local fade_percent = FADE_PIXELS / next_width

    self.ctx.timer.run_in_seconds(
      0.25,
      function()
        self.plot_mesh_tex.visible = true
        self.plot_mesh_tex.fade_right = fade_percent
        self.ctx.anim{
          start_val = 1,
          end_val = 1 - width_percent,
          prop_table = self.plot_mesh_tex,
          prop_name = "crop_right",
          duration = 1.25,
          easing = pfeasing.linear,
        }
      end
    )

    self.line_mesh_tex.fade_right = fade_percent
    self.ctx.anim{
      start_val = 1,
      end_val = 1 - width_percent,
      prop_table = self.line_mesh_tex,
      prop_name = "crop_right",
      duration = 1,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = 0,
      end_val = 1,
      prop_table = self.judge_bg_canvas,
      prop_name = "alpha",
      duration = 2,
      easing = pfeasing.linear,
    }

    self.ctx.anim{
      start_val = 0,
      end_val = 1,
      prop_table = self.judge_bg_canvas,
      prop_name = "scale_y",
      duration = 2,
      easing = pfeasing.out_expo,
    }
  end,
}

return pfoffsetgraphwidget
