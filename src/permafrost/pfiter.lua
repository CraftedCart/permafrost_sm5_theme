local class = require("permafrost.class")

local pfiter = {}

pfiter.Iterator = class.strict {
  internal_next = function(self)
    error("You need to override internal_next! Also don't call the super.")
  end,

  next = function(self)
    return self:internal_next()
  end,

  filter = function(self, predicate)
    return pfiter.FilterIter(self, predicate)
  end,

  map = function(self, func)
    return pfiter.MapIter(self, func)
  end,

  sort = function(self, predicate)
    local vals = self:collect()
    table.sort(vals, predicate)

    return pfiter.ListIter(vals)
  end,

  skip = function(self, num_to_skip)
    for i = 1, num_to_skip do self:next() end
    return self
  end,

  take = function(self, num_to_take)
    return pfiter.TakeIter(self, num_to_take)
  end,

  sum = function(self)
    local total = 0
    local next_val = self:next()
    while next_val ~= nil do
      total = total + next_val
      next_val = self:next()
    end

    return total
  end,

  average = function(self)
    local length = 0
    local total = 0
    local next_val = self:next()
    while next_val ~= nil do
      total = total + next_val
      length = length + 1
      next_val = self:next()
    end

    return total / length
  end,

  collect = function(self)
    local out = {}

    while true do
      local val = self:next()
      if val == nil then break end

      table.insert(out, val)
    end

    return out
  end,
}

pfiter.FilterIter = class.strict(pfiter.Iterator) {
  from_iter = class.NULL,
  predicate = class.NULL,

  __init = function(self, from_iter, predicate)
    pfiter.Iterator.__init(self)

    self.from_iter = from_iter
    self.predicate = predicate
  end,

  internal_next = function(self)
    local next_val = self.from_iter:next()
    if next_val == nil then return nil end

    while not self.predicate(next_val) do
      next_val = self.from_iter:next()
      if next_val == nil then return nil end
    end

    return next_val
  end,
}

pfiter.MapIter = class.strict(pfiter.Iterator) {
  from_iter = class.NULL,
  func = class.NULL,

  __init = function(self, from_iter, func)
    pfiter.Iterator.__init(self)

    self.from_iter = from_iter
    self.func = func
  end,

  internal_next = function(self)
    local next_val = self.from_iter:next()
    if next_val == nil then return nil end

    return self.func(next_val)
  end,
}

pfiter.TakeIter = class.strict(pfiter.Iterator) {
  from_iter = class.NULL,
  to_take = class.NULL,
  num_taken = 0,

  __init = function(self, from_iter, to_take)
    pfiter.Iterator.__init(self)

    self.from_iter = from_iter
    self.to_take = to_take
  end,

  internal_next = function(self)
    if self.num_taken > self.to_take then return nil end

    local next_val = self.from_iter:next()
    if next_val == nil then return nil end

    self.num_taken = self.num_taken + 1

    return next_val
  end,
}

pfiter.ListIter = class.strict(pfiter.Iterator) {
  list = class.NULL,
  idx = 1,

  __init = function(self, list)
    pfiter.Iterator.__init(self)

    self.list = list
  end,

  internal_next = function(self)
    local val = self.list[self.idx]
    self.idx = self.idx + 1

    return val
  end,
}

return pfiter
