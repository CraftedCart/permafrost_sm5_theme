local class = require("permafrost.class")
local pfutil = require("permafrost.pfutil")

local pfsong = {}

pfsong.songs = {}
pfsong.groups = {}

pfsong.PfSong = class.strict {
  background_path = class.property {
    get = function(self) return self.sm_song:GetBackgroundPath() end,
    set = function(self, new) error("Setter should not be used") end,
  },

  banner_path = class.property {
    get = function(self) return self.sm_song:GetBannerPath() end,
    set = function(self, new) error("Setter should not be used") end,
  },

  display_main_title = class.NULL,
  main_title = class.NULL,
  display_sub_title = class.NULL,
  display_artist = class.NULL,
  group = class.NULL,
  group_name = class.property {
    get = function(self) return self.group.group_name end,
    set = function(self, new) error("Setter should not be used") end,
  },

  music_path = class.property {
    get = function(self) return self.sm_song:GetMusicPath() end,
    set = function(self, new) error("Setter should not be used") end,
  },
  preview_music_path = class.property {
    get = function(self) return self.sm_song:GetPreviewMusicPath() end,
    set = function(self, new) error("Setter should not be used") end,
  },
  preview_start = class.NULL,
  music_length = class.NULL,
  gameplay_length = class.NULL,

  _cached_min_bpm = class.NULL,
  min_bpm = class.property {
    get = function(self)
      if self._cached_min_bpm == class.NULL then
        self._cached_min_bpm = self.sm_song:GetTimingData():GetActualBPM()[1]
      end

      return self._cached_min_bpm
    end,
    set = function(self, new) error("Setter should not be used") end,
  },

  _cached_max_bpm = class.NULL,
  max_bpm = class.property {
    get = function(self)
      if self._cached_max_bpm == class.NULL then
        self._cached_max_bpm = self.sm_song:GetTimingData():GetActualBPM()[2]
      end

      return self._cached_max_bpm
    end,
    set = function(self, new) error("Setter should not be used") end,
  },

  steps = class.NULL,

  sm_song = class.NULL,

  __init = function(self, sm_song)
    self.display_main_title = sm_song:GetDisplayMainTitle()
    self.main_title = sm_song:GetMainTitle()
    self.display_sub_title = sm_song:GetDisplaySubTitle()
    self.display_artist = sm_song:GetDisplayArtist()
    self.group = pfsong.get_or_create_group(sm_song:GetGroupName())

    -- self.music_path = sm_song:GetMusicPath()
    -- self.preview_music_path = sm_song:GetPreviewMusicPath()
    self.preview_start = sm_song:GetSampleStart()
    self.music_length = sm_song:MusicLengthSeconds()
    self.gameplay_length = sm_song:GetLastSecond()

    self.steps = {}
    for _, step in pairs(sm_song:GetAllSteps()) do
      table.insert(self.steps, pfsong.PfStep(step, self))
    end

    self.sm_song = sm_song
  end,
}

pfsong.PfStep = class.strict {
  sm_step = class.NULL,
  song = class.NULL,
  difficulty = class.NULL,

  __init = function(self, sm_step, pf_song)
    self.sm_step = sm_step
    self.song = pf_song
    self.difficulty = pfutil.is_etterna and sm_step:GetMSD(1, 1) or sm_step:GetMeter()
  end,
}

pfsong.PfSongGroup = class.strict {
  group_name = class.NULL,

  __init = function(self, group_name)
    self.group_name = group_name
  end,
}

function pfsong.get_current_song()
  return pfsong.PfSong(GAMESTATE:GetCurrentSong())
end

function pfsong.get_group_by_name(group_name)
  for _, v in pairs(pfsong.groups) do
    if v.group_name == group_name then
      return v
    end
  end

  -- No matching group found
  return nil
end

function pfsong.get_or_create_group(group_name)
  local group = pfsong.get_group_by_name(group_name)
  if group then
    return group
  end

  -- We need to create a new group
  group = pfsong.PfSongGroup(group_name)
  table.insert(pfsong.groups, group)
  return group
end

local TO_INDEX_PER_FRAME = 5000
if jit ~= nil then
  TO_INDEX_PER_FRAME = 10000
end

-- Providing a progress callback will make this async
function pfsong.index_songs(progress_callback)
  pfsong.songs = {}
  pfsong.groups = {}

  local all_songs = SONGMAN:GetAllSongs()
  local all_songs_len = #all_songs
  for k, v in pairs(all_songs) do
    table.insert(pfsong.songs, pfsong.PfSong(v))

    if progress_callback ~= nil and k % TO_INDEX_PER_FRAME == 0 then
      progress_callback(k, all_songs_len)
      coroutine.yield()
    end
  end

  if progress_callback ~= nil then
    progress_callback(all_songs_len, all_songs_len)
  end
end

-- Index songs if they aren't already indexed
-- This exists because this can be slow in vanilla StepMania, presumably because it uses the reference Lua impl rather
-- than LuaJIT
-- Providing a progress callback will make this async
function pfsong.conditional_index_songs(progress_callback)
  if #pfsong.songs < 1 then
    pfsong.index_songs(progress_callback)
  end
end

function pfsong.needs_indexing()
  return #pfsong.songs < 1
end

function pfsong.get_all_steps()
  local t = {}

  for _, song in pairs(pfsong.songs) do
    local steps = pfutil.table_copy(song.steps)

    table.sort(
      steps,
      function(a, b)
        return a.difficulty < b.difficulty
      end
    )

    for _, step in ipairs(steps) do
      table.insert(t, step)
    end
  end

  return t
end

pfsong.sort_methods = {
  difficulty = {
    asc = function(a, b) return a.difficulty < b.difficulty end,
    dec = function(a, b) return a.difficulty > b.difficulty end,
    cmp = function(a, b) return a.difficulty == b.difficulty and 0 or a.difficulty < b.difficulty and 1 or -1 end,
  },
  display_main_title = {
    asc = function(a, b) return a.song.display_main_title < b.song.display_main_title end,
    dec = function(a, b) return a.song.display_main_title > b.song.display_main_title end,
    cmp = function(a, b) return a.song.display_main_title == b.song.display_main_title and 0 or a.song.display_main_title < b.song.display_main_title and 1 or -1 end,
  },
  display_sub_title = {
    asc = function(a, b) return a.song.display_sub_title < b.song.display_sub_title end,
    dec = function(a, b) return a.song.display_sub_title > b.song.display_sub_title end,
    cmp = function(a, b) return a.song.display_sub_title == b.song.display_sub_title and 0 or a.song.display_sub_title < b.song.display_sub_title and 1 or -1 end,
  },
  group = {
    asc = function(a, b) return a.song.group_name < b.song.group_name end,
    dec = function(a, b) return a.song.group_name > b.song.group_name end,
    cmp = function(a, b) return a.song.group_name == b.song.group_name and 0 or a.song.group_name < b.song.group_name and 1 or -1 end,
  },
  min_bpm = {
    asc = function(a, b) return a.song.min_bpm < b.song.min_bpm end,
    dec = function(a, b) return a.song.min_bpm > b.song.min_bpm end,
    cmp = function(a, b) return a.song.min_bpm == b.song.min_bpm and 0 or a.song.min_bpm < b.song.min_bpm and 1 or -1 end,
  },
  max_bpm = {
    asc = function(a, b) return a.song.max_bpm < b.song.max_bpm end,
    dec = function(a, b) return a.song.max_bpm > b.song.max_bpm end,
    cmp = function(a, b) return a.song.max_bpm == b.song.max_bpm and 0 or a.song.max_bpm < b.song.max_bpm and 1 or -1 end,
  },
  gameplay_length = {
    asc = function(a, b) return a.song.gameplay_length < b.song.gameplay_length end,
    dec = function(a, b) return a.song.gameplay_length > b.song.gameplay_length end,
    cmp = function(a, b) return a.song.gameplay_length == b.song.gameplay_length and 0 or a.song.gameplay_length < b.song.gameplay_length and 1 or -1 end,
  },
}

pfsong.filter_parameters = {
  difficulty = function(step) return step.difficulty end,
  group = function(step) return step.song.group_name end,
  min_bpm = function(step) return step.song.min_bpm end,
  max_bpm = function(step) return step.song.max_bpm end,
  gameplay_length = function(step) return step.song.gameplay_length end,
}

return pfsong
