local pfinput = require("permafrost.pfinput")
local pfsignal = require("permafrost.pfsignal")
local pffs = require("permafrost.pffs")
local json = require("permafrost.json")

local PfSignal = pfsignal.PfSignal
local FileEntry = pffs.FileEntry

local pfutil = {}

local function string_starts_with(str, start)
  return str:sub(1, #start) == start
end
pfutil.string_starts_with = string_starts_with

-- Duplicated from init.lua, because the two files should not depend on each other
-- (init.lua is responsible for module loading)
local function unload_pf_modules()
  local to_unload = {}

  for k, _ in pairs(package.loaded) do
    if string_starts_with(k, "permafrost") then
      table.insert(to_unload, k)
    end
  end

  for _, v in pairs(to_unload) do
    package.loaded[v] = nil
  end
end

function pfutil.reload_theme()
  unload_pf_modules()

  SCREENMAN:SetNewScreen(SCREENMAN:GetTopScreen():GetName())
  THEME:ReloadMetrics()
  SCREENMAN:ReloadOverlayScreens()
  collectgarbage() -- Clean up stale actors
  require("permafrost.pfinput").push_input_layer() -- Need to do the require as we just unloaded pf modules

  SCREENMAN:SystemMessage("Theme reloaded")
end

pfutil.is_etterna = GAMESTATE.GetEtternaVersion ~= nil
pfutil.ETTERNA_CURRENT_WIFE_VERSION = 3

-- Return a table composed of the individual characters from a string.
local explode = function(str)
  local t = {}
  for i=1, #str do
    t[#t + 1] = string.sub(str, i, i)
  end

  return t
end

-- Yoinked from https://github.com/telemachus/split/blob/master/src/split.lua
function pfutil.string_split(str, delimiter)
  -- Handle an edge case concerning the str parameter. Immediately return an
  -- empty table if str == ''.
  if str == '' then return {} end

  -- Handle special cases concerning the delimiter parameter.
  -- 1. If the pattern is nil, split on contiguous whitespace.
  -- 2. If the pattern is an empty string, explode the string.
  -- 3. Protect against patterns that match too much. Such patterns would hang
  --    the caller.
  delimiter = delimiter or '%s+'
  if delimiter == '' then return explode(str) end
  if string.find('', delimiter, 1) then
    local msg = string.format('The delimiter (%s) would match the empty string.', delimiter)
    error(msg)
  end

  -- The table `t` will store the found items. `s` and `e` will keep
  -- track of the start and end of a match for the delimiter. Finally,
  -- `position` tracks where to start grabbing the next match.
  local t = {}
  local s, e
  local position = 1
  s, e = string.find(str, delimiter, position)

  while s do
    t[#t + 1] = string.sub(str, position, s-1)
    position = e + 1
    s, e = string.find(str, delimiter, position)
  end

  -- To get the (potential) last item, check if the final position is
  -- still within the string. If it is, grab the rest of the string into
  -- a final element.
  if position <= #str then
    t[#t + 1] = string.sub(str, position)
  end

  -- Special handling for a (potential) final trailing delimiter. If the
  -- last found end position is identical to the end of the whole string,
  -- then add a trailing empty field.
  if position > #str then
    t[#t + 1] = ''
  end

  return t
end

-- Emitted by Screen off
pfutil.pre_screen_switched_signal = PfSignal()
-- Emitted by ScreenSystemLayer
pfutil.screen_switched_signal = PfSignal()

function pfutil.screen_switch(screen)
  local top = SCREENMAN:GetTopScreen()
  pfinput.pop_input_layer()

  -- Not needed any more given that we do this manually
  -- top:SetNextScreenName(screen)

  -- We don't broadcast a SM_GoToNextScreen screen message as we handle that manually
  top:StartTransitioningScreen("")
  -- Disable input while we transition
  top:lockinput(top:GetTweenTimeLeft())

  local pftimer = require("permafrost.pftimer")
  local pfanim = require("permafrost.pfanim")
  pftimer.run_in_seconds(
    top:GetTweenTimeLeft(),
    function()
      pfanim.recurse_stop_animating_actor(top)
      -- MESSAGEMAN:Broadcast("PF_ScreenExit")
      SCREENMAN:SetNewScreen(screen)
      pfinput.push_input_layer()
    end
  )
end

function pfutil.screen_switch_immediate(screen)
  local top = SCREENMAN:GetTopScreen()
  pfinput.pop_input_layer()

  local pfanim = require("permafrost.pfanim")
  pfanim.recurse_stop_animating_actor(top)
  -- MESSAGEMAN:Broadcast("PF_ScreenExit")
  SCREENMAN:SetNewScreen(screen)
  pfinput.push_input_layer()
end

pfutil.CLICK_SOUND = THEME:GetPathS("", "Common value")
function pfutil.play_click_sound()
  SOUND:PlayOnce(pfutil.CLICK_SOUND)
end

function pfutil.get_mouse_position()
  return {
    x = INPUTFILTER:GetMouseX(),
    y = INPUTFILTER:GetMouseY(),
  }
end

--- Checks whether the mouse is over an actor
-- @tparam actor element the actor
-- @treturn bool true if the mouse is over the actor
function pfutil.is_mouse_over(element)
  if pfutil.is_etterna then
    local mouse = pfutil.get_mouse_position()
    return element:IsOver(mouse.x, mouse.y)
  else
    return false
  end
end

-- Thank you https://stackoverflow.com/a/42062321
function pfutil.table_to_string(node)
  -- to make output beautiful
  local function tab(amt)
    local str = ""
    for _ = 1, amt do
      str = str .. "  "
    end
    return str
  end

  local cache, stack, output = {},{},{}
  local depth = 1
  local output_str = "{\n"

  while true do
    local size = 0
    for _, _ in pairs(node) do
      size = size + 1
    end

    local cur_index = 1
    for k,v in pairs(node) do
      if (cache[node] == nil) or (cur_index >= cache[node]) then

        if (string.find(output_str,"}",output_str:len())) then
          output_str = output_str .. ",\n"
        elseif not (string.find(output_str,"\n",output_str:len())) then
          output_str = output_str .. "\n"
        end

        -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
        table.insert(output,output_str)
        output_str = ""

        local key
        local is_hidden = false
        if (type(k) == "number" or type(k) == "boolean") then
          key = "["..tostring(k).."]"
        else
          key = "['"..tostring(k).."']"
          if k:sub(1, 1) == "_" then
            -- is_hidden = true
          end
        end

        if is_hidden then
          output_str = output_str .. tab(depth) .. key .. " = ".."... (hidden)"
        elseif (type(v) == "number" or type(v) == "boolean") then
          output_str = output_str .. tab(depth) .. key .. " = "..tostring(v)
        elseif (type(v) == "table") then
          output_str = output_str .. tab(depth) .. key .. " = {\n"
          table.insert(stack,node)
          table.insert(stack,v)
          cache[node] = cur_index+1
          break
        else
          output_str = output_str .. tab(depth) .. key .. " = '"..tostring(v).."'"
        end

        if (cur_index == size) then
          output_str = output_str .. "\n" .. tab(depth-1) .. "}"
        else
          output_str = output_str .. ","
        end
      else
        -- close the table
        if (cur_index == size) then
          output_str = output_str .. "\n" .. tab(depth-1) .. "}"
        end
      end

      cur_index = cur_index + 1
    end

    if (size == 0) then
      output_str = output_str .. "\n" .. tab(depth-1) .. "}"
    end

    if (#stack > 0) then
      node = stack[#stack]
      stack[#stack] = nil
      depth = cache[node] == nil and depth + 1 or depth - 1
    else
      break
    end
  end

  -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
  table.insert(output,output_str)
  output_str = table.concat(output)

  return output_str
end

-- Shallow copy
function pfutil.table_copy(to_copy)
  local t = {}

  for k, v in pairs(to_copy) do
    t[k] = v
  end

  return t
end

function pfutil.table_index_of(t, elem)
  for k, v in pairs(t) do
    if v == elem then
      return k
    end
  end

  return nil
end

-- Merges b into a
function pfutil.table_merge_inplace(t, other)
  for k, v in pairs(other) do
    if not t[k] then
      t[k] = v
    else
      if type(t[k]) ~= type(v) then
        t[k] = v
      elseif type(v) == "table" then
        pfutil.table_merge_inplace(t[k], v)
      else
        t[k] = v
      end
    end
  end
end

function pfutil.table_map(t, func)
  for _, v in pairs(t) do
    func(v)
  end
end

function pfutil.table_contains(t, elem)
  for _, v in pairs(t) do
    if v == elem then
      return true
    end
  end

  -- Not found
  return false
end

function pfutil.table_index_of(t, val)
  for k, v in pairs(t) do
    if v == val then
      return k
    end
  end

  return nil
end

function pfutil.combine_cmp_sorts(algos)
  return function(a, b)
    for _, algo in ipairs(algos) do
      local cmp_result = algo(a, b)
      if cmp_result ~= 0 then
        return cmp_result == 1 and true or false
      end
    end

    return false
  end
end

function pfutil.invert_cmp_sort(algo)
  return function(a, b)
    return algo(a, b) * -1
  end
end

---
-- @treturn table List of hit sound file entries (see pffs.FileEntry)
function pfutil.get_hit_sounds()
  local dir = FileEntry(THEME:GetCurrentThemeDirectory() .. "Sounds/hitsound/")
  return dir:list_dir():iter():filter(function(entry)
    return entry.is_sound
  end):collect()
end

---
-- @treturn table List of combo break sound file entries (see pffs.FileEntry)
function pfutil.get_combo_break_sounds()
  local dir = FileEntry(THEME:GetCurrentThemeDirectory() .. "Sounds/combo_break/")
  return dir:list_dir():iter():filter(function(entry)
    return entry.is_sound
  end):collect()
end

-- Key hints on the top bar
pfutil.key_hints = {}
pfutil.key_hints.on_added_signal = PfSignal()
function pfutil.key_hints.add(action_text, key_text)
  pfutil.key_hints.on_added_signal:emit({
    action_text = action_text,
    key_text = key_text,
  })
end

return pfutil
