local pfutil = require("permafrost.pfutil")
local pfsong = require("permafrost.pfsong")

local pfconfig = {}

pfconfig.game_name = ProductFamily()
pfconfig.game_version = pfutil.is_etterna and GAMESTATE:GetEtternaVersion() or ProductVersion()
pfconfig.theme_name = "Permafrost"

pfconfig.fonts = {
  regular_90 = LoadFont("_exo 2 90px"),
  light_90 = LoadFont("_exo 2 light 90px"),
  thin_90 = LoadFont("_exo 2 thin 90px"),
  regular_40 = LoadFont("_exo 2 40px"),
  light_40 = LoadFont("_exo 2 light 40px"),
  thin_40 = LoadFont("_exo 2 thin 40px"),
}

pfconfig.font_info = {
  [pfconfig.fonts.regular_40] = {
    line_spacing = 58,
  },
}

pfconfig.palette = {
  purple = color("#6902AB"),
  purple_dark = color("#4A0377"),
  purple_light = color("#9B03FC"),

  pink = color("#C9007A"),
  pink_light = color("#FD0099"),

  red = color("#E9003A"),
  red_light = color("#FE0040"),

  black = color("#000000"),
  gray = color("#202020"),
  gray_dark = color("#0A0A0A"),
  gray_light = color("#303030"),
  gray_light2 = color("#606060"),
  white = color("#FFFFFF"),

  -- judge_w1 = color("#99CCFF"), -- Flawless
  -- judge_w2 = color("#F2CB30"), -- Perfect
  -- judge_w3 = color("#14CC8F"), -- Great
  judge_w1 = color("#00AEEF"), -- Flawless
  judge_w2 = color("#FFF568"), -- Perfect
  judge_w3 = color("#A4FF00"), -- Great
  judge_w4 = color("#1AB2FF"), -- Good
  judge_w5 = color("#FF1AB3"), -- Bad
  judge_miss = color("#CC2929"), -- Miss
}

pfconfig.colors = {
  screen_background = pfconfig.palette.gray_dark,
  title_text = pfconfig.palette.white,

  button_background = pfconfig.palette.gray_light,
  button_accent = pfconfig.palette.purple,
  button_accent_hover = pfconfig.palette.purple_light,
  button_accent_press = pfconfig.palette.pink,
  button_text = pfconfig.palette.white,

  text_input_background = pfconfig.palette.gray_light,
  text_input_text = pfconfig.palette.white,
  text_input_placeholder_text = Alpha(pfconfig.palette.white, 0.5),
  text_input_insert_point = pfconfig.palette.white,

  progress_bar_fill = pfconfig.palette.pink,
  progress_bar_fill_accent = pfconfig.palette.pink_light,
  progress_bar_glow = pfconfig.palette.pink_light,

  accent = pfconfig.palette.purple,
  accent_dark = pfconfig.palette.purple_dark,

  top_bar_background = pfconfig.palette.gray,
  panel_background = Alpha(pfconfig.palette.gray, 0.95),
  panel_text = pfconfig.palette.white,

  shadow = Alpha(pfconfig.palette.black, 0.4),

  judge_w1 = pfconfig.palette.judge_w1, -- Flawless
  judge_w2 = pfconfig.palette.judge_w2, -- Perfect
  judge_w3 = pfconfig.palette.judge_w3, -- Great
  judge_w4 = pfconfig.palette.judge_w4, -- Good
  judge_w5 = pfconfig.palette.judge_w5, -- Bad
  judge_miss = pfconfig.palette.judge_miss, -- Miss

  combo_w1 = pfconfig.palette.judge_w1, -- Flawless
  combo_w2 = pfconfig.palette.judge_w2, -- Perfect
  combo_w3 = pfconfig.palette.judge_w3, -- Great
  combo_any = pfconfig.palette.white,

  grade_0 = color("#FFFFFF"), -- AAAAA (Etterna only)
  grade_1 = pfconfig.palette.judge_w1, -- AAAA
  grade_2 = pfconfig.palette.judge_w2, -- AAA
  grade_3 = pfconfig.palette.judge_w3, -- AA
  grade_4 = pfconfig.palette.pink, -- A
  grade_5 = pfconfig.palette.purple_light, -- B
  grade_6 = pfconfig.palette.red_light, -- C
  grade_7 = pfconfig.palette.gray_light, -- D
  grade_fail = pfconfig.palette.gray_light2, -- F

  skip_background = Alpha(pfconfig.palette.black, 0.25),
  skip_text = pfconfig.palette.white,
  skip_bar = pfconfig.palette.white,

  song_bar_bg = pfconfig.palette.gray,
  song_bar_fg = pfconfig.palette.pink,
  song_bar_title = pfconfig.palette.white,

  life = pfconfig.palette.pink,

  leaderboard_bg = pfconfig.palette.gray_dark,
  leaderboard_rank_bg = pfconfig.palette.gray,
  leaderboard_rank_bg_highlight = pfconfig.palette.pink,

  select = pfconfig.palette.pink_light,
}

pfconfig.tap_note_score_colors = {
  TapNoteScore_W1 = pfconfig.colors.judge_w1,
  TapNoteScore_W2 = pfconfig.colors.judge_w2,
  TapNoteScore_W3 = pfconfig.colors.judge_w3,
  TapNoteScore_W4 = pfconfig.colors.judge_w4,
  TapNoteScore_W5 = pfconfig.colors.judge_w5,
  TapNoteScore_Miss = pfconfig.colors.judge_miss,
}

pfconfig.title_menu_options = {
  {
    id = "game_start",
    action = function()
      GAMESTATE:JoinPlayer(PLAYER_1)

      if PROFILEMAN:GetNumLocalProfiles() > 1 then
        pfutil.screen_switch("ScreenSelectProfile")
      else
        pfutil.screen_switch("ScreenProfileLoad")
      end
    end,
  },

  {
    id = "multiplayer",
    action = function()
      GAMESTATE:JoinPlayer(PLAYER_1)

      if IsNetSMOnline() then
        pfutil.screen_switch("ScreenNetSelectProfile")
      else
        pfutil.screen_switch("ScreenNetworkOptions")
      end
    end,
  },

  {
    id = "settings",
    action = function()
      pfutil.screen_switch("ScreenOptionsService")
    end,
  },

  {
    id = "quit",
    action = function()
      pfutil.screen_switch("ScreenExit")
    end,
  },
}

pfconfig.screen_transition_seconds = 0.2

pfconfig.top_bar_height = 42

-- pfconfig.select_music_entry_count = 21
-- pfconfig.select_music_entry_off_screen_border = 64
pfconfig.select_music_entry_count = 34
pfconfig.select_music_entry_off_screen_border = 500
pfconfig.select_music_entry_height = 60
pfconfig.select_music_entry_width = 650
pfconfig.select_music_bar_max_difficulty = 30

pfconfig.online_platform = {
  name = "EtternaOnline",
}

pfconfig.skill_sets = {
  {
    id = "Overall",
    lang_key = "skillset_overall",
  },
  {
    id = "Stream",
    lang_key = "skillset_stream",
  },
  {
    id = "Jumpstream",
    lang_key = "skillset_jump_stream",
  },
  {
    id = "Handstream",
    lang_key = "skillset_hand_stream",
  },
  {
    id = "Stamina",
    lang_key = "skillset_stamina",
  },
  {
    id = "JackSpeed",
    lang_key = "skillset_jack_speed",
  },
  {
    id = "Chordjack",
    lang_key = "skillset_chord_jack",
  },
  {
    id = "Technical",
    lang_key = "skillset_technical",
  },
}

Branch.AfterProfileLoad = function()
  if pfsong.needs_indexing() then
    return "ScreenPfIndexing"
  else
    return "ScreenSelectMusic"
  end
  -- Default: ScreenSelectPlayMode
end

return pfconfig
