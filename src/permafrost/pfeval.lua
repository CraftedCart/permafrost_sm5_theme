local class = require("permafrost.class")
local pfscore = require("permafrost.pfscore")
local pfsong = require("permafrost.pfsong")
local pfutil = require("permafrost.pfutil")

local PfScore = pfscore.PfScore

local pfeval = {}

function pfeval.get_stage_stats()
  if pfutil.is_etterna then
    local sm_stage_stats = STATSMAN:GetCurStageStats()
    return pfeval.StageStats(sm_stage_stats)
  else
    local pftimer = require("permafrost.pftimer")

    local sm_stage_stats = SCREENMAN:GetTopScreen():GetStageStats()
    return pfeval.StageStats(sm_stage_stats)
  end
end

pfeval.StageStats = class.strict {
  sm_stage_stats = class.NULL,
  player_stats = class.NULL,

  __init = function(self, sm_stage_stats)
    self.sm_stage_stats = sm_stage_stats
    self.player_stats = {}

    for _, v in pairs(GAMESTATE:GetEnabledPlayers()) do
      self.player_stats[v] = pfeval.PlayerStageStats(sm_stage_stats:GetPlayerStageStats(v), self)
    end
  end,
}

pfeval.PlayerStageStats = class.strict {
  sm_player_stats = class.NULL,
  pf_stage_stats = class.NULL,

  grade = class.property {
    get = function(self)
      return pfscore.grade_lookup_map[self.sm_player_stats:GetGrade()]
    end,
    set = function(self, new) error("Cannot assign new grade to player stage stats") end,
  },

  score = class.property {
    get = function(self)
      return self.sm_player_stats:GetScore()
    end,
    set = function(self, new) self.sm_player_stats:SetScore(new) end,
  },

  high_score = class.property {
    get = function(self)
      local score = PfScore()
      -- score.music_rate = -- TODO
      score.sm_highscore = self.sm_player_stats:GetHighScore()
      return score
    end,
    set = function(self, new) error("Cannot assign new high score to player stage stats") end,
  },

  tap_w1_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_W1") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  tap_w2_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_W2") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  tap_w3_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_W3") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  tap_w4_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_W4") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  tap_w5_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_W5") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  tap_miss_count = class.property {
    get = function(self) return self.sm_player_stats:GetTapNoteScores("TapNoteScore_Miss") end,
    set = function(self, new) error("Cannot assign new tap note score count to player stage stats") end,
  },

  max_combo = class.property {
    get = function(self) return self.sm_player_stats:MaxCombo() end,
    set = function(self, new) error("Cannot assign new max combo to player stage stats") end,
  },

  played_steps = class.property {
    get = function(self)
      local sm_steps = self.sm_player_stats:GetPlayedSteps()
      local sm_songs = self.pf_stage_stats.sm_stage_stats:GetPlayedSongs()

      local steps = {}
      for k, v in ipairs(sm_steps) do
        steps[k] = pfsong.PfStep(v, pfsong.PfSong(sm_songs[k]))
      end

      return steps
    end,
    set = function(self, new) error("Cannot assign new played steps to player stage stats") end,
  },

  __init = function(self, sm_player_stats, pf_stage_stats)
    self.sm_player_stats = sm_player_stats
    self.pf_stage_stats = pf_stage_stats
  end,
}

return pfeval
