local pfutil = require("permafrost.pfutil")

local pflang = {}

function pflang.get_entry(key)
  return THEME:GetString("pflang", key)
end

function pflang.format_entry(args)
  local new_args = pfutil.table_copy(args)
  new_args[1] = pflang.get_entry(args[1])

  return pflang.format_string(new_args)
end

function pflang.format_string(args)
  local str = args[1] -- args[1] should be the format string

  for k, v in pairs(args) do
    local key = k

    -- Minus 1 because the first element in the args table is the format string
    if type(key) == "number" then key = key - 1 end

    local function printf_format(format)
      return string.format(format, v)
    end

    -- Pluralization rules work like how Qt does, see https://doc.qt.io/qt-6/i18n-plural-rules.html
    local function plural_format(format)
      local lang = THEME:GetCurLanguage()
      local parts = pfutil.string_split(format:sub(3 + #k, -2), ",")

      if lang == "fr" then -- French
        if v < 2 then return parts[1]
        else return parts[2]
        end
      elseif lang == "cs" then -- Czech
        if v % 100 == 1 then return parts[1]
        elseif v % 100 >= 2 and v % 100 <= 4 then return parts[2]
        else return parts[3]
        end
      elseif lang == "ga" then -- Irish
        if v == 1 then return parts[1]
        elseif v == 2 then return parts[2]
        else return parts[3]
        end
      elseif lang == "lv" then -- Latvian
        if v % 10 == 1 and v % 100 ~= 11 then return parts[1]
        elseif v ~= 0 then return parts[2]
        else return parts[3]
        end
      elseif lang == "mk" then -- Macedonian
        if v % 10 == 1 then return parts[1]
        elseif v % 10 == 2 then return parts[2]
        else return parts[3]
        end
      elseif lang == "pl" then -- Polish
        if v == 1 then return parts[1]
        elseif v % 10 >= 2 and v % 10 <= 4 and (v % 100 < 10 or v % 100 > 20) then return parts[2]
        else return parts[3]
        end
      elseif lang == "ro" then -- Romanian
        if v == 1 then return parts[1]
        elseif v == 0 or (v % 100 >= 1 and n % 100 <= 20) then return parts[2]
        else return parts[3]
        end
      elseif lang == "ru" then -- Russian
        if v % 10 == 1 and v % 100 ~= 11 then return parts[1]
        elseif v % 10 >= 2 and v % 10 <= 4 and (v % 100 < 10 or v % 100 > 20) then return parts[2]
        else return parts[3]
        end
      elseif lang == "sk" then -- Slovak
        if v == 1 then return parts[1]
        elseif v >= 2 and v <= 4 then return parts[2]
        else return parts[3]
        end
      elseif lang == "ja" then -- Japanese
        return parts[1]
      else -- Fall back to English rules
        if v == 1 then return parts[1]
        else return parts[2]
        end
      end
    end

    -- `{key}` - replace key with value
    str = str:gsub("{" .. key .. "}", v)

    -- `{key:%d}` - replace key with value, passed through string.format
    str = str:gsub("{" .. key .. ":(.-)}", printf_format)

    -- `{seconds+second,seconds}` - replace key with either second or seconds, depending on language pluralization
    -- rules
    str = str:gsub("{" .. key .. "%+,s}", plural_format)
  end

  return str
end

return pflang
