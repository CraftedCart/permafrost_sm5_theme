local class = require("permafrost.class")

local pfeditableui = {}

pfeditableui.PfEditableFloatProp = class.strict {
  display_name_key = class.NULL,
  setter = class.NULL,
  getter = class.NULL,
  increment = 1,
  format = "%.0f",
  min_value = class.NULL,
  max_value = class.NULL,

  with_display_name_key = function(self, value)
    self.display_name_key = value
    return self
  end,

  with_bind = function(self, var_name)
    self.setter = function(obj, new_value)
      obj[var_name] = new_value
    end
    self.getter = function(obj)
      return obj[var_name]
    end

    return self
  end,

  with_increment = function(self, value)
    self.increment = value
    return self
  end,

  with_format = function(self, value)
    self.format = value
    return self
  end,

  with_min_value = function(self, value)
    self.min_value = value
    return self
  end,

  with_max_value = function(self, value)
    self.max_value = value
    return self
  end,
}

pfeditableui.PfEditableWidget = class.strict {
  name = class.NULL,
  display_name_key = class.NULL,
  widget = class.NULL,
  bounds_getter = class.NULL,

  props = class.NULL,

  __init = function(self, widget)
    self.widget = widget

    self.props = {}
  end,

  with_name = function(self, value)
    self.name = value
    return self
  end,

  with_display_name_key = function(self, value)
    self.display_name_key = value
    return self
  end,

  with_bounds = function(self, value)
    self.bounds_getter = value
    return self
  end,

  with_float_prop = function(self, args)
    local prop = pfeditableui.PfEditableFloatProp()
      :with_bind(args[1])
      :with_display_name_key(args[2])
      :with_increment(args.increment)

    if args.format ~= nil then prop:with_format(args.format) end
    if args.min_value ~= nil then prop:with_min_value(args.min_value) end
    if args.max_value ~= nil then prop:with_max_value(args.max_value) end

    table.insert(self.props, prop)
    return self
  end,

  serialize = function(self)
    local out = {}
    for _, prop in pairs(self.props) do
      -- TODO: Should I be using display name here?
      out[prop.display_name_key] = prop.getter(self.widget)
    end

    return out
  end,

  deserialize = function(self, from)
    for _, prop in pairs(self.props) do
      local data = from[prop.display_name_key]
      if data ~= nil then
        prop.setter(self.widget, data)
      end
    end
  end,
}

-- NOT a strict class
pfeditableui.PfEditableUi = class {
  root_widget = class.NULL,
  editable_widgets = class.NULL,

  __init = function(self)
    self.editable_widgets = {}
  end,

  serialize_editable_widgets = function(self)
    local out = {}
    for _, v in pairs(self.editable_widgets) do
      out[v.name] = v:serialize()
    end

    return out
  end,

  deserialize_editable_widgets = function(self, from)
    for _, v in pairs(self.editable_widgets) do
      local data = from[v.name]
      if data ~= nil then
        v:deserialize(data)
      end
    end

    return out
  end,
}

return pfeditableui
