local pfutil = require("permafrost.pfutil")
local json = require("permafrost.json")

local pfuserconfig = {}

pfuserconfig.public_config = {
  notefield = {
    scale = 1,
    hit_sound = {
      file = nil,
      volume = 1,
    },
    combo_break_sound = {
      file = nil,
      volume = 1,
    },
    ui_widgets = {},
  },
}

pfuserconfig.private_config = {
  warning = "PRIVATE CONFIG - DO NOT SHARE WITH OTHERS (This file may contain login information)",
  etterna = {
    user_name = nil,
    token = nil,
  },
}

pfuserconfig.PUBLIC_CONFIG_PATH = "Save/permafrost_theme/public_user_config.json"
pfuserconfig.PRIVATE_CONFIG_PATH = "Save/permafrost_theme/private_user_config.json"

function pfuserconfig.save_to_file()
  local public_str = json.encode(pfuserconfig.public_config)
  File.Write(pfuserconfig.PUBLIC_CONFIG_PATH, public_str)

  local private_str = json.encode(pfuserconfig.private_config)
  File.Write(pfuserconfig.PRIVATE_CONFIG_PATH, private_str)
end

function pfuserconfig.load_from_file()
  local public_contents = File.Read(pfuserconfig.PUBLIC_CONFIG_PATH)
  if public_contents then
    pfutil.table_merge_inplace(pfuserconfig.public_config, json.decode(public_contents))
  end

  local private_contents = File.Read(pfuserconfig.PRIVATE_CONFIG_PATH)
  if private_contents then
    pfutil.table_merge_inplace(pfuserconfig.private_config, json.decode(private_contents))
  end
end

return pfuserconfig
