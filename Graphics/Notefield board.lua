local pfmath = require("permafrost.pfmath")
local pfutil = require("permafrost.pfutil")
local pfwidget = require("permafrost.pfwidget")
local pftimer = require("permafrost.pftimer")
local pfeasing = require("permafrost.pfeasing")
local pfsignal = require("permafrost.pfsignal")
local pfinput = require("permafrost.pfinput")
local pfsong = require("permafrost.pfsong")
local pfconfig = require("permafrost.pfconfig")
local pflang = require("permafrost.pflang")
local pfnotefieldconfig = require("permafrost.pfnotefieldconfig")

local Vec2 = pfmath.Vec2
local PfSignal = pfsignal.PfSignal
local PfCanvas = pfwidget.PfCanvas
local PfRect = pfwidget.PfRect
local PfText = pfwidget.PfText
local PfTextInput = pfwidget.PfTextInput
local PfNoteFieldConfig = pfnotefieldconfig.PfNoteFieldConfig

local tap_judgment_signal = PfSignal()
local hold_judgment_signal = PfSignal()

local FILTER_PADDING = 10
local ARROW_WIDTH = 64 -- Until noteskin metrics are implemented...

local style = GAMESTATE:GetCurrentStyle()
local num_columns = style:ColumnsPerPlayer()
local style_width = style:GetWidth(PLAYER_1)
local column_width = style_width / num_columns

local filter_width = (ARROW_WIDTH * num_columns) + (FILTER_PADDING * 2)

-- TEMP
local nf_config = PfNoteFieldConfig()
nf_config:load_from_player(1)
nf_config:apply_to_player(1)

local canvas = PfCanvas()

local combo_breakers = {
  TapNoteScore_W4 = { color = pfconfig.colors.judge_w4 },
  TapNoteScore_W5 = { color = pfconfig.colors.judge_w5 },
  TapNoteScore_Miss = { color = pfconfig.colors.judge_miss },
}

for i = 1, num_columns do
  local player = PLAYER_1

  local lane_overlay = PfRect()
  lane_overlay:add_hook(
    "InitCommand",
    function(actor)
      local is_reverse = GAMESTATE:GetPlayerState(player):GetCurrentPlayerOptions():UsingReverse()
      local receptor_y = is_reverse and THEME:GetMetric("Player", "ReceptorArrowsYStandard") or THEME:GetMetric("Player", "ReceptorArrowsYReverse")
      receptor_y = receptor_y * (1 / nf_config.scale) -- Adjust for the Mini mod

      actor.position = Vec2(
        (-(ARROW_WIDTH * (num_columns / 2)) + ((i - 1) * ARROW_WIDTH) + (ARROW_WIDTH / 2)),
        -receptor_y - (ARROW_WIDTH / 2)
        )

      actor.align = Vec2(0.5, 0)
      actor.size = Vec2(ARROW_WIDTH - 4, SCREEN_HEIGHT)
      actor.color = pfconfig.palette.white
      actor.fade_bottom = 1
      actor.alpha = 0
    end
    )
  lane_overlay:add_hook(
    "JudgmentMessageCommand",
    function(actor, params)
      if params.HoldNoteScore then return end
      if params.Player ~= player then return end
      if params.FirstTrack + 1 ~= i then return end
      if params.TapNoteScore then
        local breaker = combo_breakers[params.TapNoteScore]
        if breaker ~= nil then
          actor.color = breaker.color

          actor.ctx.anim{
            start_val = 0.4,
            end_val = 0,
            prop_table = actor,
            prop_name = "alpha",
            duration = 0.5,
            easing = pfeasing.linear,
          }
          -- actor.ctx.anim{
            -- start_val = 0,
            -- end_val = 1,
            -- prop_table = actor,
            -- prop_name = "scale_y",
            -- duration = 0.5,
            -- easing = pfeasing.out_expo,
          -- }
        end
      end
    end
    )
  canvas:add_child(lane_overlay)
end

-- for i=1,num_columns do
  -- r[#r+1] = Def.Quad{
    -- InitCommand = function(self)
      -- self:zoomto((arrowWidth - 4) * noteFieldWidth, SCREEN_HEIGHT * 2)

      -- local reverse = GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions():UsingReverse()
      -- local receptor = reverse and THEME:GetMetric("Player", "ReceptorArrowsYStandard") or THEME:GetMetric("Player", "ReceptorArrowsYReverse")

      -- self:diffusealpha(alpha)
      -- local thewidth
      -- if noteFieldWidth >= 1 then
        -- thewidth = math.abs(1-noteFieldWidth)
      -- else
        -- thewidth = noteFieldWidth - 1
      -- end
      -- self:xy((-(arrowWidth * (num_columns / 2)) + ((i - 1) * arrowWidth) + (arrowWidth / 2)) + (i-(num_columns/2)-(1/2))*colWidth*(thewidth),-receptor)
      -- self:fadebottom(0.6):fadetop(0.6)
      -- self:visible(false)
    -- end,
    -- JudgmentMessageCommand=function(self,params)
      -- local notes = params.Notes
      -- local firstTrack = params.FirstTrack+1
      -- if params.HoldNoteScore then return end
      -- if params.Player == pn and params.TapNoteScore then
        -- local enum  = Enum.Reverse(TapNoteScore)[params.TapNoteScore]
        -- if enum < judgeThreshold and enum > 3 and i == firstTrack then
          -- self:stoptweening()
          -- self:visible(true)
          -- self:diffuse(byJudgment(params.TapNoteScore))
          -- self:diffusealpha(alpha)
          -- self:tween(0.25, "TweenType_Bezier",{0,0,0.5,0,1,1,1,1})
          -- self:diffusealpha(0)
        -- end
      -- end
    -- end,
    -- UpdateCommand = function(self)
      -- noteFieldWidth = MovableValues.NotefieldWidth
      -- self:zoomtowidth((colWidth-border) * noteFieldWidth)
      -- self:addx((i-(num_columns/2)-(1/2))*colWidth * (noteFieldWidth - oldWidth))
    -- end
  -- }
-- end

canvas:add_hook(
  "JudgmentMessageCommand",
  function(actor, args)
    if args.TapNoteOffset then -- If this is a tap...
      tap_judgment_signal:emit{
        tap_note_offset = args.TapNoteOffset,
        tap_note_score = args.TapNoteScore,
      }
    else -- It's a hold
      hold_judgment_signal:emit{
        hold_note_offset = args.HoldNoteOffset,
        hold_note_score = args.HoldNoteScore,
      }
    end
  end
)

return canvas:make_actor()
